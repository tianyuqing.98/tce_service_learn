
#from pix2pix.jpcartoon import Cartooner, getOpt
import cv2, os, sys
import numpy as np
import math

from align_v4_linux.align_single_v4_smash import LandDetector, draw_by_map, apply_racial_lut, post_proc, set_global_paras
sys.path.append('pix2pix')

#from superres.upsample import Upsampler
sys.path.append('pix2pix')
from jpcartoon import Cartooner, getOpt
import time
from natsort import natsorted
import torch



def copy_to_cpu(gpu_tensor):
    cpu_array = gpu_tensor.cpu().float().numpy()
    cpu_array = np.flip(cpu_array, (2,))
    return cpu_array

def find_closest_resolution(shape):
    max_len = max((shape[0], shape[1]))
    for i in range(60): #最大不能超过60*64
        if i*64>max_len:
            return i*64
    
    print("size to large")
    return 256
    

def image_to_subimages(image, num_of_blocks, overlap=31): #只能处理i*（i-1）或者i*i数量的block
    num_long = math.ceil(math.sqrt(num_of_blocks))
    num_short = math.floor(math.sqrt(num_of_blocks))
    if num_long * num_short != num_of_blocks:
        print("not a valid number of blocks")
        return None, None
    long_side = 0
    if image.shape[0]<image.shape[1]:
        long_side = 1
        
 
    size_long = math.ceil(((num_long-1)*overlap + image.shape[long_side])/num_long)
    size_short = math.ceil(((num_short-1)*overlap + image.shape[1-long_side])/num_short)
    
    sub_images = []
    locs = []
    masks = []
    for i in range(num_long):
        for j in range(num_short):
            
            if long_side==0:
                up = int(i*size_long-(i)*overlap)
                down = int(min(up+size_long, image.shape[0]))
                left = int(j*size_short - (j)*overlap)
                right = int(min(left+size_short, image.shape[1]))
                
            else:
                up = int(j*size_short-j*overlap)
                down = int(min(up+size_short, image.shape[0]))
                left = int(i*size_long-i*overlap)
                right = int(min(left+size_long, image.shape[1]))
                
            print((image.shape[1], image.shape[0], long_side, num_long, num_short,size_long, size_short, up, down, left, right))
            
            sub_image = image[up:down, left:right]
#            cv2.imwrite('sub_image.jpg', sub_image)
            sub_images.append(sub_image)
            
            locs.append([up, down, left, right])
            
            mask_tmp_hori = np.ones((down-up, right-left))
            mask_tmp_vert = np.ones((down-up, right-left))
            mask_tmp = np.ones((down-up, right-left))
            if left != 0:
#                print("left fade")
                overlap_l = 0
                overlap_r =  min(overlap, right-left)
                for k in range(overlap_l, overlap_r):
                    mask_tmp_vert[:,k] = (k-overlap_l)/(overlap_r-overlap_l)
              
            if up !=0:
#                print(("up fade",up))
                overlap_u = 0
                overlap_d =  min(overlap, down-up)
                for k in range(overlap_u, overlap_d):
                    mask_tmp_hori[k,:] = (k-overlap_u)/(overlap_d-overlap_u)
                    
            mask_tmp_vert_ori =  mask_tmp_vert.copy()
            mask_tmp_vert[mask_tmp_hori<1] = 0
          
            mask_tmp_hori[mask_tmp_vert_ori<1] = 0
            mask_tmp_vert = np.tile(mask_tmp_vert[:,:,None], (1,1,3))
            mask_tmp_hori = np.tile(mask_tmp_hori[:,:,None], (1,1,3))
#            cv2.imwrite('mask_tmp_hori'+str(i)+str(j)+'.jpg', (mask_tmp_hori*255).astype(np.uint8))
#            cv2.imwrite('mask_tmp_vert'+str(i)+str(j)+'.jpg', (mask_tmp_vert*255).astype(np.uint8))
            masks.append((mask_tmp_hori, mask_tmp_vert))
           
            
    return sub_images, locs, masks
        

    

IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]


def is_image_file(filename):
    return any(filename.endswith(extension) for extension in IMG_EXTENSIONS)


class SingleImage():
    def __init__(self, image, is_bg=0, net_id=0):
        self.image = image
        self.is_bg = is_bg
        self.net_id = net_id
       
       
class ModelLoader():
    def __init__(self, face_net=1, bg_net=5, image_size = 720):
        
        self.cartoon = []
        self.cartoon.append(Cartooner(getOpt(net_type=2, image_size=384)))
        self.cartoon.append(Cartooner(getOpt(net_type=bg_net, image_size=384)))
        self.cartoon_bg = Cartooner(getOpt(net_type=bg_net, image_size=image_size))
       
    
    def test_time(self, imgs):
        time_st = time.time()
        self.cartoon[0].transform_batch(imgs)
        time_ed = time.time()
        print('time: %f' % (time_ed - time_st))
    
    def run(self, simage):
        #self.con.acquire()
     
        images = []
        images.append(simage.image)
        
        if len(images) > 0:
            if simage.is_bg==1:
                result = self.cartoon_bg.transform_batch(images)
            else:
                result = self.cartoon[simage.net_id].transform_batch(images)
            output_batch = []
            for img in result:
                output_batch.append(img)
        return output_batch

def process_init():
    global land_detector

    land_detector = LandDetector()
 

    print('smash detector created by process {}'.format(os.getpid()))

times = {"detect time":0, 
         "net time":0,
         "draw time":0, 
         "de/en-code time":0, 
          "gauss time":0, 
         "warp time":0, 
         "blend time":0, 
         "lut time":0, 
         "sharpen time":0, 
         "texture time":0, 
         "draw inner time":0,
         "post inner time":0,
         "resize one time":0}
bg_image_size = 1080
def process(img, model, use_blocks=1, bg_size=720, app_id=1):
    
    time_initial = time.time()
#    time_st = time.time()
  
#    image = img
    time_st_deen = time.time()
    image = cv2.imdecode(np.asarray(bytearray(img), dtype=np.uint8), cv2.IMREAD_COLOR) 
    times["de/en-code time"] = time.time() - time_st_deen + times["de/en-code time"]
    time_st = time.time()
    count, face_img, bg_img, cv2_trans_inv,warp_map_back, map_kernel, branch_id, proc_shape = land_detector.clip(image, app_id)
   

    times["detect time"] = time.time() - time_st + times["detect time"]
    
   
    
    time_st = time.time()

    res_images = []
    num_of_blocks = 1
    if use_blocks:
        sub_bg_images, locs, masks = image_to_subimages(bg_img, num_of_blocks)
        for i in range(num_of_blocks):
            clipped = cv2.cvtColor(sub_bg_images[i], cv2.COLOR_BGR2RGB)
    #        cv2.imwrite('clipped.jpg', clipped)
            if num_of_blocks==1:
#                res = find_closest_resolution(image.shape)
                clipped = cv2.resize(clipped, (bg_size, bg_size))
#                print((res,res))
            else:
                clipped = cv2.resize(clipped, (bg_size, bg_size))
            clipped = clipped.astype(np.float32) # / 127.5 - 1.0
         
        #    torch.cuda.synchronize(1)
            simage = SingleImage(clipped, 1)
            res_images.append(model.run(simage)[0])
    else:
       num_of_blocks = 1
       clipped = cv2.cvtColor(bg_img, cv2.COLOR_BGR2RGB)
       clipped = clipped.astype(np.float32) 
       simage = SingleImage(clipped, 1)
       res_images.append(model.run(simage)[0])
       
    if branch_id==2:
        net_id = 0
    else:
        net_id = 1
    for i in range(count):
        cur_clipped = cv2.cvtColor(face_img[:,:,3*i:(i+1)*3], cv2.COLOR_BGR2RGB)
        cv2.imwrite('face_img_'+str(i)+'.jpg', face_img[:,:,3*i:(i+1)*3])
        cur_clipped = cur_clipped.astype(np.float32) 
        simage = SingleImage(cur_clipped, 0, net_id)
        res_images.append(model.run(simage)[0])
   
    torch.cuda.synchronize(0)
    times["net time"] = time.time() - time_st + times["net time"]
    
    print("start draw")
    time_st = time.time()
    
    result = np.zeros((proc_shape[0],proc_shape[1],3))
    if use_blocks==1 and num_of_blocks!=1: # to do for jianying branch 3
        for i in range(num_of_blocks):
            result_bg = res_images[i]
#            cv2.imwrite('result_bg'+str(i)+'.jpg', result_bg)
            result_bg = cv2.resize(result_bg, (locs[i][3]-locs[i][2], locs[i][1]-locs[i][0]))
    #        print(result_bg.shape)
    #        print(result[locs[i][0]:locs[i][1], locs[i][2]:locs[i][3]].shape)
    #        print(masks[i].shape)
            if i==0:
                result[locs[i][0]:locs[i][1], locs[i][2]:locs[i][3]] = result_bg
            if locs[i][0]!=0:#不是上边界
                result[locs[i][0]:locs[i][1], locs[i][2]:locs[i][3]] = \
                    result[locs[i][0]:locs[i][1], locs[i][2]:locs[i][3]]*(1-masks[i][0]) + result_bg*masks[i][0]
            if locs[i][2]!=0:#不是左边界
                result[locs[i][0]:locs[i][1], locs[i][2]:locs[i][3]] = \
                    result[locs[i][0]:locs[i][1], locs[i][2]:locs[i][3]]*(1-masks[i][1]) + result_bg*masks[i][1]
    #        cv2.imwrite('mask_tmp_vert'+str(i)+'.jpg', (masks[i][1]*255).astype(np.uint8))
    #        cv2.imwrite('mask_tmp_hori'+str(i)+'.jpg', (masks[i][0]*255).astype(np.uint8))
    #        cv2.imwrite('result'+str(i)+'.jpg', result.astype(np.uint8))
            result[result>255] = 255
    else:
        result_bg = res_images[0]
        result = cv2.resize(result_bg, (proc_shape[1], proc_shape[0]))
   
    result = result.astype(np.uint8)
#    cv2.imwrite('result.jpg', result)
    
#    result = cv2.resize(result_bg, (image.shape[1], image.shape[0]))
    times["resize one time"] = time.time() - time_st + times["resize one time"]
    
    
#    for i in range(len(sub_bg_images)):
        
   
    for i in range(count):
        result_face = res_images[i+num_of_blocks] # np, float, (-1,1), CHW
        cv2.imwrite('result_face'+str(i)+'.jpg', result_face)
#        cv2.imwrite('warp_map_back'+str(i)+'.jpg', warp_map_back[:,:,3*i:(i+1)*3])
       
        result, draw_times = draw_by_map(1, result_face, result, warp_map_back[:,:,3*i:(i+1)*3], 
                                                cv2_trans_inv[:,3*i:(i+1)*3], [int(map_kernel[i])],  (proc_shape[1], proc_shape[0]))
#        result = draw_by_map(1, result_face, result, warp_map_back[:,:,3*i:(i+1)*3], 
#                                                cv2_trans_inv[:,3*i:(i+1)*3], [int(map_kernel[i])],  (proc_shape[1], proc_shape[0]))
#        times["resize time"] = draw_times["resize time"] + times["resize time"]
        times["gauss time"] = draw_times["gauss time"] + times["gauss time"]
        times["warp time"] = draw_times["warp time"] + times["warp time"]
        times["blend time"] = draw_times["blend time"] + times["blend time"]
        times["draw inner time"] = draw_times["draw total time"] + times["draw inner time"]
        print("gauss time: "+str(draw_times["gauss time"]))
        print("warp time: "+str(draw_times["warp time"]))
        print("blend time: "+str(draw_times["blend time"]))
        print("draw inner time: "+str(draw_times["draw total time"]))
#        cv2.imwrite('result'+str(i)+'.jpg', result)
    if app_id==1:
        result, draw_times = post_proc(result, land_detector.lut, land_detector.texture)
        if branch_id==3:
            result = cv2.resize(result, (image.shape[1],image.shape[0]))

#        times["lut time"] = draw_times["lut time"] + times["lut time"]
#        times["texture time"] = draw_times["texture time"] + times["texture time"]
#        times["sharpen time"] = draw_times["sharpen time"] + times["sharpen time"]
#        times["post inner time"] = draw_times["post total time"] + times["post inner time"]
    times["draw time"] = time.time() - time_st + times["draw time"]
    
    time_st_deen = time.time()
    result_enc = cv2.imencode('.jpg', result)[1].tostring()
    data = np.fromstring(result_enc, dtype=np.uint8)
    result = cv2.imdecode(data, 1)
    times["de/en-code time"] = time.time() - time_st_deen + times["de/en-code time"]
#    print('draw time: %f' % (time.time() - time_st))
   
    return result, time.time() - time_initial

if __name__ == '__main__':

    in_dir = '/data00/huxinghong/mangaface/code/demo_vici_video/dataset/videos/多人-出入画面1'
    out_dir = '/data00/huxinghong/mangaface/code/demo_video_local/dataset/videos_output/多人-出入画面1'
    loop_count = 5
    use_blocks = 0
    face_net = 1 #1-1w6, 2-stylegan, 3-pix2pix scene, 4-400M, 5-1.7g
    bg_net = 5
    bg_size = 720
    app_id = 1 #1 for jianying 2 for others
    if len(sys.argv)>1:
        if len(sys.argv)==2:
            print("give output path")
            sys.exit(0)
        elif len(sys.argv)==3:
            in_dir = sys.argv[1]
            out_dir = sys.argv[2]
            print(in_dir)
        elif len(sys.argv)==4:
            in_dir = sys.argv[1]
            out_dir = sys.argv[2] 
            face_net = int(sys.argv[3])
        elif len(sys.argv)==5:
            in_dir = sys.argv[1]
            out_dir = sys.argv[2] 
            face_net = int(sys.argv[3])
            bg_net = int(sys.argv[4])
        elif len(sys.argv)==6:
            in_dir = sys.argv[1]
            out_dir = sys.argv[2] 
            face_net = int(sys.argv[3])
            bg_net = int(sys.argv[4])
            loop_count = int(sys.argv[5])
        elif len(sys.argv)==7:
            in_dir = sys.argv[1]
            out_dir = sys.argv[2] 
            face_net = int(sys.argv[3])
            bg_net = int(sys.argv[4])
            loop_count = int(sys.argv[5])
            use_blocks = int(sys.argv[6])
        elif len(sys.argv)==8:
            in_dir = sys.argv[1]
            out_dir = sys.argv[2] 
            face_net = int(sys.argv[3])
            bg_net = int(sys.argv[4])
            loop_count = int(sys.argv[5])
            use_blocks = int(sys.argv[6])  
            bg_size = int(sys.argv[7]) 
        elif len(sys.argv)==9:
            in_dir = sys.argv[1]
            out_dir = sys.argv[2] 
            face_net = int(sys.argv[3])
            bg_net = int(sys.argv[4])
            loop_count = int(sys.argv[5])
            use_blocks = int(sys.argv[6])  
            bg_size = int(sys.argv[7]) 
            app_id = int(sys.argv[8]) 
       
        sys.argv = sys.argv[:1]
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
        
    process_init()
    model = ModelLoader(face_net, bg_net, bg_size)
    face_ids = []
    index = 0
    processed = -1
    all_time = 0
    for root, _, fnames in sorted(os.walk(in_dir)):
        for fname in natsorted(fnames):
            
            for i in range(loop_count):
                if is_image_file(fname): 
                    
                    if index<=processed:
                        continue
                    print(os.path.join(in_dir, fname))
    #                if '147.' in fname: 
                    img = open(os.path.join(in_dir, fname), 'rb').read()
#                    img = cv2.imread(os.path.join(in_dir, fname))
#                    print(img.shape)
                    res_img, used_time= process(img, model, use_blocks, bg_size, app_id)
#                    if res_img is not None:
                       
                    index = index+1
                    all_time += used_time
                    print("finish one image: "+str(all_time/index))
                    for items, values in times.items():
                        print(items+ " "+str(values/index))
                    cv2.imwrite(os.path.join(out_dir, fname), res_img)

        break
    


