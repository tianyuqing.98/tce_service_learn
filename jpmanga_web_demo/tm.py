from __future__ import print_function
import os, sys, time
import threading, argparse
import euler
from jpcartoon_thrift import CartoonService
from jpcartoon_thrift import ProcessJpcartoonRequest, ProcessJpcartoonResponse
import cv2
import numpy as np 


class MThread(threading.Thread):
    def __init__(self, img_path, id):
        threading.Thread.__init__(self)
        self.id = id
        self.path = img_path
        self.fd = ProcessJpcartoonRequest()
        self.client = euler.Client(CartoonService, 'tcp://localhost:1234')

    def run(self):
        
#        for _, _, files in os.walk(self.path):
#            for i in range(5):
#                for fname in files:
#        full_path = os.path.join(self.path, fname)
        process(self.client, self.path, self.id)
       


def process(client, file_path, id):
    fd = ProcessJpcartoonRequest()
    fd.image = open(file_path, 'rb').read()
    fd.BasePara.para1 = 0
    fd.BasePara.para2 = 0
    fd.BasePara.para3 = 0
    fd.image = open(file_path, 'rb').read()
    time_st = time.time()
    resp = client.Process(fd)
    time_ed = time.time()
    
    if resp.image:
        data = np.fromstring(resp.image, dtype=np.uint8)
        cv_output = cv2.imdecode(data, 1)
#        im_out = np.concatenate((im_out, cv_output), axis=1)
        cv2.imwrite(os.path.join(out_path,''+file_path.split('/')[-1]),cv_output)
   
    print('thread:%d, %f' % (id, time_ed - time_st))
    print('client done')



out_path = '/data00/huxinghong/mangaface/code/demo_server_real/test'
in_path = '/data00/huxinghong/mangaface/code/demo_server_real/dataset/人种'
im_path = '/data00/huxinghong/mangaface/code/demo_server_stylegan/test/image.png'
if __name__ == '__main__':
    
    if len(sys.argv)>1:
        if len(sys.argv)<=2:
            print("give output path")
            sys.exit(0)
        elif len(sys.argv)==3:
            in_path = sys.argv[1]
            out_path = sys.argv[2]
          
    
    if not os.path.isdir(out_path):
        os.makedirs(out_path)
    threadList = []
    num = 1
    for i in range(num):
        threadList.append(MThread(im_path, i))

    for i in range(num):
        threadList[i].start()

