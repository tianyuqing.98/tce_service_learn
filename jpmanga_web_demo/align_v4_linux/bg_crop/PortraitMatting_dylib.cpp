

#ifndef PortraitMatting_dylib_cpp
#define PortraitMatting_dylib_cpp

#include "include/PortraitMatting_API.h"
#include <iostream>
#include <stdio.h> 
#include <string.h> 
#include <typeinfo>

class PortraitMatting
{
public:
    PortraitMatting(): _handle(nullptr)
    {
//        _mp_args.base.image = nullptr;
//        _mp_args.base = ModuleBaseArgs base_args;
        _mp_args.need_flip_alpha = false;
    }
    
    ~PortraitMatting()
    {
        MP_ReleaseHandle(_handle);
    }
    
    int init(const char* model_path, int model_type, int contour_type, int min_len)
    {
       
        
        int ret = MP_CreateHandler(&_handle);
        if(ret != TT_OK)
        {
            std::cout << "MP_CreateHandler error!" << std::endl;
            return -1;
        }
        
        if(model_type==0)
        {
            ret = MP_InitModel(_handle, MP_LARGE_MODEL, model_path);
        }
        else
        {
            ret = MP_InitModel(_handle, MP_SMALL_MODEL, model_path);
        }
        if(ret != TT_OK)
        {
            std::cout << "MP_InitModel error!" << std::endl;
            return -1;
        }
        
        ret = MP_SetParam(_handle, MP_EdgeMode, contour_type);
        ret = MP_SetParam(_handle, MP_FrashEvery, 15);
        ret = MP_SetParam(_handle, MP_OutputMinSideLen, min_len);
        if(ret != TT_OK)
        {
            std::cout << "MP_SetParam error!" << std::endl;
            return -1;
        }
        
        return 0;
    }
    
    int do_portrait_matting(unsigned char* data, int rows, int cols, int step, bool use_rgb, unsigned char* alpha_mat)
    {
        int min_len;
        MP_GetParam(_handle, MP_OutputMinSideLen, &min_len);
        _mp_args.base.image = data;
        
        std::cout << "asign data success!" << std::endl;
       
        if(use_rgb)
        {
            _mp_args.base.pixel_fmt = kPixelFormat_RGBA8888;
        }
        else
        {
            _mp_args.base.pixel_fmt = kPixelFormat_BGR888;
        }
        _mp_args.base.image_width = cols;
        _mp_args.base.image_height = rows;
        _mp_args.base.orient = kClockwiseRotate_0;
        _mp_args.base.image_stride = step;
        
        if(_mp_args.base.image_width>=_mp_args.base.image_height)
        {
  
            _mp_ret.height = min_len;
            _mp_ret.width = (int)(1.0*min_len/_mp_args.base.image_height*_mp_args.base.image_width);
            _mp_ret.width = 16*(int(float(_mp_ret.width)/16+0.5f));
        }
        else
        {
            _mp_ret.width = min_len;
            _mp_ret.height = (int)(1.0*min_len/_mp_args.base.image_width*_mp_args.base.image_height);
            _mp_ret.height = 16*(int(float(_mp_ret.height)/16+0.5f));
        }
        std::cout << _mp_ret.width << std::endl;
        std::cout << _mp_ret.height << std::endl;
        _mp_ret.alpha = new unsigned char [_mp_ret.width * _mp_ret.height];
//        _mp_ret.alpha = (unsigned char * )malloc(sizeof(unsigned char) * (_mp_ret.width * _mp_ret.height));

        
        std::cout << "starting predicting" << std::endl;
        int ret = MP_DoPortraitMatting(_handle, &_mp_args, &_mp_ret);
//        std::cout << *(_mp_ret.alpha) << std::endl;
        
        if(ret != TT_OK)
        {
            std::cout << "MP_DoPortraitMatting error!" << std::endl;
            return -1;
        }
        else
        {
            std::cout << "MP_DoPortraitMatting finish!" << std::endl;
        }

        memcpy(alpha_mat, _mp_ret.alpha, sizeof(unsigned char) * (_mp_ret.width * _mp_ret.height));
        delete _mp_ret.alpha;
        
        return 0;
    }
    
private:
    PortraitMattingHandle _handle;
    MP_Args _mp_args; //输入
    MP_Ret _mp_ret; //输出
    
    
//    FaceBaseInfo _face_base_info[1];
//    FaceNewLandmark_Input _face_new_landmark_input;
//    FaceNewLandmark_Output* _p_face_new_landmark_output;
};

PortraitMatting portrait_matting;
extern "C"
{
int init_PM(const char* model_path, int model_type, int contour_type, int min_len)
{
    portrait_matting.init(model_path, model_type, contour_type, min_len);
    return 0;
}

int do_PM(unsigned char* image, int height, int width, int step, bool use_rgba, unsigned char* alpha_mat)
{
    int ret = portrait_matting.do_portrait_matting(image, height, width, step, use_rgba, alpha_mat);
    
    return ret;
}
}

#endif /* PortraitMatting_dylib_cpp */
