import ctypes
import numpy as np
import cv2
import os
import sys
import time
from natsort import natsorted

IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]


def is_image_file(filename):
    return any(filename.endswith(extension) for extension in IMG_EXTENSIONS)


model_path = './tt_matting_v9.0.model'.encode()
lib_path = './portraitmatting.so'

class PortraitMatting:
    def __init__(self, model_path=model_path, lib_path=lib_path, model_type=0, contour_type=0, min_len=128):
        self.min_len  = 128
        self.pm = ctypes.cdll.LoadLibrary(lib_path)
        ret = self.pm.init_PM(model_path, model_type, contour_type, self.min_len)
        if ret==0:
            print("MP_InitModel success!")
            


    def detect(self, image, use_rgba=False):
        
        h, w, c = image.shape
        if use_rgba:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGBA)

        if w>=h:
            mat_height = self.min_len;
            mat_width = 16*(int(w*float(self.min_len)/h/16.0+0.5));
        else:
            mat_width = self.min_len;
            mat_height = 16*(int(h*float(self.min_len)/w/16.0+0.5));
        print((mat_width, mat_height))
        self.alpha_mat_c = (ctypes.c_ubyte * mat_width * mat_height)()
        ret = self.pm.do_PM(image.tostring(), h, w, w*c , use_rgba, self.alpha_mat_c)
        
        
        if ret==-1:
            return 0, None
        
        alpha_mat = np.ctypeslib.as_array(self.alpha_mat_c)
        alpha_mat = cv2.resize(alpha_mat, (w,h))

        return 1, alpha_mat

  



if __name__ == '__main__':
    in_dir = '/data00/huxinghong/mangaface/code/demo_video_local/dataset/videos/videos/单人-出入画面1'
    out_dir = '/data00/huxinghong/mangaface/code/smash_wrapper/bg_crop/output/单人-出入画面1'
    
    use_trace = 1
    bg_net = 1 #1 for cycelgan, 2 for pix2pix, 3 for pix2pix_unetmobile
    align_type = 1 # 1 for alignment, 2 for crop, 3 for crop and blend
    if len(sys.argv)>1:
        if len(sys.argv)==2:
            print("give output path")
            sys.exit(0)
        elif len(sys.argv)==3:
            in_dir = sys.argv[1]
            out_dir = sys.argv[2]
            print(in_dir)
       
            
    pormat = PortraitMatting(model_path, lib_path)
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
        
    index = 0
    for root, _, fnames in sorted(os.walk(in_dir)):
        for fname in natsorted(fnames):
            if is_image_file(fname): 
                index = index+1
                
                print(fname)
#                if '147.' in fname: 
                img = cv2.imread(os.path.join(in_dir, fname))
                if use_trace==0:
                    face_ids = []
                ret, res_img = pormat.detect(img)
                if ret==0:
                    continue
                cv2.imwrite(os.path.join(out_dir, fname), res_img)
                
#            if index==2:
        break
  
