// Copyright (C) 2019 Beijing Bytedance Network Technology Co., Ltd. All rights reserved.
#ifndef ESPRESSO_API_ESPRESSO_H_
#define ESPRESSO_API_ESPRESSO_H_

#include <string>
#include <vector>

namespace espresso {

#define ESPRESSO_VERSION "2.0.0.1023"

    const char *espresso_get_version();

    class Net;

    struct __attribute__((visibility("default"))) LayerOutput {
        void *data;
        int num;
        int width;
        int height;
        int channel;
        int type; // data type
        int fl; // fraction length


        explicit LayerOutput(void *data, int num, int width, int height, int channel,
                             int type, int fl) : data(data), num(num), width(width),
                                                 height(height), channel(channel),
                                                 type(type), fl(fl) {
        }

        int Count() {
            return num * width * height * channel;
        }

        int Offset(int n, int w, int h, int c) {
            return (n * (width * height * channel) + h * (width * channel) + w * channel) + c;
        }
    };

    class __attribute__((visibility("default"))) Thrustor {
    public:
        explicit Thrustor();

        // You should set the output layer names in case the memory have been cycled used.
        int CreateNet(const std::string &net,
                      void *param,
                      std::vector<std::string> &layer_out_names);

        // Input image should be resized as the network settings, DON'T USE.
        int SetInput(const std::string layer_name, void *data, int data_size,
                     int width, int height);

        int ReInferShape(int width, int height);

        int Inference();

        // No need to release the returned data
        LayerOutput Extract(const std::string &layer_name);

        virtual ~Thrustor();

        int getLayers();

        void setThreads(int nums);

        int SkipLayer(const std::string layer_name);

#ifdef DEBUG
        std::string LayerName(int layer);
#endif

        void VerifyNetParamters();

        int GetWeightLen();

        /**
         * Default output is the last layer.
         * If you don't set output layer name, you can use this to simplify the change.
         * @return if error, LayerOutput will null data.
         */
        LayerOutput getOutput();

        void InferenceBenchmark(int times);

    private:
        friend void ThrustorEnforceCPURuntime(Thrustor *thrustor);
        Net *handler_;
    };

    void ThrustorEnforceCPURuntime(Thrustor *thrustor);
}

#endif // ESPRESSO_API_ESPRESSO_H_
