import ctypes
import numpy as np
import cv2
import os
import sys
import time
from natsort import natsorted

IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]


def is_image_file(filename):
    return any(filename.endswith(extension) for extension in IMG_EXTENSIONS)


   
class FaceAttribute:
    def __init__(self, model_path, attr_extra_path, lib_path, slow_mode=False):
        
        self.fa = ctypes.cdll.LoadLibrary(lib_path)
        ret = self.fa.init_FS_ATTR(model_path, attr_extra_path, slow_mode)
        if ret==0:
            print("init_FA success!")
            
       
    def detect(self, image, landmarks, count, use_rgba=False):
        
        h, w, c = image.shape
        if use_rgba:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGBA)
   
        type_c = (ctypes.c_ubyte * 1 )()
        landmarks_c = (ctypes.c_float * 212)()
        for i in range(106 * 2) : landmarks_c[i] = landmarks[i]
        skin_type = -1
        ret = self.fa.do_predict_FS_ATTR(image.tostring(), h, w, w*c ,use_rgba, landmarks_c, count, type_c)
       
        if ret==-1:
            return 0, None
        skin_type = np.ctypeslib.as_array(type_c)[0]
       

        return 1, skin_type

  


if __name__ == '__main__':
    model_path = './model/tt_face_attribute_v5.0.model'.encode()
    face_model_path = './model/tt_face_v7.0.model'.encode()
    face_extra_path = './model/tt_face_extra_v10.0.model'.encode()
    attr_extra_path = './model/tt_face_attribute_extra_v2.0.model'.encode()
    lib_path = './faceattribute.so'

    type_str = ['白种人', '黄种人', '印度人', '黑人']
    in_dir = '/data00/huxinghong/mangaface/code/demo_video_local/dataset/videos/videos/单人-出入画面1'
    out_dir = '/data00/huxinghong/mangaface/code/smash_wrapper/bg_crop/output/单人-出入画面1'
    use_trace = 1
    bg_net = 1 #1 for cycelgan, 2 for pix2pix, 3 for pix2pix_unetmobile
    align_type = 1 # 1 for alignment, 2 for crop, 3 for crop and blend
    if len(sys.argv)>1:
        if len(sys.argv)==2:
            print("give output path")
            sys.exit(0)
        elif len(sys.argv)==3:
            in_dir = sys.argv[1]
            out_dir = sys.argv[2]
            print(in_dir)
       
    fa = FaceAttribute(face_model_path, face_extra_path, model_path, attr_extra_path, lib_path, False)   
    
    
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    f = open(os.path.join(out_dir, 'skin_type.txt'), 'w')    
    index = 0
    for root, _, fnames in sorted(os.walk(in_dir)):
        for fname in natsorted(fnames):
            if is_image_file(fname): 
                index = index+1
                
                print(fname)
#                if '147.' in fname: 
                img = cv2.imread(os.path.join(in_dir, fname))
                if use_trace==0:
                    face_ids = []
                ret, skin_type = fa.detect(img)
                print("skin type is "+ str(skin_type))
                if ret==0:
                    f.write(fname+' no face is detected\n')
                else:
                    f.write(fname+' '+type_str[skin_type]+'\n')
                if ret==0:
                    continue
                
                
#            if index==2:
        break
        f.close()
  
