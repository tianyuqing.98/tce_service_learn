import ctypes
import numpy as np
import cv2


class FaceOutlines:
    # no smooth: smooth_mode=0
    def __init__(self, platform, model_path, faceoutlines_lib_path, smooth_mode=0):
        self.platform = platform
        self.detector = ctypes.cdll.LoadLibrary(faceoutlines_lib_path)
        self.detector.init_FNL(model_path, smooth_mode)
        self.points = (ctypes.c_float * 88)()
        self.landmarks106 = (ctypes.c_float * 212)()
    def detect(self, image, landmarks, use_rgba):
        for i in range(106 * 2) : self.landmarks106[i] = landmarks[i]
        image_detection = cv2.cvtColor(image, cv2.COLOR_BGR2BGRA)
        h, w, c = image_detection.shape
        
        self.detector.do_predict_FNL(image_detection.tostring(), h, w, w * c, self.landmarks106, use_rgba, self.points)

        p = np.zeros(44 * 2, dtype=float)
        for i in range(44 * 2) : p[i] = self.points[i]
        return p

class FaceDetection:
    # Image format for face detection: 
    # - Linux: RGB
    # - Mac  : BGR
    def __init__(self, platform, model_path, extra_path, land_lib_path, slow_mode=False):
        self.platform = platform
        self.detector = ctypes.cdll.LoadLibrary(land_lib_path)
        self.point5 = (ctypes.c_float * 10)()
        self.point106 = (ctypes.c_float * 212)()

    def detect106(self, image, image_fmt):
        try:
            shape = image.shape
            if image_fmt == 'BGR':
                image_detection = image[:,:,::-1]
            else:
                image_detection = image.copy()
            count = self.detector.do_predict106(image_detection.tostring(), shape[0], shape[1], shape[1] * shape[2], self.point106)
           
            if count == 0:
                return count, None
            
            p = np.zeros(106 * 2, dtype=float)
            for i in range(106 * 2) : p[i] = self.point106[i]
            return count, p
        except:
            print("FaceDetection error")
            return 0, None

  

class FaceAlignment():
    # model_path & extra_path must be '***'.encode()
    # pad: left, right, up, bottom padding for the std_lms
    def __init__(self, platform, model_path, extra_path, land_lib_path, pad=[0, 0, 0, 0], slow_mode=False, use_detection=True):
        self.use_detection = use_detection
        if self.use_detection:
            self.face_detection = FaceDetection(platform=platform, model_path=model_path, extra_path=extra_path, land_lib_path=land_lib_path, slow_mode=slow_mode)
        else:
            self.face_detection = None

        self.pad = pad
        self.lms_id = np.array(
            [33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,
            56,57,58,59,60,61,62,63,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,
            100,101,102,103], dtype=np.int32)
        self.std_lms = np.array([
            [ 0.000213256,  0.106454  ], #17 33
            [ 0.0752622,    0.038915  ], #18 34
            [ 0.18113,      0.0187482 ], #19 35
            [ 0.29077,      0.0344891 ], #20 36
            [ 0.393397,     0.0773906 ], #21 37
            [ 0.586856,     0.0773906 ], #22 38
            [ 0.689483,     0.0344891 ], #23 39
            [ 0.799124,     0.0187482 ], #24 40
            [ 0.904991,     0.038915  ], #25 41
            [ 0.98004,      0.106454  ], #26 42
            [ 0.490127,     0.203352  ], #27 43
            [ 0.490127,     0.307009  ], #28 44
            [ 0.490127,     0.409805  ], #29 45
            [ 0.490127,     0.515625  ], #30 46
            [ 0.36688,      0.587326  ], #31 47
            [ 0.426036,     0.609345  ], #32 48
            [ 0.490127,     0.628106  ], #33 49
            [ 0.554217,     0.609345  ], #34 50
            [ 0.613373,     0.587326  ], #35 51
            [ 0.121737,     0.216423  ], #36 52
            [ 0.187122,     0.178758  ], #37 53
            [ 0.265825,     0.179852  ], #38 54
            [ 0.334606,     0.231733  ], #39 55
            [ 0.260918,     0.245099  ], #40 56
            [ 0.182743,     0.244077  ], #41 57
            [ 0.645647,     0.231733  ], #42 58
            [ 0.714428,     0.179852  ], #43 59
            [ 0.793132,     0.178758  ], #44 60
            [ 0.858516,     0.216423  ], #45 61
            [ 0.79751,      0.244077  ], #46 62
            [ 0.719335,     0.245099  ], #47 63
            [ 0.254149,     0.780233  ], #48 84
            [ 0.340985,     0.745405  ], #49 85
            [ 0.428858,     0.727388  ], #50 86
            [ 0.490127,     0.742578  ], #51 87
            [ 0.551395,     0.727388  ], #52 88
            [ 0.639268,     0.745405  ], #53 89
            [ 0.726104,     0.780233  ], #54 90
            [ 0.642159,     0.864805  ], #55 91
            [ 0.556721,     0.902192  ], #56 92
            [ 0.490127,     0.909281  ], #57 93
            [ 0.423532,     0.902192  ], #58 94
            [ 0.338094,     0.864805  ], #59 95
            [ 0.290379,     0.784792  ], #60 96
            [ 0.428096,     0.778746  ], #61 97
            [ 0.490127,     0.785343  ], #62 98
            [ 0.552157,     0.778746  ], #63 99
            [ 0.689874,     0.784792  ], #64 100
            [ 0.553364,     0.824182  ], #65 101
            [ 0.490127,     0.831803  ], #66 102
            [ 0.42689 ,     0.824182  ]  #67 103
            ], dtype=np.float32)
        self.pad_std_lms()
        
    def pad_std_lms(self):
        p = self.pad
        if p[0] > 0 or p[1] > 0 or p[2] > 0 or p[3] > 0:
            self.std_lms[:, 0] = (self.std_lms[:, 0] / (1 + p[0] + p[1])) + p[0] / (1 + p[0] + p[1])
            self.std_lms[:, 1] = (self.std_lms[:, 1] / (1 + p[2] + p[3])) + p[2] / (1 + p[2] + p[3])

    
    def expand_eyebrows(self, landmarks, eyebrows_expand_mod=1.0):
        landmarks = np.array(landmarks.copy(), dtype=np.float32)

        #
        ml_pnt = (landmarks[52] + landmarks[0]) / 2
        mr_pnt = (landmarks[61] + landmarks[31]) / 2
        ql_pnt = (landmarks[52] + ml_pnt) / 2
        qr_pnt = (landmarks[61] + mr_pnt) / 2

        # Top of the eye arrays
        bot_l = np.array((ql_pnt, landmarks[52], landmarks[53], landmarks[54], landmarks[55]))
        bot_r = np.array((landmarks[58], landmarks[59], landmarks[60], landmarks[61], qr_pnt))
        top_l = landmarks[33:38]
        top_r = landmarks[38:43]

        landmarks[33:38] = top_l + eyebrows_expand_mod * 0.5 * (top_l - bot_l)
        landmarks[38:43] = top_r + eyebrows_expand_mod * 0.5 * (top_r - bot_r)

        return landmarks

    def get_mask(self, image_shape, landmarks, color=(1,), expand_eyebrows=False):
        mask = np.zeros(image_shape[0:2] + (1,), dtype=np.float32)
        parts = []
        if(expand_eyebrows):
            new_landmarks = self.expand_eyebrows(landmarks, 1.0)
            mask = np.zeros(image_shape[0:2] + (1,), dtype=np.float32)

            r_jaw = (new_landmarks[0:17], new_landmarks[33:34])
            l_jaw = (new_landmarks[16:33], new_landmarks[42:43])
            r_cheek = (new_landmarks[33:36], new_landmarks[16:17])
            l_cheek = (new_landmarks[40:43], new_landmarks[16:17])
            nose_ridge = (new_landmarks[35:41], new_landmarks[16:17])
            r_eye = (new_landmarks[33:38], new_landmarks[43:44], new_landmarks[47:52], new_landmarks[82:84], new_landmarks[16:17])
            l_eye = (new_landmarks[38:43], new_landmarks[43:44], new_landmarks[47:52], new_landmarks[82:84], new_landmarks[16:17])
            nose = (new_landmarks[43:52], new_landmarks[80:84])
            parts = [r_jaw, l_jaw, r_cheek, l_cheek, nose_ridge, r_eye, l_eye, nose]

            for item in parts:
                merged = np.concatenate(item).astype(np.int32)
                cv2.fillConvexPoly(mask, cv2.convexHull(merged), color)
        else:
            new_landmarks = np.array(landmarks.copy(), dtype=np.float32)
            for i in range(0,44):
                outline = [int(landmarks[i,0]), int(landmarks[i,1])]
                parts.append(outline)    
                
            parts = np.array(parts)
            filler = cv2.convexHull(parts)
            cv2.fillConvexPoly(mask, filler, 1)

        return mask

  
def expand_eyebrows_func(landmarks, eyebrows_expand_mod=1.0):
    landmarks = np.array(landmarks.copy(), dtype=np.float32)

    #
    ml_pnt = (landmarks[52] + landmarks[0]) / 2
    mr_pnt = (landmarks[61] + landmarks[31]) / 2
    ql_pnt = (landmarks[52] + ml_pnt) / 2
    qr_pnt = (landmarks[61] + mr_pnt) / 2

    # Top of the eye arrays
    bot_l = np.array((ql_pnt, landmarks[52], landmarks[53], landmarks[54], landmarks[55]))
    bot_r = np.array((landmarks[58], landmarks[59], landmarks[60], landmarks[61], qr_pnt))
    top_l = landmarks[33:38]
    top_r = landmarks[38:43]

    landmarks[33:38] = top_l + eyebrows_expand_mod * 0.5 * (top_l - bot_l)
    landmarks[38:43] = top_r + eyebrows_expand_mod * 0.5 * (top_r - bot_r)

    return landmarks

def get_mask(image_shape, landmarks, color=(1,), expand_eyebrows=False):
    mask = np.zeros(image_shape[0:2] + (1,), dtype=np.float32)
    parts = []
    if(expand_eyebrows):
        new_landmarks = expand_eyebrows_func(landmarks, 1.0)
        mask = np.zeros(image_shape[0:2] + (1,), dtype=np.float32)

        r_jaw = (new_landmarks[0:17], new_landmarks[33:34])
        l_jaw = (new_landmarks[16:33], new_landmarks[42:43])
        r_cheek = (new_landmarks[33:36], new_landmarks[16:17])
        l_cheek = (new_landmarks[40:43], new_landmarks[16:17])
        nose_ridge = (new_landmarks[35:41], new_landmarks[16:17])
        r_eye = (new_landmarks[33:38], new_landmarks[43:44], new_landmarks[47:52], new_landmarks[82:84], new_landmarks[16:17])
        l_eye = (new_landmarks[38:43], new_landmarks[43:44], new_landmarks[47:52], new_landmarks[82:84], new_landmarks[16:17])
        nose = (new_landmarks[43:52], new_landmarks[80:84])
        parts = [r_jaw, l_jaw, r_cheek, l_cheek, nose_ridge, r_eye, l_eye, nose]

        for item in parts:
            merged = np.concatenate(item).astype(np.int32)
            cv2.fillConvexPoly(mask, cv2.convexHull(merged), color)
    else:
        new_landmarks = np.array(landmarks.copy(), dtype=np.float32)
        for i in range(0,44):
            outline = [int(landmarks[i,0]), int(landmarks[i,1])]
            parts.append(outline)    
            
        parts = np.array(parts)
        filler = cv2.convexHull(parts)
        cv2.fillConvexPoly(mask, filler, 1)

    return mask

    
