#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 25 14:56:46 2019

align method v4 using smash

detailed explanation can be found in https://bytedance.feishu.cn/docs/doccnCvmiIEPVcQHvqymVal7tCb#HyAFdY

@author: huxinghong
"""



import sys
import numpy as np
import cv2
from .utils.matlab_cp2tform import get_similarity_transform, cvt_tform_mat_for_cv2
import os
import math
from .face_alignment import face_alignment_outline as fao
from .face_alignment.face_alignment_outline import get_mask
from .face_alignment import get_face_attribute as faceAttr
from .bg_crop.crop_bg import PortraitMatting
from PIL import Image, ImageFilter
import time


IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]


def get_eye_mask(cv_img, lmks, rec=0):
    mask = np.zeros(cv_img.shape[:-1],np.uint8)

    parts1 = []
    parts2 = []
    for i in range(52,58):
        outline1 = [int(lmks[i,0]), int(lmks[i,1])]
        outline2 = [int(lmks[i+6,0]), int(lmks[i+6,1])]
        parts1.append(outline1)
        parts2.append(outline2)

    parts1 = np.array(parts1)
    parts2 = np.array(parts2)
    filler1 = cv2.convexHull(parts1)
    filler2 = cv2.convexHull(parts2)
    cv2.fillConvexPoly(mask, filler1, 1)
    cv2.fillConvexPoly(mask, filler2, 1)
    return mask

def adjust_gamma(image, gamma=1.0):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
    return cv2.LUT(image,table)

def get_face_mask(cv_img, lmks, expand_eyebrows=False):
    mask = get_mask(cv_img.shape[:2], lmks, expand_eyebrows=expand_eyebrows)
    return mask

def get_halfbottom_mask(cv_img, lmks):
    mask = np.zeros(cv_img.shape[:-1],np.uint8)
    parts = []
    for i in range(7,26):
        outline = [int(lmks[i,0]), int(lmks[i,1])]
        parts.append(outline)
    parts.append([int(lmks[45,0]), int(lmks[45,1])])

    parts = np.array(parts)
    filler = cv2.convexHull(parts)
    cv2.fillConvexPoly(mask, filler, 1)
   
    return mask

def get_eyebrow_mask(cv_img, lmks, rec=0, inpaint_by_skimage=False):
   mask_right = np.zeros(cv_img.shape[:-1],np.uint8) 
   mask_left = np.zeros(cv_img.shape[:-1],np.uint8) 
            
   parts1 = []
   parts2 = []
   for i in range(33,38):
      outline1 = [int(lmks[i,0]), int(lmks[i,1])]
      outline2 = [int(lmks[i+5,0]), int(lmks[i+5,1])]
      parts1.append(outline1)    
      parts2.append(outline2)
   for i in range(67,63,-1):
      outline1 = [int(lmks[i,0]), int(lmks[i,1])]
      outline2 = [int(lmks[i+4,0]), int(lmks[i+4,1])]
      parts1.append(outline1)    
      parts2.append(outline2)
      
   parts1 = np.array(parts1)
   parts2 = np.array(parts2)
   filler1 = cv2.convexHull(parts1)
   filler2 = cv2.convexHull(parts2)
   cv2.fillConvexPoly(mask_left, filler1, 1)
   cv2.fillConvexPoly(mask_right, filler2, 1)

   return mask_left, mask_right

def get_mouth_mask(cv_img, lmks):
    mask = np.zeros(cv_img.shape[:-1],np.uint8)
    parts = []
    for i in range(84,96):
        outline = [int(lmks[i,0]), int(lmks[i,1])]
        parts.append(outline)

    parts = np.array(parts)
    filler = cv2.convexHull(parts)
    cv2.fillConvexPoly(mask, filler, 1)
    return mask

def pil_to_cv2(pil_img):
    numpy_image=np.array(pil_img)
    cv2_image=cv2.cvtColor(numpy_image, cv2.COLOR_RGB2BGR)
    return cv2_image

def cv2_to_pil(cv_img):
    image = Image.fromarray(cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB))
    return image

def exist_black(cv_img, check_thresh = 0.005):
    h,w,c = cv_img.shape
    lighten_image = adjust_gamma(cv_img, gamma=3)
    black_count = lighten_image[:,:,0]+lighten_image[:,:,1]+lighten_image[:,:,2]
    
    if np.sum(black_count == 0)>check_thresh*h*w:
        return 1
    else:
        return 0
    
def draw_by_map(cv_img, border_map):
#    start_time = time.time()
    outer_mask = (border_map.astype(np.float32)-127)/127
    outer_mask[outer_mask<0]=0
    outer_mask[outer_mask>1]=1
    inner_mask = (127-border_map.astype(np.float32))/127
    inner_mask[inner_mask<0]=0
    inner_mask[inner_mask>1]=1
    
    new_img = np.zeros(cv_img.shape, np.float32)
    
    zero_img = np.zeros(cv_img.shape, np.float32)
    one_img = np.ones(cv_img.shape, np.float32)*255
    new_img = outer_mask*one_img+inner_mask*cv_img.astype(np.float32)+(1-outer_mask-inner_mask)*zero_img
    new_img[new_img>255] = 255
    new_img[new_img<0] = 0
    
#    print(time.time()-start_time)
    
    return new_img.astype(np.uint8)

def get_nose_mask(cv_img, lmks):
    mask = np.zeros(cv_img.shape[:-1],np.uint8) 
    parts = []
    parts.append([int(lmks[55,0]), int(lmks[55,1])]) 
    parts.append([int(lmks[82,0]), int(lmks[82,1])]) 
    parts.append([int(lmks[49,0]), int(lmks[49,1])]) 
    parts.append([int(lmks[83,0]), int(lmks[83,1])]) 
    parts.append([int(lmks[58,0]), int(lmks[58,1])]) 
    parts.append([int(lmks[43,0]), int(lmks[43,1])]) 
           
    parts = np.array(parts)
    filler = cv2.convexHull(parts)
    cv2.fillConvexPoly(mask, filler, 1)
    return mask
 
def get_average_color(cv_img, mask):
    ave_color = np.zeros(3)
#    print(cv_img.shape[-1])
    for i in range(cv_img.shape[-1]):
        channel = cv_img[:,:,i]
        ave_color[i] = np.mean(channel[mask>0.5])
    return ave_color

def get_average_lumi(cv_img, mask):
    lumi_img = cv2.cvtColor(cv_img, cv2.COLOR_BGR2GRAY)
    if len(mask>0)==0:
        return 255
    return np.mean(lumi_img[mask>0])


def get_black_class(mean_lumi, spec_class=[255,110,0]):
#    print(mean_lumi)
    for i in range(len(spec_class)-1):
        if mean_lumi<=spec_class[i] and mean_lumi>spec_class[i+1]:
            return 3+i
    return 2+len(spec_class)


        
def read_lut_file(path):
    lut = cv2.imread(path)
    lut = cv2.cvtColor(lut, cv2.COLOR_BGR2RGB).astype(np.float32)
 
    lut_table = np.zeros((64*64*64,3), np.uint8)
    for j in range(8):
        for i in range(8):
            cur_block = lut[64*j:64*(j+1), 64*i:64*(i+1),:]
            cur_block = cur_block.reshape(64*64,3)
            lut_table[(j*8+i)*64*64:(j*8+i+1)*64*64,:] = cur_block

    lut_table = lut_table / 255.0
    return lut_table


def brazil_lut(ori_img, face_mask, bg_lut_img, skin_type, luts):
    face_mask = face_mask.astype(np.float32)
    lut_face_image  = Image.fromarray(cv2.cvtColor(ori_img.astype(np.uint8), cv2.COLOR_BGR2RGB))
    lut_face_image = lut_face_image.filter(luts[skin_type+5])
    lut_face_image = cv2.cvtColor(np.array(lut_face_image), cv2.COLOR_RGB2BGR).astype(np.float32)    
    for i in range(3):
        bg_lut_img[:,:,i][face_mask[:,:,i]>0] = lut_face_image[:,:,i][face_mask[:,:,i]>0]
    return bg_lut_img

lut_func = {1 : brazil_lut,}


def apply_racial_lut(image, luts, skin_type, face_mask, country):
   
    lut_image  = Image.fromarray(cv2.cvtColor(image.astype(np.uint8), cv2.COLOR_BGR2RGB))
    lut_image = lut_image.filter(luts[skin_type])
    lut_image = cv2.cvtColor(np.array(lut_image), cv2.COLOR_RGB2BGR).astype(np.float32)    
    
    if country==1:
        lut_image = lut_func[country](image, face_mask, lut_image, skin_type, luts)

    return lut_image

out_dir = './'
gamma_cor = 1 #脸部gamma矫正, gamma correction for faces
reflect_pad = 0 #对黑边的处理, how to deal with boundary

resize=1 #裁剪前是否降采样, resize image before being processed
resize_bound = 512 #降采样最短边, shortest length of the resized image
target_size = 384 #输出分辨率, target size 
out_face = 1 #使用外轮廓点进行gamma, using face outlines for gamma correction
check_black = 0 #是否做黑边检测, whether to check black boundary

hair_thresh = 1
hair_thresh_strategy = 2 #1:fix threshold  2:determine threshold by brow color
nose_tgt_y = 0.5*target_size
nose_tgt_x = 0.5*target_size
change_ratio = target_size/256
border_width = 3
eye_ratio = 4.5

class LandDetector():
    def __init__(self, hair_thresh=70, hair_thresh_strategy=2):
        self.hair_thresh = hair_thresh
        self.hair_thresh_strategy = hair_thresh_strategy
        
    
        model_path = './model/tt_face_v7.0.model'.encode()
        extra_path = './model/tt_face_extra_v10.0.model'.encode()
        land_lib_path = './align_v4_linux/lib/land.so'
        fo_model_path = './model/tt_face_new_landmark_v1.0.model'.encode()
        fo_lib_path = './align_v4_linux/lib/facenewlandmark.so'
        attr_model_path = './model/tt_face_attribute_v5.0.model'.encode()
        attr_extra_path = './model/tt_face_attribute_extra_v2.0.model'.encode()
        attr_lib_path = './align_v4_linux/lib/faceattribute.so'
        bg_model_path = './align_v4_linux/bg_crop/tt_matting_v9.0.model'.encode()
        bg_lib_path = './align_v4_linux/bg_crop/portraitmatting.so'
        
       
        self.portrait_matting =  PortraitMatting(bg_model_path, bg_lib_path)
        
        if out_face == 1:
            self.face_alignment_outline = fao.FaceAlignment(platform='Linux', model_path=model_path, extra_path=extra_path,
                                                            land_lib_path=land_lib_path, pad=[0.4, 0.4, 0.8, 1.2], use_detection=False)
            self.face_outlines = fao.FaceOutlines(platform='Linux', model_path=fo_model_path, faceoutlines_lib_path=fo_lib_path,smooth_mode=0)

        self.face_detection_fast = fao.FaceDetection(platform='Linux', model_path=model_path, extra_path=extra_path,
                                                     land_lib_path=land_lib_path, slow_mode=False)
        self.face_attr = faceAttr.FaceAttribute(attr_model_path,  attr_extra_path, attr_lib_path, slow_mode=True)   
       
    
    def clip(self, cv_img, bg_crop=0, bg_color=0):
#        times = {"downscale time":0 , "detection time":0, "alignment time":0, "gamma time":0, "blur time":0}
        if bg_crop==1:
            try:
                cv_img_nobgcrop = cv_img.copy()
                ret, res_img = self.portrait_matting.detect(cv_img)
                print(np.max(res_img))
                res_img = np.tile(res_img[:,:,None], (1,1,3)).astype(np.uint8)
                cv_img[res_img==0] = bg_color
                
            except:
                res_img = np.zeros(cv_img.shape).astype(np.uint8)
                print("no bg detected")
        else:
            cv_img_nobgcrop = None
            res_img=None
        height, width, channels = cv_img.shape
        ori_cv_img = cv_img.copy()
        resize_factor = 1
        # 降采样 
        # resizing
        if resize == 1:
            pil_img = cv2_to_pil(cv_img)
            if height > resize_bound and height <= width:
                resize_factor = resize_bound / height
                width = int(resize_bound / height * width)
                height = resize_bound
                if bg_crop==1:
                    res_img = cv2.resize(res_img, (width, height), interpolation=cv2.INTER_AREA)
                    cv_img_nobgcrop = cv2.resize(cv_img_nobgcrop, (width, height), interpolation=cv2.INTER_AREA)
       
#                cv_img = cv2.resize(cv_img, (width, height), interpolation=cv2.INTER_AREA)
                pil_img = pil_img.resize((width, height), Image.ANTIALIAS)
                cv_img = pil_to_cv2(pil_img)
            elif width > resize_bound and width < height:
                resize_factor = resize_bound / width
                height = int(resize_bound / width * height)
                width = resize_bound
#                cv_img = cv2.resize(cv_img, (width, height), interpolation=cv2.INTER_AREA)
                if bg_crop==1:
                    res_img = cv2.resize(res_img, (width, height), interpolation=cv2.INTER_AREA)
                    cv_img_nobgcrop = cv2.resize(cv_img_nobgcrop, (width, height), interpolation=cv2.INTER_AREA)
       
                pil_img = pil_img.resize((width, height), Image.ANTIALIAS)
                cv_img = pil_to_cv2(pil_img)

        # 轮廓点检测
        # landmark detection
        time_st = time.time()
        count, lms = self.face_detection_fast.detect106(cv_img, 'BGR')
        print("detection time inner: "+str(time.time()-time_st))
        if count <= 0:
            print("No face detected!")
            return 0, None, None, None, None
        if out_face == 1:
            fo_lms = self.face_outlines.detect(cv_img, landmarks=lms, use_rgba=False)
            fo_lmks = fo_lms.reshape((-1, 2))
        lmks = lms.reshape((-1, 2))

#        if country==0: #china
#            skin_type = 2
#        else:
        time_st = time.time()
        ret, skin_type = self.face_attr.detect(cv_img, landmarks=lms, count=count, use_rgba=False)
        print("skin detect time "+str(time.time()-time_st))
#            if ret == 0:
#                skin_type = 2
        
        # alignment
        left_eye_x, left_eye_y = lmks[74, 0], lmks[74, 1]
        right_eye_x, right_eye_y = lmks[77, 0], lmks[77, 1]

        eye_middle_x = (left_eye_x + right_eye_x) / 2
        eye_middle_y = (left_eye_y + right_eye_y) / 2

        nose_x = lmks[45, 0]
        nose_y = lmks[45, 1]

        eye_to_eye = math.sqrt((left_eye_x - right_eye_x) ** 2 + (left_eye_y - right_eye_y) ** 2)

        crop_size = eye_to_eye * eye_ratio
        print(("eye_ratio", eye_ratio))
        factor = target_size / crop_size

        eye_to_nose = math.sqrt((nose_x - eye_middle_x) ** 2 + (nose_y - eye_middle_y) ** 2)
        cur_left_eye_x = left_eye_x * factor
        cur_left_eye_y = left_eye_y * factor
        cur_right_eye_x = right_eye_x * factor
        cur_right_eye_y = right_eye_y * factor

        tar_eye_y = 0.51 - eye_to_nose / (eye_to_eye * eye_ratio)
        tar_left_eye_x = (1-1/eye_ratio)/2
        tar_right_eye_x = 1-(1-1/eye_ratio)/2

        matRotate = cv2.getRotationMatrix2D((0, 0), 0, factor)
        warp_img = cv2.warpAffine(cv_img, matRotate, (int(width * factor), int(height * factor)))
        if bg_crop==1: 
            res_img = cv2.warpAffine(res_img, matRotate, (int(width * factor), int(height * factor)))
            cv_img_nobgcrop = cv2.warpAffine(cv_img_nobgcrop, matRotate, (int(width * factor), int(height * factor)))
      

        fixed_points = np.array(
            [[tar_left_eye_x, tar_right_eye_x, 0.5], [tar_eye_y, tar_eye_y, 0.51]]).transpose() * target_size
        target_lmks = np.array([[cur_left_eye_x, cur_right_eye_x, nose_x * factor],
                                [cur_left_eye_y, cur_right_eye_y, nose_y * factor]]).transpose()
        trans, trans_inv = get_similarity_transform(target_lmks.copy(), fixed_points, reflective=True)

        cv2_trans = cvt_tform_mat_for_cv2(trans)

      
        warp_img = cv2.warpAffine(warp_img, cv2_trans, (target_size, target_size))
        if bg_crop==1:
            res_img = cv2.warpAffine(res_img, cv2_trans, (target_size, target_size))
            cv_img_nobgcrop = cv2.warpAffine(cv_img_nobgcrop, cv2_trans, (target_size, target_size))
        cv2_trans_large = cv2_trans.copy()
   
        cv2_trans_large[0,2] = cv2_trans_large[0,2]/factor/resize_factor
        cv2_trans_large[1,2] = cv2_trans_large[1,2]/factor/resize_factor

        ori_face = cv2.warpAffine(ori_cv_img, cv2_trans_large, (int(target_size/factor/resize_factor), 
                                                                int(target_size/factor/resize_factor)),borderValue=(255,255,255))
        
        
        #计算边框mask
        #compute border mask
        border_map = np.zeros(cv_img.shape, np.uint8)
        border_map = cv2.warpAffine(border_map, matRotate, (int(width * factor), int(height * factor)))
        border_map[:border_width,:] = 127
        border_map[-border_width:,:] = 127
        border_map[:,:border_width] = 127
        border_map[:,-border_width:] = 127
        border_map = cv2.warpAffine(border_map, cv2_trans, (target_size, target_size),borderValue=(255,255,255))
        
        real_border_index = np.where((border_map[0,:,0]<=127))
        border_map[0:border_width,real_border_index] = 127
        real_border_index = np.where((border_map[:,0,0]<=127))
        border_map[real_border_index,0:border_width] = 127
        real_border_index = np.where((border_map[-1,:,0]<=127))
        border_map[-border_width:,real_border_index] = 127
        real_border_index = np.where((border_map[:,-1,0]<=127))
        border_map[real_border_index, -border_width:] = 127
        
       
        # 计算新lmk点
        # compute new landmarks
        lmks_final = np.matmul(lmks, matRotate[:2, :2].transpose())
        z_dim_lmks = np.ones(lmks_final.shape[0])
        lmks_final = np.column_stack((lmks_final, z_dim_lmks))
        lmks_final = np.matmul(lmks_final, cv2_trans.transpose())
        
        if out_face==1:
            fo_lmks_final = np.matmul(fo_lmks, matRotate[:2, :2].transpose())
            z_dim_fo_lmks = np.ones(fo_lmks_final.shape[0])
            fo_lmks_final = np.column_stack((fo_lmks_final, z_dim_fo_lmks))
            fo_lmks_final = np.matmul(fo_lmks_final, cv2_trans.transpose())
        
        warp_img_ori = warp_img.copy()
        if gamma_cor == 1:
            gray_img = cv2.cvtColor(warp_img, cv2.COLOR_BGR2GRAY)
            gray_img2 = gray_img.copy()

            # detect face region
            gaussian_kernel = int(15 * change_ratio)
            gaussian_kernel_eye = int(11 * change_ratio)
            if gaussian_kernel % 2 == 0:
                gaussian_kernel = gaussian_kernel + 1
            if gaussian_kernel_eye % 2 == 0:
                gaussian_kernel_eye = gaussian_kernel_eye + 1

            if out_face == 0:
                face_mask = get_face_mask(warp_img, lmks_final,  expand_eyebrows=True)
            else:
                face_mask = get_face_mask(warp_img, fo_lmks_final)
                
#            skin_mask = get_halfbottom_mask(warp_img, lmks_final)
#            skin_mask = skin_mask - get_mouth_mask(warp_img, lmks_final)
#        
#            skin_mask = (255-skin_mask*255).astype(np.uint8)
#            skin_mask = np.tile(skin_mask[:,:,None],(1,1,3))
              
            face_mask = (face_mask * 255).astype(np.uint8)
           

            face_mask_smooth = cv2.GaussianBlur(face_mask, (gaussian_kernel, gaussian_kernel), 0)
     
           
            face_mask_smooth = face_mask_smooth.astype(np.float32) / 255

            # exclude mouth and eye
            mouth_mask = get_mouth_mask(warp_img, lmks_final)
            mouth_mask = (mouth_mask * 255).astype(np.uint8)
            mouth_mask_smooth = cv2.GaussianBlur(mouth_mask, (gaussian_kernel, gaussian_kernel), 0)
            mouth_mask_smooth = mouth_mask_smooth.astype(np.float32) / 255
            eye_mask = get_eye_mask(warp_img, lmks_final)

            eye_mask_smooth = cv2.GaussianBlur(eye_mask * 255, (gaussian_kernel_eye, gaussian_kernel_eye),
                                               0)  # v4_eye15
            eye_mask_smooth[eye_mask_smooth > 0] = 255
            eye_mask_smooth = cv2.GaussianBlur(eye_mask_smooth, (gaussian_kernel, gaussian_kernel), 0)
            
            eye_mask_smooth = eye_mask_smooth.astype(np.float32) / 255
            

            face_mask_smooth = face_mask_smooth - mouth_mask_smooth - eye_mask_smooth
            
           
           

            # exclude hair region
            eye_y = lmks_final[52, 1]
            for i in range(52, 64):
                eye_y = np.min((eye_y, int(lmks_final[i, 1])))
            eye_y = int(eye_y)
            gray_img[eye_y:, :] = 255
            
            if self.hair_thresh_strategy==2:
                eyebrow_mask_l, eyebrow_mask_r = get_eyebrow_mask(warp_img, lmks_final)
                hair_thresh_l = np.mean(gray_img[eyebrow_mask_l>0].astype(np.float32))
                hair_thresh_r = np.mean(gray_img[eyebrow_mask_r>0].astype(np.float32))
                hair_thresh = np.max((hair_thresh_l, hair_thresh_r, self.hair_thresh))
            else:
                hair_thresh = self.hair_thresh


            face_mask_smooth[gray_img < hair_thresh] = 0
            face_mask_smooth = cv2.GaussianBlur(face_mask_smooth, (gaussian_kernel, gaussian_kernel), 0)
            
          
            
            # gamma correction for the whole image
            lighten_image = adjust_gamma(warp_img, gamma=1.5)
            out_img = warp_img.copy()
            # weighted sum
            for i in range(3):
                out_img[:, :, i] = lighten_image[:, :, i] * face_mask_smooth + out_img[:, :, i] * (1 - face_mask_smooth)
            out_img[out_img > 255] = 255
            out_img[out_img < 0] = 0
            warp_img = out_img.astype(np.uint8)
            face_mask = np.tile(face_mask,(1,1,3))     
        return 1, warp_img, warp_img_ori, res_img, cv_img_nobgcrop




if __name__ == "__main__":
#    if len(sys.argv)>2:
#        print("No input image path")
#        #sys.exit(0)
    if len(sys.argv)>1:
        land = LandDetector(hair_thresh=hair_thresh, hair_thresh_strategy=hair_thresh_strategy)
        cv_img = cv2.imread(sys.argv[1])
        start = time.time()
        _, warp_img, ori_face, border_map = land.clip(cv_img)
        print(time.time()-start)
#        for x,y in times.items():
#            print(x,str(y))
        cv2.imwrite(os.path.join(out_dir,'test_out.jpg'), warp_img)
        cv2.imwrite(os.path.join(out_dir,'test_out_large.jpg'), ori_face)
        cv2.imwrite(os.path.join(out_dir,'map.jpg'), border_map)
    else:
        try:
            
            cv_img = cv2.imread("test3.jpg")#cv2.imread(sys.argv[1])
            #pdb.set_trace()
            land = LandDetector(hair_thresh=hair_thresh, hair_thresh_strategy=hair_thresh_strategy)
            start = time.time()
            _, warp_img, ori_face, border_map = land.clip(cv_img)
            print(time.time()-start)
            #写入图片
            #writing images
            print("saving output image to "+os.path.join(out_dir,'test_out.jpg'))
            cv2.imwrite(os.path.join(out_dir,'test_out.jpg'), warp_img)
            cv2.imwrite(os.path.join(out_dir,'test_out_large.jpg'), ori_face)
            cv2.imwrite(os.path.join(out_dir,'map.jpg'), border_map)
        except Exception as e:
            print(e)
           

