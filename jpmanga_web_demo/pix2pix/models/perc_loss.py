import torchvision.models as models
import torch
import torch.nn as nn
  
from collections import namedtuple

LossOutput = namedtuple("LossOutput", ["relu1_2", "relu2_2", "relu3_3", "relu4_3"])

class LossPercVgg16(nn.Module):
    def __init__(self, requires_grad=False):
        super(LossPercVgg16, self).__init__()
        vgg16 = models.vgg16(pretrained=True)
        self.vgg_layers = vgg16.features
        self.layer_name_mapping = {'3' : 'relu1_2',
                                   '8' : 'relu2_2',
                                   '15' : 'relu3_3',
                                   '22' : 'relu4_3'}

        if not requires_grad:
            for param in self.parameters():
                param.requires_grad = False

    def forward(self, x):
        output = {}
        for name, module in self.vgg_layers._modules.items():
            x = module(x)
            if name in self.layer_name_mapping:
                output[self.layer_name_mapping[name]] = x
        return LossOutput(**output)

def feat_loss(feat_A, feat_B, criterion, feat_names=["relu2_2"]):
    loss = 0
    for feat_name in feat_names:
        _, ch, h, w = getattr(feat_A, feat_name).size()
        loss += criterion(getattr(feat_A, feat_name), getattr(feat_B, feat_name)) / (ch * w * h)
    return loss

def gram_matrix(y):
    b, ch, h, w = y.size()
    features = y.view(b, ch, w * h)
    features_t = features.transpose(1, 2)
    gram = features.bmm(features_t) / (ch * h * w)
    return gram

def gram_loss(feat_A, feat_B, criterion, feat_names=["relu1_2", "relu2_2", "relu3_3", "relu4_3"]):
    loss = 0
    for feat_name in feat_names:
        gram_A = gram_matrix(getattr(feat_A, feat_name))
        gram_B = gram_matrix(getattr(feat_A, feat_name))
        loss += criterion(gram_A, gram_B)

    return loss