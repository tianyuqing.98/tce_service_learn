#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 24 02:41:32 2018

@author: hu
"""

import torch
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
from math import exp
import torchvision.models.vgg as vgg
import torch.nn as nn
import torch.utils.model_zoo as model_zoo


class VGG19_bn(torch.nn.Module):
    def __init__(self, opt):
        super(VGG19_bn, self).__init__()
        
#        features = models.vgg19_bn(pretrained=True).features
        model = vgg.VGG(vgg.make_layers(vgg.cfgs['E'], batch_norm=True))
        model.load_state_dict(model_zoo.load_url(vgg.model_urls['vgg19_bn'],model_dir=opt.vgg_model))
        features = model.features
        
        self.relu1_1 = torch.nn.Sequential()
        self.relu1_2 = torch.nn.Sequential()

        self.relu2_1 = torch.nn.Sequential()
        self.relu2_2 = torch.nn.Sequential()

        self.relu3_1 = torch.nn.Sequential()
        self.relu3_2 = torch.nn.Sequential()
        self.conv3_3 = torch.nn.Sequential()
        self.relu3_3 = torch.nn.Sequential()
        self.relu3_4 = torch.nn.Sequential()

        self.relu4_1 = torch.nn.Sequential()
        self.relu4_2 = torch.nn.Sequential()
        self.relu4_3 = torch.nn.Sequential()
        self.conv4_4 = torch.nn.Sequential()
        self.relu4_4 = torch.nn.Sequential()


        for x in range(3):
            self.relu1_1.add_module(str(x), features[x])

        for x in range(3, 6):
            self.relu1_2.add_module(str(x), features[x])

        for x in range(6, 10):
            self.relu2_1.add_module(str(x), features[x])

        for x in range(10, 13):
            self.relu2_2.add_module(str(x), features[x])

        for x in range(13, 17):
            self.relu3_1.add_module(str(x), features[x])

        for x in range(17, 20):
            self.relu3_2.add_module(str(x), features[x])

        #split conv3_3 and relu3_3
        self.conv3_3.add_module(str(20), features[20])
        for x in range(21, 23):
            self.relu3_3.add_module(str(x), features[x])

        for x in range(23, 26):
            self.relu3_4.add_module(str(x), features[x])

        for x in range(26, 30):
            self.relu4_1.add_module(str(x), features[x])

        for x in range(30, 33):
            self.relu4_2.add_module(str(x), features[x])

        for x in range(33, 36):
            self.relu4_3.add_module(str(x), features[x])

        # split conv and bn/relu
        self.conv4_4.add_module(str(36), features[36])
       

        for param in self.parameters():
            param.requires_grad = False

    def forward(self, x):
        relu1_1 = self.relu1_1(x)
        relu1_2 = self.relu1_2(relu1_1)

        relu2_1 = self.relu2_1(relu1_2)
        relu2_2 = self.relu2_2(relu2_1)

        relu3_1 = self.relu3_1(relu2_2)
        relu3_2 = self.relu3_2(relu3_1)
        conv3_3 = self.conv3_3(relu3_2)
        relu3_3 = self.relu3_3(conv3_3)
        relu3_4 = self.relu3_4(relu3_3)

        relu4_1 = self.relu4_1(relu3_4)
        relu4_2 = self.relu4_2(relu4_1)
        relu4_3 = self.relu4_3(relu4_2)
        conv4_4 = self.conv4_4(relu4_3)
        
        return conv4_4


class ContentLoss(nn.Module):
    def __init__(self, opt):
        super(ContentLoss, self).__init__()
   
        self.vgg = VGG19_bn(opt)
        self.criterionL1 = nn.L1Loss()
      
    def __call__(self, x, y):
        x, y = (x+1.)/2. , (y+1)/2.
        y_vgg, x_vgg = self.vgg(y), self.vgg(x)
        content_loss = self.criterionL1(x_vgg, y_vgg)
      
        return content_loss