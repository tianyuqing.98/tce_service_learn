#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 18:41:41 2019

@author: huxinghong
"""


import torch.nn as nn
from . import net_canny


class EdgeLoss(nn.Module):
    def __init__(self, use_cuda=False):
        super(EdgeLoss, self).__init__()
        
        self.criterionL1 = nn.L1Loss()
      
        self.cannyNet = net_canny.Net(threshold=1.0, use_cuda=use_cuda)
      
    def __call__(self, tensor_img1, tensor_img2):
        tensor_img1 = (tensor_img1+1.0)/2.0
        tensor_img2 = (tensor_img2+1.0)/2.0
        
        edge_img1, edge_img2 = self.canny(tensor_img1), self.canny(tensor_img2)
        edge_loss = self.criterionL1(edge_img1, edge_img2)
      
        return edge_loss
    
    def canny(self, tensor_img):
        blurred_img, grad_mag, grad_orientation, thin_edges, thresholded, early_threshold = self.cannyNet(tensor_img)
        final_thresh = thresholded>0
        final_thresh = final_thresh.float()
        return final_thresh