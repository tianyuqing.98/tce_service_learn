import os, cv2
from options.test_options import TestOptions
from models import create_model
import torch
from util import util
from PIL import Image
from data.base_dataset import get_params, get_transform
import numpy as np
import pdb

model_names = ['model1', ]
class Cartooner():
    def __init__(self):
        self.model = []
        print(getOpt(model_names[0]))
        self.model.append(create_model(getOpt(model_names[0], True)))
        #self.model.append(create_model(getOpt(model_names[1], True)))
        #self.model.append(create_model(getOpt(model_names[2], True)))
#        self.model = create_model(opt)
        
        
        for i in range(1):
            self.model[i].setup(getOpt(model_names[i]))
            self.model[i].eval()
        self.test_init()
        
    def test_init(self):
        im = cv2.imread("./test.jpg")
        clip_img_rgb = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        out_img = self.transform(clip_img_rgb, 0)
        cv2.imwrite("output.jpg", out_img)
      
    def transform(self, clipped_image, model_num):
        input_tensor = torch.from_numpy(clipped_image).unsqueeze(0).type(torch.FloatTensor)
#        input = torch.from_numpy(clipped_image).permute(2, 0, 1).unsqueeze(0)
        with torch.no_grad():
            self.model[model_num].forward1(input_tensor)
  
        output = util.tensor2im(self.model[model_num].fake_B)
        return output

    def transform_batch(self, clipped_images):
        batch_size = len(clipped_images)
        #print(clipped_images[0].shape)
        '''
        inputs = torch.zeros(batch_size, 3, 384, 384)
        for i, img in enumerate(clipped_images):
            input = torch.from_numpy(img).permute(2, 0, 1)
            inputs[i] = input
        '''
        inputs = torch.from_numpy(np.stack(clipped_images))
        #print(inputs.shape)
        with torch.no_grad():
            self.model.forward1(inputs)

        #print(self.model.fake_B.shape)
        output = []
        for i in range(batch_size):
            fake_b = (self.model.fake_B[i].permute(1, 2, 0) + 1.0) * 127.5
            fake_b = torch.flip(fake_b, (2,))
            image_numpy = fake_b.cpu().float().numpy()
            #image_numpy = self.model.fake_B[i].cpu().float().numpy()
            #image_numpy = (np.transpose(image_numpy, (1, 2, 0)) + 1.0) * 127.5
            #output.append(image_numpy.astype(np.uint8))
            #print(image_numpy.shape)
            output.append(image_numpy) # CHW, (-1, 1)
        #output = util.tensor2im(self.model.fake_B)
        return output

    
        
def getOpt(name, no_dropout=False):
    opt = TestOptions().parse()
    #opt.gpu_ids = '0'
    opt.num_threads = 0
    opt.batch_size = 1
    opt.serial_batches = True
    opt.no_flip = True
    opt.display_id = -1
    opt.name = name
    opt.load_size = 384
    opt.crop_size = 384
    opt.model = 'cycle_gan'
    opt.direction = 'AtoB'
    opt.netG = 'resnet_9blocks'
    opt.norm = 'instance'
    opt.checkpoints_dir = './pix2pix/checkpoints'
    opt.no_dropout = no_dropout
    opt.phase = 'test'
    return opt

if __name__ == '__main__':
    #pdb.set_trace()

    opt = getOpt()
    #opt.print_options(opt)
    print(opt.direction)
    
    cartoon = Cartooner(opt)
    image1 = cv2.cvtColor(cv2.imread('clip.png'), cv2.COLOR_BGR2RGB)
    image1 = image1.astype(np.float32) / 127.5 - 1.0

    image2 = cv2.cvtColor(cv2.imread('clip.png'), cv2.COLOR_BGR2RGB)
    image2 = image2.astype(np.float32) / 127.5 - 1.0
    #image = Image.open('test.png').convert('RGB')
    #clip = self.detector.clip(image)
    #cv2.imwrite('clip.png', clip)
    images = [image1, image2]
    output = cartoon.transform_batch(images) 
#    print(output[0].shape)

    img_np = (np.transpose(output[0], (1, 2, 0)) + 1.0) * 127.5
    img_np = img_np.astype(np.uint8)


    cv2.imwrite('out.png', img_np)
    #util.save_image(output[0], 'out.png')

    #cv_img = cv2.imencode('.jpg', output)[1].tostring()
    #cv2.imwrite('cv.png', cv_img)
    #open('cv.png', 'wb').write(Image.fromarray(output))

