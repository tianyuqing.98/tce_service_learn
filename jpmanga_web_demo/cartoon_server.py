#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 12:38:55 2020

@author: huxinghong
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 21:36:21 2020

@author: huxinghong
"""

from threading import Thread
import struct
import time
import hashlib
import base64
import socket
import types
import multiprocessing
import os
import sys
import cv2
import base64
import json
import numpy as np
import decimal
import demjson
import torch
import http.server
from http.server import HTTPServer, BaseHTTPRequestHandler
from cgi import parse_header, parse_multipart
from urllib.parse import parse_qs
import zipfile
from align_v4_linux.align_single_v4_smash import LandDetector

sys.path.append('pix2pix')
#sys.path.append('stylegan/stylegan2_pytorch')
from jpcartoon import Cartooner
#from stylegan2_pytorch.projector import project_simage
#from stylegan2_pytorch.generate import generate
#from stylegan2_pytorch.apply_factor import change_latent
import random
import string

def ranstr(num):
    salt = ''.join(random.sample(string.ascii_letters + string.digits, num))
    return salt

img_dir = './images'

IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]

def is_image_file(filename):
    return any(filename.endswith(extension) for extension in IMG_EXTENSIONS)
 
class MyRequestHandler(http.server.BaseHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        self.busy = False
        BaseHTTPRequestHandler.__init__(self, *args, **kwargs)
        
        
        
    def do_OPTIONS(self):           
        self.send_response(200, "ok")       
        self.send_header('Access-Control-Allow-Origin', '*')                
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With")      
    
    def do_POST(self):
        
        
        if self.busy:
            self.send_response(201, "waiting")
            self.send_header('Content-Type', 'application/json')
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            json_data = {"status":"waiting"}
            self.wfile.write(json.dumps(json_data).encode())
#        while True:
        if not self.busy:
            self.send_response(202, "processing")
#                self.send_header('Content-Type', 'application/json')
#                self.send_header('Access-Control-Allow-Origin', '*')
#                self.end_headers()
#                json_data = {"status":"processing"}
#                self.wfile.write(json.dumps(json_data).encode()) 
            self.busy = True
          
        ctype, pdict = parse_header(self.headers['content-type'])
        print(pdict)
        pdict['boundary'] = bytes(pdict['boundary'], "utf-8")
       
        fields = parse_multipart(self.rfile, pdict)
        model_num = int(fields.get('model_num')[0].decode())
        if model_num==0 or model_num==1:
            bg_crop=0
        else:
            bg_crop=1
        file =  fields.get('file')[0]
        print(fields.get('ops')[0].decode())
        file_name = ranstr(20)
        if fields.get('ops')[0].decode() =="GenerateZip":
            zip_file = file
            zip_filename = file_name+"zip"
            f = open(zip_filename, "wb")
            f.write(zip_file)
            
            with zipfile.ZipFile(zip_filename,"r") as zip_ref:
                zip_ref.extractall("./"+file_name)
#            f = open("test.zip", "wb")
#            f.write(zip_file)
           
            os.system("rm -rf "+zip_filename)
            print("rm -rf "+zip_filename)
            out_dir = './return_'+file_name
            if not os.path.isdir(out_dir):
                os.makedirs(out_dir)
            output_zip = zipfile.ZipFile("return_"+zip_filename, 'w')  
            for root, _, fnames in sorted(os.walk("./"+file_name)):
                for fname in sorted(fnames):
                    if is_image_file(fname) and fname[0]!='.': 
                        
                        im = cv2.imread(os.path.join(root, fname))
                        print("processing "+fname)
                       
                        count, clip_img, clip_img_nogamma, mask, clip_img_nobgcrop = land_detector.clip(im, bg_crop=bg_crop)
                        
                        clip_img_rgb = cv2.cvtColor(clip_img, cv2.COLOR_BGR2RGB)
                        if count==0:
                            print("no faces detected")
                            continue
                        else:
                            print(clip_img_rgb.shape)
                            out_img = cartooner.transform(clip_img_rgb, model_num)
                            out_img = cv2.cvtColor(out_img, cv2.COLOR_RGB2BGR)
                            if bg_crop==1:
                                bg_mask = mask.astype(np.float32)/255.0
                                out_img = out_img.astype(np.float32)*(bg_mask)+clip_img_nobgcrop.astype(np.float32)*(1-bg_mask)
                                out_img[out_img<0] = 0
                                out_img[out_img>255] = 255
                                out_img = out_img.astype(np.uint8)
                              
#                            output_zip.write(clip_img, "ori_"+fname)
#                            output_zip.write(out_img, "manga_"+fname)
                            cv2.imwrite(os.path.join(out_dir, "ori_"+fname), clip_img_nogamma)
                            cv2.imwrite(os.path.join(out_dir, "manga_"+fname), out_img)
                            output_zip.write(os.path.join(out_dir, "ori_"+fname))
                            output_zip.write(os.path.join(out_dir, "manga_"+fname))
#            os.system("rm -rf images_client")
#            global is_running
            output_zip.close()
            
            
#            str(base64.b64encode(image)
#            data = output_zip.read()
#            print(model_num)
#            print(fields)
#        content_length = int(self.headers.get('content-length'))    
#        print(content_length)
#        body = self.rfile.read(content_length)
#        print(body)
#        data = json.loads(body, encoding='utf-8')
#        if data["ops"]=="Generate":
##            img_np, latent = generate(cartooner.opt, cartooner.model, cartooner.opt.device, cartooner.mean_latent)
#            zip_file = data["zip_file"]
#            f = open("test.zip", "wb")
#            f.write(zip_file)
#            img_np, latent = cartooner.generate()
#            latent = latent[0,0,:]
#            print(latent.shape)
##            file_latent = open("latent.txt","w")
##            for i in range(512):
##                file_latent.write(str(latent[i])+',')
##            file_latent.close()
#            image = cv2.imencode('.jpg', img_np)[1]
#            base64_data = str(base64.b64encode(image))[2:-1]
#           
#            latent = latent.reshape(512)
#            print(len(latent))
#            latent_buf = struct.pack("%sf" % len(latent), *latent)
#            latent_decode = base64.b64encode(latent_buf)
#            base64_latent = str(latent_decode, "utf-8")
##            base64_latent = latent_decode
            
            
#            json_data = {"result":"success"}
            self.send_response(200)
#            self.send_header('Content-Type', 'application/zip, application/octet-stream')
            self.send_header('Content-Type', 'application/zip')
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            with open("return_"+zip_filename, 'rb') as f:
                self.wfile.write(f.read())
#            self.wfile.write(base64_data.encode())
            os.system("rm -rf "+"return_"+zip_filename)
#            os.system("rm -rf "+out_dir)
#            os.system("rm -rf "+file_name)
            self.busy = False  
            
        else:
            
            post_fix = fields.get('postfix')[0].decode()
            f = open(os.path.join(img_dir, file_name+post_fix), "wb")
            f.write(file)
            f.close()
            im = cv2.imread(os.path.join(img_dir, file_name+post_fix))
            count, clip_img, clip_img_nogamma, mask, clip_img_nobgcrop = land_detector.clip(im, bg_crop=bg_crop)
            
            
            if count==0:
                print("no faces detected")
                base64_data = ""
            else:
                clip_img_rgb = cv2.cvtColor(clip_img, cv2.COLOR_BGR2RGB)
#                print(clip_img_rgb.shape)
                out_img = cartooner.transform(clip_img_rgb, model_num)
                out_img = cv2.cvtColor(out_img, cv2.COLOR_RGB2BGR)
                if bg_crop==1:
#                    print(np.max(mask))
                    bg_mask = mask.astype(np.float32)/255.0
#                    cv2.imwrite(os.path.join(img_dir, "out_img_"+file_name+post_fix), out_img)
#                    cv2.imwrite(os.path.join(img_dir, "mask_"+file_name+post_fix), mask)
                    out_img = out_img.astype(np.float32)*(bg_mask)+clip_img_nobgcrop.astype(np.float32)*(1-bg_mask)
                    out_img[out_img<0] = 0
                    out_img[out_img>255] = 255
                    out_img = out_img.astype(np.uint8)
#                    cv2.imwrite(os.path.join(img_dir, "clip_img_"+file_name+post_fix), clip_img)
                    
                    
                out_image = cv2.imencode('.jpg', out_img)[1]
                base64_data = str(base64.b64encode(out_image))[2:-1]
            json_data = {"count":count, "img":base64_data}
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            self.wfile.write(json.dumps(json_data).encode()) 
            
def init_net():
    global cartooner
#    global is_running 
    cartooner = Cartooner()
    global land_detector
    land_detector = LandDetector()
    

max_net_num = 3
def main(ip='127.0.0.1', port=9999, opt=None):
#    global is_running
#    is_running = [0,0,0,0,0,0]
   
  
    
    server = HTTPServer((ip, port), MyRequestHandler)
    init_net()
    server.serve_forever()
    print('Listening on localhost:%s' % port)

if __name__ == "__main__":
    if len(sys.argv)>1:
        ip = sys.argv[1]
        port = int(sys.argv[2])
        sys.argv.pop(1)
        sys.argv.pop(1)
        print(sys.argv)
        main(ip,port)
#        sys.argv = sys.argv[3:]
#        print(sys.argv)
    else:
        main()