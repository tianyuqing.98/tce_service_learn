
from __future__ import print_function
import os, sys, time
import threading, argparse
import euler
from jpcartoon_thrift import CartoonService
from jpcartoon_thrift import ProcessJpTuningRequest, ProcessJpTuningResponse, BasePara
import cv2
import numpy as np 
import torch
from torch.multiprocessing import Pool, Process, set_start_method
import struct

class MThread(threading.Thread):
    def __init__(self,  id, latent=None):
        threading.Thread.__init__(self)
        self.id = id
        self.fd = ProcessJpTuningRequest()
        self.fd.image = open('images/inp.jpg', 'rb').read()
        print(type(self.fd.image))
#        self.fd.latent = latent
        self.client = euler.Client(CartoonService, 'tcp://localhost:1234')
        
    def run(self):
        process(self.client, self.fd, self.id)
       


def process_response(image_data, path):
    print('image recv')
    f = open(path, 'wb')
    f.write(image_data.image)
    f.close()


def process(client, fd, id):
    time_st = time.time()
    if id<1:
        fd.command = "Generate"
        resp = client.Process(fd)
       
        latent = resp.latent
        print(latent)
        #fd.image = open('./images/inp.png', 'rb').read()
    else:
        bp = BasePara()
        bp.para1 = 0
        bp.para2 = 0
        bp.para3 = 0
        bp.para4 = 0
        bp.para5 = 0
        bp.para6 = 0
        bp.para7 = 0
        bp.para8 = 0
        bp.para9 = 0
        bp.para10 = 0
        bp.range_s = 0
        bp.range_e = 14
        
        fd.command = "Adjust"
        fd.basePara = bp
        latent_tmp = np.zeros(512)
        latent_buf = struct.pack("%sd" % len(latent_tmp), *latent_tmp)
#        print(latent_buf)
#        print(type(latent_buf))
        fd.latent = latent_buf
        
        resp = client.Process(fd)
    

    time_ed = time.time()
    f = open("images/xxx"+str(id)+".png", 'wb')
    f.write(resp.image)
    f.close()
   
    print('thread:%d, %f' % (id, time_ed - time_st))
    print('client done')
   



if __name__ == '__main__':
    
#    if len(sys.argv)>1:
#        if len(sys.argv)<=2:
#            print("give output path")
#            sys.exit(0)
#        elif len(sys.argv)==3:
#            in_path = sys.argv[1]
#            out_path = sys.argv[2]
#          
#    
#    if not os.path.isdir(out_path):
#        os.makedirs(out_path)
    
 
    threadList = []
    num = 2
    for i in range(num):
        threadList.append(MThread(i))

    for i in range(num):
        threadList[i].start()
    
  
    
    
#    threadList[i].start()
#    threadList.append(MThread(1))
#    threadList[1].start()

#if __name__ == '__main__':
#    fd = ProcessJpTuningRequest()
#    fd.image = open('images/inp.jpg', 'rb').read()
#    fd.command = "Generate"
#    #fd.image = open('./images/inp.png', 'rb').read()
#    time_st = time.time()
#    resp = client.Process(fd)
#    time_ed = time.time()
#    print('thread: %f' % (time_ed - time_st))
#    process_response(resp, 'images/xxx.png')
#    print('generate done')
#    
#    fd = ProcessJpTuningRequest()
#    bp = BasePara()
#    bp.para1 = -2
#    bp.para2 = -2
#    bp.para3 = 0
#    bp.para4 = 0
#    bp.para5 = 0
#    bp.para6 = 0
#    bp.para7 = 0
#    bp.para8 = 0
#    bp.para9 = 0
#    bp.para10 = 0
#    bp.range_s = 0
#    bp.range_e = 14
#    
#    
#    fd.image = open('images/inp.jpg', 'rb').read()
#    fd.command = "Adjust"
#    fd.basePara = bp
#    fd.latent = resp.latent
#    #fd.image = open('./images/inp.png', 'rb').read()
#    time_st = time.time()
#    resp = client.Process(fd)
#    time_ed = time.time()
#    print('thread: %f' % (time_ed - time_st))
#    process_response(resp, 'images/xxx2.png')
#    print('generate done') 
