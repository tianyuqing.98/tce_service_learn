#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 12:38:55 2020

@author: huxinghong
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 21:36:21 2020

@author: huxinghong
"""

from threading import Thread
import struct
import time
import hashlib
import base64
import socket
import types
import multiprocessing
import os
import sys
import cv2
import base64
import json
import numpy as np
import decimal
import demjson
import torch
import http.server
from http.server import HTTPServer, BaseHTTPRequestHandler
from cgi import parse_header, parse_multipart
from urllib.parse import parse_qs

sys.path.append('stylegan')
sys.path.append('stylegan/stylegan2_pytorch')
from jpcartoon import Cartooner



def get_default_para():
    para_idx = np.array([4, 12, 9, 7, 11, 19, 3, 15, 12]).astype(int)
    level_range = np.array([[0,4],[0,4],[3,6],[4,8],[4,8],[4,8],[8,14],[8,14],[8,14]]).astype(int)
    return para_idx, level_range
    
class MyRequestHandler(http.server.BaseHTTPRequestHandler):
    def do_OPTIONS(self):           
        self.send_response(200, "ok")       
        self.send_header('Access-Control-Allow-Origin', '*')                
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With")        

    
    def do_POST(self):
        content_length = int(self.headers.get('content-length'))    
        print(content_length)
        body = self.rfile.read(content_length)
#        print(body)
        data = json.loads(body, encoding='utf-8')
        if data["ops"]=="Generate":
            blend_width = float(data["slider1"])
            layer = float(data["layer"])
#            img_np, latent = generate(cartooner.opt, cartooner.model, cartooner.opt.device, cartooner.mean_latent)
            img_np, latent = cartooner.generate()
            latent = latent[0,0,:]
            print(latent.shape)
            latent_torch = torch.from_numpy(latent.reshape((1,512)).astype(np.float32))
            img2_np, _ = cartooner.blend_model(layer, blend_width, latent_torch)
            
            image = cv2.imencode('.jpg', img_np)[1]
            base64_data = str(base64.b64encode(image))[2:-1]
            image2 = cv2.imencode('.jpg', img2_np)[1]
            base64_data2 = str(base64.b64encode(image2))[2:-1]
            
           
            latent = latent.reshape(512)
            print(len(latent))
            latent_buf = struct.pack("%sf" % len(latent), *latent)
            latent_decode = base64.b64encode(latent_buf)
            base64_latent = str(latent_decode, "utf-8")
#            base64_latent = latent_decode
            
            
            json_data = {"img":base64_data, "img2": base64_data2, "latent":base64_latent}
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            self.wfile.write(json.dumps(json_data).encode())
       
        elif data["ops"]=="Adjust":
            print(len(data))
           
            blend_width = float(data["slider1"])
            layer = float(data["layer"])
          
#            print(base64.b64decode(data["latent"]))
            latent_buf = base64.b64decode(data["latent"].encode("utf-8"))
#            print(len(latent_buf))
            latent = struct.unpack("<{0}f".format(512), latent_buf)  
#            print(latent)
            latent = torch.from_numpy(np.array(latent).reshape((1,512)).astype(np.float32))
            
             
            img_np, _ = cartooner.blend_model(layer, blend_width, latent)
            image = cv2.imencode('.jpg', img_np)[1]
            base64_data = str(base64.b64encode(image))[2:-1]
            
            json_data = {"img":base64_data, "latent":data["latent"]}
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            self.wfile.write(json.dumps(json_data).encode())
     
  
   
def init_net():
    global cartooner
    cartooner = Cartooner()
   
    
    
def main(ip='127.0.0.1', port=9999, opt=None):
    if len(sys.argv)>1:
        sys.argv.pop(1)
        sys.argv.pop(1)
        print(sys.argv)
    init_net()
    print('Listening on localhost:%s' % port)
    server = HTTPServer((ip, port), MyRequestHandler)
    server.serve_forever()

if __name__ == "__main__":
    if len(sys.argv)>1:
        main(sys.argv[1],int(sys.argv[2]))
    else:
        main()
        print(sys.argv)