#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 21:36:21 2020

@author: huxinghong
"""

from threading import Thread
import struct
import time
import hashlib
import base64
import socket
import types
import multiprocessing
import os
import sys
import cv2
import base64
import json
import numpy as np
import decimal
import demjson
import torch
import jsonlines

sys.path.append('stylegan')
sys.path.append('stylegan/stylegan2_pytorch')
from jpcartoon import Cartooner
from stylegan2_pytorch.projector import project_simage
from stylegan2_pytorch.generate import generate
from stylegan2_pytorch.apply_factor import change_latent


#sys.path.append('pix2pix')

mode = "initialize"
pic_size = 0
pic_receive = 0
pic = ""
pic_repeat = []





class returnCrossDomain(Thread):
    def __init__(self, connection):
        Thread.__init__(self)
        self.con = connection
        self.isHandleShake = False

    def run(self):
        global mode
        global pic_size
        global pic_receive
        global pic
        global pic_repeat
        while True:
            if not self.isHandleShake:
                # 开始握手阶段
                header = self.analyzeReq()
                secKey = header['Sec-WebSocket-Key'];

                acceptKey = self.generateAcceptKey(secKey)

                response = "HTTP/1.1 101 Switching Protocols\r\n"
                response += "Upgrade: websocket\r\n"
                response += "Connection: Upgrade\r\n"
                response += "Sec-WebSocket-Accept: %s\r\n\r\n" % (acceptKey.decode('utf-8'))
                self.con.send(response.encode())
                self.isHandleShake = True
                if(mode=="initialize"):
                    mode = "get_order"
                    
                print('response:\r\n' + response)
                # 握手阶段结束
            elif mode == "get_order":
                
                opcode = self.getOpcode()
                if opcode == 8:
                    self.con.close()
                self.getDataLength()
                total_len = self.payDataLength
                
                data = ""
                iter_num = 0
                max_num = 2
                clientData, data_len = self.readClientData()
                
                
                print(data_len)
                while data_len!=0:
                    total_len -= data_len
                    data = data + clientData
                    iter_num += 1
                    print((total_len, self.dataLength, data_len))
                    clientData, data_len = self.readClientData()
                    print(data_len)
                print('客户端数据：' + str(clientData))
                # 处理数据
                print((total_len, self.dataLength, len(data)))
                
                ans = self.answer(json.loads(data))
#                self.sendDataToClient(ans)
                
                
                self.sendDictDataToClient(ans)
                
#                if (ans != "Unresolvable Command!" and ans != "hello world"):
#                    pic_size = int(clientData[3:])
#                    pic_receive = 0
#                    pic = ""
#                    pic_repeat=[]
#                    print("需要接收的数据大小：", pic_size)
#                    mode = "get_pic"

            #读取图片阶段
            elif mode == "get_pic":
                opcode = self.getOpcode()
                if opcode == 8:
                    self.con.close()
                self.getDataLength()
                clientData,_ = self.readClientData()
                print('客户端数据：' + str(clientData))
                pic_receive += len(clientData)
                pic += clientData
                if pic_receive < pic_size:
                    self.sendDataToClient("Receive:"+str(pic_receive)+"/"+str(pic_size))
                    print("图片接收情况:",pic_receive,"/",pic_size)
                    #print("当前图片数据:",pic)
                else:
                    print("完整图片数据:",pic)
                    self.sendDataToClient("Receive:100%")
                    result = self.process(pic)
                    self.sendDataToClient(result)
                    pic_size = 0
                    pic_receive = 0
                    pic = ""
                    pic_repeat=[]
                    mode = "get_order"
                # 处理数据
    def legal(self, string):  # python总会胡乱接收一些数据。。只好过滤掉
        if len(string) == 0:
            return 0
        elif len(string) <= 100:
            if self.loc(string) != len(string):
                return 0
            else:
                if mode != "get_pic":
                    return 1
                elif len(string) + pic_receive == pic_size:
                    return 1
                else:
                    return 0
        else:
            if self.loc(string) > 100:
                if mode != "get_pic":
                    return 1
                elif string[0:100] not in pic_repeat:
                    pic_repeat.append(string[0:100])
                    return 1
                else:
                    return -1  # 收到重复数据，需要重定位
            else:
                return 0
    def loc(self, string):
        i = 0
        while(i<len(string) and self.rightbase64(string[i])):
            i = i+1
        return i

    def rightbase64(self, ch):
        if (ch >= 'a') and (ch <= 'z'):
            return 1
        elif (ch >= 'A') and (ch <= 'Z'):
            return 1
        elif (ch >= '0') and (ch <= '9'):
            return 1
        elif ch == '+' or ch == '/' or ch == '|' or ch == '=' or ch == ' ' or ch == '\'' or ch == '!' or ch == ':':
            return 1
        else:
            return 0

    def analyzeReq(self):
        reqData = self.con.recv(1024).decode()
        reqList = reqData.split('\r\n')
        headers = {}
        for reqItem in reqList:
            if ': ' in reqItem:
                unit = reqItem.split(': ')
                headers[unit[0]] = unit[1]
        return headers
    
    def generateAcceptKey(self, secKey):
        sha1 = hashlib.sha1()
        sha1.update((secKey + '258EAFA5-E914-47DA-95CA-C5AB0DC85B11').encode())
        sha1_result = sha1.digest()
        acceptKey = base64.b64encode(sha1_result)
        return acceptKey

    def getOpcode(self):
        first8Bit = self.con.recv(1)
        first8Bit = struct.unpack('B', first8Bit)[0]
        opcode = first8Bit & 0b00001111
        return opcode

    def getDataLength(self):
        second8Bit = self.con.recv(1)
        print("second8Bit", second8Bit)
        second8Bit = struct.unpack('B', second8Bit)[0]
        masking = second8Bit >> 7
        dataLength = second8Bit & 0b01111111
        print("dataLength:",dataLength)
        if dataLength <= 125:
            payDataLength = dataLength
        elif dataLength == 126:
            payDataLength = struct.unpack('H', self.con.recv(2))[0]
        elif dataLength == 127:
            payDataLength = struct.unpack('Q', self.con.recv(8))[0]
        self.masking = masking
        self.payDataLength = payDataLength
        self.dataLength = dataLength
        print("payDataLength:", payDataLength)
    
    def readClientData(self):
        datalen = 0
        if self.masking == 1:
            maskingKey = self.con.recv(4)
            print(maskingKey)
            
        data = self.con.recv(self.payDataLength)
        datalen += len(data)
#        data = self.legal(data)
        print(data)
        
        if self.masking == 1:
            i = 0
            trueData = ''
            for d in data:
                trueData += chr(d ^ maskingKey[i % 4])
                i += 1
            
            return trueData, datalen
        else:
            return data, datalen
        

    def sendDictDataToClient(self, data):
#        bytes(data,encoding="utf-8")
        sendData = ''
        sendData = struct.pack('!B', 0x81)

        length = len(json.dumps(data))
        print("ans length "+str(length))
        if length <= 125:
            sendData += struct.pack('!B', length)
        elif length <= 65536:
            sendData += struct.pack('!B', 126)
            sendData += struct.pack('!H', length)
        elif length == 127:
            sendData += struct.pack('!B', 127)
            sendData += struct.pack('!Q', length)
            
        sendData += struct.pack('!%ds' % (length),(json.dumps(data)).encode())
#        sendData += struct.pack('!%ds' % (length),bytes(json.dumps(data),encoding="utf-8"))
        dataSize = self.con.sendall(sendData)
#        self.con.sendall(bytes(json.dumps(data),encoding="utf-8"))
        
        
    def sendDataToClient(self, text):
        sendData = ''
        sendData = struct.pack('!B', 0x81)

        length = len(text)
        
        if length <= 125:
            sendData += struct.pack('!B', length)
        elif length <= 65536:
            sendData += struct.pack('!B', 126)
            sendData += struct.pack('!H', length)
        elif length == 127:
            sendData += struct.pack('!B', 127)
            sendData += struct.pack('!Q', length)

#        self.con.send(json.dumps(data).encode())
#        sendData += struct.pack('!%ds' % (length), text.encode())
        sendData += struct.pack('!%ds' % (length), (json.dumps(text)).encode())
#        sendData += struct.pack('!%ds' % (length), text)
#        print(sendData)
        dataSize = self.con.send(sendData)
#        dataSize = self.con.send(text)

    def answer(self,data):
        print(data["ops"])
        if data["ops"]=="Generate":
            img_np, latent = generate(cartooner.opt, cartooner.model, cartooner.opt.device, cartooner.mean_latent)
            latent = latent[0,0,:]
            print(latent.shape)
            file_latent = open("latent.txt","w")
            for i in range(512):
                file_latent.write(str(latent[i])+',')
            file_latent.close()
            image = cv2.imencode('.jpg', img_np)[1]
            base64_data = str(base64.b64encode(image))[2:-1]
           
            latent = latent.reshape(512)
            print(len(latent))
            latent_buf = struct.pack("%sf" % len(latent), *latent)
            latent_decode = base64.b64encode(latent_buf)
            base64_latent = str(latent_decode, "utf-8")
#            base64_latent = latent_decode
            return {"img":base64_data, "latent":base64_latent}
        else:
            paras = np.zeros(len(data)-3)
            for i in range(2,len(data)-3):
               paras[i-2] = float(data["slider"+str(i)])
               print("para "+str(i-2)+" "+str(paras[i-2]))
#            latent = np.asarray(paras["latent"]).astype(np.float32)
            print(data)
            return_latent = data["latent"]
#            print(base64.b64decode(data["latent"]))
            latent_buf = base64.b64decode(data["latent"].encode("utf-8"))
            count = len(latent_buf) // 4
#            print(len(latent_buf))
            latent = struct.unpack("<{0}f".format(512), latent_buf)  
            print(latent)
            latent = torch.from_numpy(np.array(latent).reshape((1,512)).astype(np.float32))
            
            level_range = [int(data["levelRange1"]), int(data["levelRange2"])]
            img_np = change_latent(cartooner.opt, cartooner.model, cartooner.eigen, paras, level_range, cartooner.opt.device, latent)
            image = cv2.imencode('.jpg', img_np)[1]
            base64_data = str(base64.b64encode(image))[2:-1]
            
            return {"img":base64_data, "latent":return_latent} 
        
   
    def padding(self,data):
        missing_padding = 4 - len(data) % 4
        if missing_padding:
            data += '='*missing_padding
        return data

    def process(self,pic):
        print("processing pics")
        latent, out_img = project_simage(pic, cartooner.model, cartooner.opt.device)
        #此处是图片处理阶段

        return pic

def init_net():
    global cartooner
    cartooner = Cartooner()
   
    
    
def main(ip='127.0.0.1', port=9999, opt=None):
    init_net()
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((ip, port))
    sock.listen(5)
    while True:
        try:
            connection, address = sock.accept()
            returnCrossDomain(connection).start()
            print("start...")
        except:
            time.sleep(1)
            print("sleep...")

if __name__ == "__main__":
    if len(sys.argv)>1:
        main(sys.argv[1],int(sys.argv[2]))
        sys.argv = sys.argv[3:]
    else:
        main()