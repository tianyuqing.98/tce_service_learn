
import argparse

class TestOptions():
    """This class includes test options.

    It also includes shared options defined in BaseOptions.
    """

    def initialize(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("--ckpt1", type=str, default="./stylegan/checkpoint/563000.pt")
        parser.add_argument("--ckpt2", type=str, default="./stylegan/checkpoint/580000.pt")
        parser.add_argument("--size", type=int, default=256)
        parser.add_argument("--lr_rampup", type=float, default=0.05)
        parser.add_argument("--lr_rampdown", type=float, default=0.25)
        parser.add_argument("--lr", type=float, default=0.1)
        parser.add_argument("--noise", type=float, default=0.05)
        parser.add_argument("--noise_ramp", type=float, default=0.75)
        parser.add_argument("--step", type=int, default=1000)
        parser.add_argument("--noise_regularize", type=float, default=1e5)
        parser.add_argument("--mse", type=float, default=0)
        parser.add_argument("--w_plus", action="store_true")
      
        parser.add_argument("-i", "--index", type=str, default="0")
        parser.add_argument("-d", "--degree", type=float, default=5)
#        parser.add_argument("--ckpt", type=str, required=True)
#        parser.add_argument("--size", type=int, default=256)
        parser.add_argument("--trunc_num", type=int, default=3)# how many number of image generate along the direction
        parser.add_argument("--n_sample", type=int, default=7)
        parser.add_argument("--truncation", type=float, default=1)
        parser.add_argument("--device", type=str, default="cuda")
        parser.add_argument("--out_prefix", type=str, default="factor")
        parser.add_argument("--factor", type=str, default="./stylegan/checkpoint/factor.pt")
        parser.add_argument("--out_dir", type=str, default="./factorize")
        parser.add_argument("--level", type=str, default="0,4;4,8;8,14")#change direction on  which level 
        
        
        parser.add_argument("--gpu_id", type=int, default="0")
        parser.add_argument('--gradual', action='store_true')
#        parser.add_argument('--step', action='store_true', help='manipulate latent position one by one')
        parser.add_argument('--sample', type=int, default=1)
        parser.add_argument('--latent', type=int, default=512)
        parser.add_argument('--pics', type=int, default=1)
        parser.add_argument('--truncation_mean', type=int, default=4096)
        parser.add_argument('--channel_multiplier', type=int, default=2)
        
        opt, _ = parser.parse_known_args()
        print(opt)
  
        return opt
