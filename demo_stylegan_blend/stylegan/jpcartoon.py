import os, cv2
from options.test_options import TestOptions
from stylegan2_pytorch.model import Generator
from stylegan2_pytorch import create_model, create_new_model
import torch

from PIL import Image
import numpy as np
import pdb
from stylegan2_pytorch import lpips
from torch import optim
from tqdm import tqdm
import math
from torch.nn import functional as F
from torchvision import utils


def tile(a, dim, n_tile):
    init_dim = a.size(dim)
    repeat_idx = [1] * a.dim()
    repeat_idx[dim] = n_tile
    a = a.repeat(*(repeat_idx))
    order_index = torch.LongTensor(np.concatenate([init_dim * np.arange(n_tile) + i for i in range(init_dim)]))
    return torch.index_select(a, dim, order_index)

def generate_single(args, g_ema, layer,  mean_latent, input_code, input_is_latent=False):
    with torch.no_grad():
        g_ema.eval()
    
        if input_is_latent:
            sample, latent = g_ema([input_code], truncation=args.truncation, truncation_latent=None, return_latents=False,input_is_latent=input_is_latent)
        else:
            sample, latent = g_ema([input_code], truncation=args.truncation, truncation_latent=None, return_latents=True,input_is_latent=input_is_latent)
        
        grid = utils.make_grid(sample, nrow=1, normalize=True, range=(-1, 1))
           
        image_ndarr = grid.mul(255).add_(0.5).clamp_(0, 255).permute(1, 2, 0).to('cpu', torch.uint8).numpy()
        image_ndarr = cv2.cvtColor(image_ndarr, cv2.COLOR_BGR2RGB)
      
        return image_ndarr


class Cartooner():
    def __init__(self):
        self.opt = getOpt()
        self.model, self.model2 = create_model(self.opt)
        self.device = torch.device('cuda:{}'.format(self.opt.gpu_id)) if self.opt.gpu_id!=-1 else torch.device('cpu') 
       
#        self.opt=opt
        self.model.eval()
        with torch.no_grad():
            self.mean_latent = self.model.mean_latent(1)
    
    def generate(self):
        if not os.path.isdir(self.opt.out_dir):
            os.makedirs(self.opt.out_dir)
        with torch.no_grad():
            self.model.eval()
    #        if args.gradual:
            sample_a = torch.randn(self.opt.sample, self.opt.latent, device=self.device)
            sample_b = torch.randn(self.opt.sample, self.opt.latent, device=self.device)
            sample_z = torch.zeros(self.opt.sample, self.opt.latent, device=self.device)
            for i in tqdm(range(self.opt.pics)):
               if self.opt.gradual:
                   sample_z = sample_a+(sample_b - sample_a)/self.opt.pics*i
               else:
                   sample_z = torch.randn(self.opt.sample, self.opt.latent, device=self.device)
    
               sample, cur_latent = self.model([sample_z], truncation=self.opt.truncation, truncation_latent=self.mean_latent, return_latents=True)
               
               grid = utils.make_grid(sample, nrow=1, normalize=True, range=(-1, 1))
               cur_latent = cur_latent.to('cpu').numpy()
               image_ndarr = grid.mul(255).add_(0.5).clamp_(0, 255).permute(1, 2, 0).to('cpu', torch.uint8).numpy()
#               utils.save_image(
#                sample,
#                os.path.join(self.opt.out_dir, str(i).zfill(6)+'.png'),
#                nrow=1,
#                normalize=True,
#                range=(-1, 1),
#            )
        image_ndarr = cv2.cvtColor(image_ndarr, cv2.COLOR_BGR2RGB)
        return image_ndarr, cur_latent
    
    def blend_model(self, layer, blend_width, latent_cpu):
        
        latent = latent_cpu.to(self.device)
        used_g_ema = create_new_model(self.opt)
        used_g_ema.load_state_dict(self.model.state_dict())
        if layer==4:
            if self.opt.truncation < 1:
                with torch.no_grad():
                    mean_latent = self.model2.mean_latent(self.opt.truncation_mean)
            else:
                mean_latent = None
            used_g_ema = self.model2
        elif layer==self.opt.size:
            if self.opt.truncation < 1:
                with torch.no_grad():
                    mean_latent = self.model.mean_latent(self.opt.truncation_mean)
            else:
                mean_latent = None
            used_g_ema = self.model
        else:
            log_size = int(math.log(layer, 2))
            layer_num = (log_size - 2) 
            log_max_size = int(math.log(self.opt.size, 2))
            max_layer_num = (log_max_size - 2)  
            
            start_point_pos = (layer_num-1)*2
            
            max_exp = 2*max_layer_num - start_point_pos
            
            g_ema2_state_dict = self.model2.state_dict()
            g_ema1_state_dict = self.model.state_dict()
            print("replacing range ["+str(layer_num)+","+str(max_layer_num)+"]")
            for j in range(layer_num, max_layer_num+1):
                for name, param in used_g_ema.named_parameters():
                    if name.find("convs."+str(2*j)+'.')!=-1:
#                        print("replacing "+name)
                        cur_pos = 2*j
                        x = cur_pos - start_point_pos
#                            print(x)
                        if blend_width!=1:
                            exponent = math.pow(1/blend_width, 1/max_exp)
                            y = exponent**x*blend_width
#                            ori
#                            exponent = -x/blend_width
#                            y = 1 / (1 + math.exp(exponent))
                        else:
                            y = 1 if x > 1 else 0
                        param.data = g_ema2_state_dict[name]*y+g_ema1_state_dict[name]*(1-y)
                    if name.find("convs."+str(2*j+1)+'.')!=-1:
#                        print("replacing "+name)
                        cur_pos = 2*j+1
                        x = cur_pos - start_point_pos+1
#                        print(x)
                        if blend_width!=1:
                            exponent = math.pow(1/blend_width, 1/max_exp)
                            y = exponent**x*blend_width
#                            ori
#                            exponent = -x/blend_width
#                            y = 1 / (1 + math.exp(exponent))
#                            print((y, j, x, max_exp, exponent))
                        else:
                            y = 1 if x > 1 else 0
                        
                        param.data = g_ema2_state_dict[name]*y+g_ema1_state_dict[name]*(1-y)
                    if name.find("to_rgbs."+str(j)+'.')!=-1:
#                        print("replacing "+name)
                        cur_pos = 2*j+1
                        x = cur_pos - start_point_pos+1
#                        print(x)
                        if blend_width!=1:
                            exponent = math.pow(1/blend_width, 1/max_exp)
                            y = exponent**x*blend_width
#                            ori
#                            exponent = -x/blend_width
#                            y = 1 / (1 + math.exp(exponent))
                        else:
                            y = 1 if x > 1 else 0
                        param.data = g_ema2_state_dict[name]*y+g_ema1_state_dict[name]*(1-y)
#            
        if self.opt.truncation < 1:
            with torch.no_grad():
                mean_latent = used_g_ema.mean_latent(self.opt.truncation_mean)
        else:
            mean_latent = None    
        image_ndarr = generate_single(self.opt, used_g_ema, layer, mean_latent, latent, input_is_latent=True) 
       
        torch.cuda.empty_cache()
        
        return image_ndarr, latent_cpu
    
    def change_latent(self, new_para, para_idx, cur_level_range, latent):
         if not os.path.isdir(self.opt.out_dir):
            os.makedirs(self.opt.out_dir)
         with torch.no_grad():
            self.model.eval()
            trunc = self.model.mean_latent(4096)
#
            latent = latent.to(self.device)
            post_fix = "-{:.1f}".format(new_para[0])
            if len(cur_level_range)<=2:
                direction = new_para[0] * self.eigen[:, para_idx[0]].unsqueeze(0)
                for i in range(1, len(new_para)):
                     direction = direction+new_para[i] * self.eigen[:, para_idx[i]].unsqueeze(0)
                     post_fix = post_fix+"_{:.1f}".format(new_para[i])
    
                new_latent = latent+direction
                
                
                img, cur_latent = self.model([latent, new_latent],
                                truncation=1,
                                truncation_latent=trunc,
                                input_is_latent=True,
                                inject_index_start=cur_level_range[0],
                                inject_index_end=cur_level_range[1],
                                return_latents=True)
            else:
                latent = (latent.unsqueeze(1)).repeat(1,14,1)
                new_latent =latent.clone()
                for i in range(0, len(new_para)):
                    direction = (new_para[i] * self.eigen[:, para_idx[i]].unsqueeze(0).unsqueeze(1)).repeat(1,14,1)
                    new_latent[0, cur_level_range[i,0]:cur_level_range[i,1], :] = new_latent[0, cur_level_range[i,0]:cur_level_range[i,1], :]\
                                                                                    + direction[0,cur_level_range[i,0]:cur_level_range[i,1], :]
                img, cur_latent = self.model([latent, new_latent],
                                truncation=1,
                                truncation_latent=trunc,
                                input_is_latent=True,
                                inject_index_start=0,
                                inject_index_end=14,
                                return_latents=True)                                                                  

            grid = utils.make_grid(img, nrow=1, normalize=True, range=(-1, 1))
           
            image_ndarr = grid.mul(255).add_(0.5).clamp_(0, 255).permute(1, 2, 0).to('cpu', torch.uint8).numpy()
            

            image_ndarr = cv2.cvtColor(image_ndarr, cv2.COLOR_BGR2RGB)
            cur_latent = cur_latent.to('cpu').numpy()[0,0,:]

         
         return image_ndarr, cur_latent

        


def getOpt(image_size=256):
    opt = TestOptions().initialize()
    
    return opt


   
def norm_ip(img, min, max):
    img.clamp_(min=min, max=max)
    img.add_(-min).div_(max - min + 1e-5)
            
def norm_range(t, range):
    if range is not None:
        norm_ip(t, range[0], range[1])
    else:
        norm_ip(t, float(t.min()), float(t.max()))
    return t
            

def get_lr(t, initial_lr, rampdown=0.25, rampup=0.05):
    lr_ramp = min(1, (1 - t) / rampdown)
    lr_ramp = 0.5 - 0.5 * math.cos(lr_ramp * math.pi)
    lr_ramp = lr_ramp * min(1, t / rampup)

    return initial_lr * lr_ramp

def latent_noise(latent, strength):
    noise = torch.randn_like(latent) * strength
    return latent + noise

def noise_regularize(noises):
    loss = 0

    for noise in noises:
        size = noise.shape[2]

        while True:
            loss = (
                loss
                + (noise * torch.roll(noise, shifts=1, dims=3)).mean().pow(2)
                + (noise * torch.roll(noise, shifts=1, dims=2)).mean().pow(2)
            )

            if size <= 8:
                break

            noise = noise.reshape([-1, 1, size // 2, 2, size // 2, 2])
            noise = noise.mean([3, 5])
            size //= 2

    return loss

def noise_normalize_(noises):
    for noise in noises:
        mean = noise.mean()
        std = noise.std()

        noise.data.add_(-mean).div_(std)

def make_image(tensor):
    return (
        tensor.detach()
        .clamp_(min=-1, max=1)
        .add(1)
        .div_(2)
        .mul(255)
        .type(torch.uint8)
        .permute(0, 2, 3, 1)
        .to("cpu")
        .numpy()
    )



def project(img, g_ema, device):
    imgs = img[None,:,:,:]
    n_mean_latent = 10000
    with torch.no_grad():
        noise_sample = torch.randn(n_mean_latent, 512, device=device)
        latent_out = g_ema.style(noise_sample)

        latent_mean = latent_out.mean(0)
        latent_std = ((latent_out - latent_mean).pow(2).sum() / n_mean_latent) ** 0.5

    percept = lpips.PerceptualLoss(
        model="net-lin", net="vgg", use_gpu=device.startswith("cuda")
    )

    noises_single = g_ema.make_noise()
    noises = []
    for noise in noises_single:
        noises.append(noise.repeat(imgs.shape[0], 1, 1, 1).normal_())

    latent_in = latent_mean.detach().clone().unsqueeze(0).repeat(imgs.shape[0], 1)


    latent_in.requires_grad = True

    for noise in noises:
        noise.requires_grad = True

    optimizer = optim.Adam([latent_in] + noises, lr=opt.lr)

    pbar = tqdm(range(opt.step))
    
    latent_path = []
    for i in pbar:
        t = i / 1000
        lr = get_lr(t, 0.1)
        optimizer.param_groups[0]["lr"] = lr
        noise_strength = latent_std * 0.05 * max(0, 1 - t / 0.75) ** 2
        latent_n = latent_noise(latent_in, noise_strength.item())

        img_gen, _ = g_ema([latent_n], input_is_latent=True, noise=noises)

        batch, channel, height, width = img_gen.shape

        if height > 256:
            factor = height // 256

            img_gen = img_gen.reshape(
                batch, channel, height // factor, factor, width // factor, factor
            )
            img_gen = img_gen.mean([3, 5])

        p_loss = percept(img_gen, imgs).sum()
        n_loss = noise_regularize(noises)
        mse_loss = F.mse_loss(img_gen, imgs)

        loss = p_loss + 1e5 * n_loss 

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        noise_normalize_(noises)

        if (i + 1) % 100 == 0:
            latent_path.append(latent_in.detach().clone())

        pbar.set_description(
            (
                f"perceptual: {p_loss.item():.4f}; noise regularize: {n_loss.item():.4f};"
                f" mse: {mse_loss.item():.4f}; lr: {lr:.4f}"
            )
        )

    img_gen, _ = g_ema([latent_path[-1]], input_is_latent=True, noise=noises)

#    img_ar = make_image(img_gen)

  
    return latent_path[-1], img_gen
    
def generate_by_latent(latent, g, eigvec, opt, paras, semantic_range):

    trunc = g.mean_latent(4096)

#    latent = torch.randn(1, 512, device=opt.device)
#    latent = g.get_latent(latent)

    direction = np.zeros(eigvec[:, 0].unsqueeze(0).shape)
    for i in range(len(paras)):
        direction += paras[i] * eigvec[:, i].unsqueeze(0)

    img, _ = g(
        [latent, latent+direction],
        truncation=opt.truncation,
        truncation_latent=trunc,
        truncation_latent_start = semantic_range[0],
        truncation_latent_end = semantic_range[1],
        input_is_latent=True,
    )
    img = norm_range(img)
    img_array = img.mul(255).add_(0.5).clamp_(0, 255).permute(1, 2, 0).to('cpu', torch.uint8).numpy()
    
    return img_array
   
    

if __name__ == '__main__':
    #pdb.set_trace()

    opt = getOpt()
    #opt.print_options(opt)
    print(opt.direction)
    
    cartoon = Cartooner(opt)
    image1 = cv2.cvtColor(cv2.imread('clip.png'), cv2.COLOR_BGR2RGB)
    image1 = image1.astype(np.float32) / 127.5 - 1.0

    image2 = cv2.cvtColor(cv2.imread('clip.png'), cv2.COLOR_BGR2RGB)
    image2 = image2.astype(np.float32) / 127.5 - 1.0
    #image = Image.open('test.png').convert('RGB')
    #clip = self.detector.clip(image)
    #cv2.imwrite('clip.png', clip)
    images = [image1, image2]
    output = cartoon.transform_batch(images) 
#    print(output[0].shape)

    img_np = (np.transpose(output[0], (1, 2, 0)) + 1.0) * 127.5
    img_np = img_np.astype(np.uint8)


    cv2.imwrite('out.png', img_np)
    #util.save_image(output[0], 'out.png')

    #cv_img = cv2.imencode('.jpg', output)[1].tostring()
    #cv2.imwrite('cv.png', cv_img)
    #open('cv.png', 'wb').write(Image.fromarray(output))

