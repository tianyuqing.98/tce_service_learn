include "base.thrift"

namespace go toutiao.effect.jpcartoon_tuning 
namespace py toutiao.effect.jpcartoon_tuning


struct BasePara {
    1: double para1 = 0,
    2: double para2 = 0,
    3: double para3 = 0,
    4: double para4 = 0,
    5: double para5 = 0,
    6: double para6 = 0,
    7: double para7 = 0,
    8: double para8 = 0,
    9: double para9 = 0,
    10: double para10 = 0,
    11: double range_s = 0,
    12: double range_e = 14,
}


struct ProcessJpTuningRequest {
    1: binary image,
    2: string command,
    3: binary latent,
    254: BasePara basePara,
    255: base.Base Base,
}

struct ProcessJpTuningResponse {
    1: binary image,
    2: i32 width,
    3: i32 height,
    4: binary latent,
    255: base.BaseResp BaseResp,
}

service CartoonService {
  ProcessJpTuningResponse Process(1:ProcessJpTuningRequest req)
}
