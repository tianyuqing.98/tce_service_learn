include "base.thrift"

namespace go toutiao.effect.cartoonfull
namespace py toutiao.effect.cartoonfull

struct ProcessCartoonfullRequest {
    1: binary image,
    2: string attribute,
    255: base.Base Base
}

struct ProcessCartoonfullResponse {
    1: binary image,
    2: i32 width,
    3: i32 height,
    255: base.BaseResp BaseResp
}

service CartoonService {
  ProcessCartoonfullResponse Process(1:ProcessCartoonfullRequest req)
}
