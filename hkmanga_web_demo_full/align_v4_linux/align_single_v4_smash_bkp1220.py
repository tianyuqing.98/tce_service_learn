#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 25 14:56:46 2019

align method v4 using smash

detailed explanation can be found in https://bytedance.feishu.cn/docs/doccnCvmiIEPVcQHvqymVal7tCb#HyAFdY

@author: huxinghong
"""



import sys
import numpy as np
import cv2
from .utils.matlab_cp2tform import get_similarity_transform, cvt_tform_mat_for_cv2
import os
import math
from .face_alignment import face_alignment_outline as fao
from .face_alignment.face_alignment_outline import get_mask
from .face_alignment import get_face_attribute as faceAttr
from .portrait_matting import portrait_matting as pm
from PIL import Image, ImageFilter
import time
import random


IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]




def get_eye_mask(cv_img, lmks, rec=0):
    mask = np.zeros(cv_img.shape[:-1],np.uint8)

    parts1 = []
    parts2 = []
    for i in range(52,58):
        outline1 = [int(lmks[i,0]), int(lmks[i,1])]
        outline2 = [int(lmks[i+6,0]), int(lmks[i+6,1])]
        parts1.append(outline1)
        parts2.append(outline2)

    parts1 = np.array(parts1)
    parts2 = np.array(parts2)
    filler1 = cv2.convexHull(parts1)
    filler2 = cv2.convexHull(parts2)
    cv2.fillConvexPoly(mask, filler1, 1)
    cv2.fillConvexPoly(mask, filler2, 1)
    return mask

def adjust_gamma(image, gamma=1.0):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
    return cv2.LUT(image,table)

def get_face_mask(cv_img, lmks, expand_eyebrows=False):
    mask = get_mask(cv_img.shape[:2], lmks, expand_eyebrows=expand_eyebrows)
    return mask

def get_halfbottom_mask(cv_img, lmks):
    mask = np.zeros(cv_img.shape[:-1],np.uint8)
    parts = []
    for i in range(7,26):
        outline = [int(lmks[i,0]), int(lmks[i,1])]
        parts.append(outline)
    parts.append([int(lmks[45,0]), int(lmks[45,1])])

    parts = np.array(parts)
    filler = cv2.convexHull(parts)
    cv2.fillConvexPoly(mask, filler, 1)
   
    return mask

def get_eyebrow_mask(cv_img, lmks, rec=0, inpaint_by_skimage=False):
   mask_right = np.zeros(cv_img.shape[:-1],np.uint8) 
   mask_left = np.zeros(cv_img.shape[:-1],np.uint8) 
            
   parts1 = []
   parts2 = []
   for i in range(33,38):
      outline1 = [int(lmks[i,0]), int(lmks[i,1])]
      outline2 = [int(lmks[i+5,0]), int(lmks[i+5,1])]
      parts1.append(outline1)    
      parts2.append(outline2)
   for i in range(67,63,-1):
      outline1 = [int(lmks[i,0]), int(lmks[i,1])]
      outline2 = [int(lmks[i+4,0]), int(lmks[i+4,1])]
      parts1.append(outline1)    
      parts2.append(outline2)
      
   parts1 = np.array(parts1)
   parts2 = np.array(parts2)
   filler1 = cv2.convexHull(parts1)
   filler2 = cv2.convexHull(parts2)
   cv2.fillConvexPoly(mask_left, filler1, 1)
   cv2.fillConvexPoly(mask_right, filler2, 1)

   return mask_left, mask_right

def get_mouth_mask(cv_img, lmks):
    mask = np.zeros(cv_img.shape[:-1],np.uint8)
    parts = []
    for i in range(84,96):
        outline = [int(lmks[i,0]), int(lmks[i,1])]
        parts.append(outline)

    parts = np.array(parts)
    filler = cv2.convexHull(parts)
    cv2.fillConvexPoly(mask, filler, 1)
    return mask

def pil_to_cv2(pil_img):
    numpy_image=np.array(pil_img)
    cv2_image=cv2.cvtColor(numpy_image, cv2.COLOR_RGB2BGR)
    return cv2_image

def cv2_to_pil(cv_img):
    image = Image.fromarray(cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB))
    return image

def exist_black(cv_img, check_thresh = 0.005):
    h,w,c = cv_img.shape
    lighten_image = adjust_gamma(cv_img, gamma=3)
    black_count = lighten_image[:,:,0]+lighten_image[:,:,1]+lighten_image[:,:,2]
    
    if np.sum(black_count == 0)>check_thresh*h*w:
        return 1
    else:
        return 0
    

    
def draw_by_map(count, face_img, bg_img, warp_map_back, cv2_trans_inv, map_kernel, size_ori):

   for i in range(count):
       
       cur_face_img = face_img[:,:,3*i:3*(i+1)]
       cur_warp_map_back = warp_map_back[:,:,3*i:3*(i+1)]
       cur_cv2_trans_inv = cv2_trans_inv[:,3*i:3*(i+1)]
       
       warp_img_back = cv2.warpAffine(cur_face_img, cur_cv2_trans_inv, size_ori)
     
         
       if mask_type==1:
            cur_warp_map_back = cv2.GaussianBlur(cur_warp_map_back, (map_kernel[i]+4,map_kernel[i]+4),0)
           
            cur_warp_map_back[cur_warp_map_back>0] = 255
            cur_warp_map_back = cv2.GaussianBlur(cur_warp_map_back, (map_kernel[i],map_kernel[i]),0)
            cur_warp_map_back = cv2.resize(cur_warp_map_back, size_ori)
            
            cur_warp_map_back = cur_warp_map_back.astype(np.float32)/255
     
            blend_back_img = cur_warp_map_back*bg_img+(1-cur_warp_map_back)*warp_img_back
          
  
       elif mask_type==2:
            #mask边缘模糊
            
            cur_warp_map_back = cv2.GaussianBlur(cur_warp_map_back, (map_kernel[i],map_kernel[i]),0)
            cv2.normalize(cur_warp_map_back,  cur_warp_map_back, 0, 255, cv2.NORM_MINMAX)
#            cv2.imwrite("cur_warp_map_back.jpg", cur_warp_map_back)

            cur_warp_map_back = cv2.resize(cur_warp_map_back, size_ori)
            
          
            cur_warp_map_back = cur_warp_map_back.astype(np.float32)/255
            
            blend_back_img = cur_warp_map_back*bg_img+(1-cur_warp_map_back)*warp_img_back
           
  
 
   return blend_back_img

def align_face(cv_img, lmks, resize_factor, ori_shape, map_kernel_ratio, app_id=0, fo_lmks=None,
               target_size=384, eye_ratio=6):

    height_ori, width_ori = ori_shape
    
    height, width, channels = cv_img.shape
    
   
    left_eye_x, left_eye_y = lmks[74]
    right_eye_x, right_eye_y = lmks[77]
    eye_middle_x = (left_eye_x+right_eye_x)/2
    eye_middle_y = (left_eye_y+right_eye_y)/2
    nose = np.copy(lmks[45])

    eye_to_eye = np.sqrt((left_eye_x -right_eye_x)**2+(left_eye_y -right_eye_y)**2)
    crop_size = eye_to_eye*eye_ratio
    eye_to_nose = np.linalg.norm(np.array([eye_middle_x, eye_middle_y]) - nose)


    tar_eye_y = 0.51 - eye_to_nose/(eye_to_eye*eye_ratio)
    tar_left_eye_x = (1-1/eye_ratio)/2
    tar_right_eye_x = 1-(1-1/eye_ratio)/2

    fixed_points = np.array([[tar_left_eye_x, tar_right_eye_x,  0.5], [tar_eye_y, tar_eye_y, 0.51]]).transpose() * target_size
    lmks_cur = np.array([[left_eye_x, right_eye_x,  nose[0]], [left_eye_y, right_eye_y, nose[1]]]).transpose()
    trans, trans_inv = get_similarity_transform(lmks_cur.copy(), fixed_points, reflective=True)
 
    cv2_trans = cvt_tform_mat_for_cv2(trans)
    cv2_trans_inv = cvt_tform_mat_for_cv2(trans_inv)
    
    z_dim_lmks = np.ones(lmks.shape[0])
    lmks_final = np.column_stack((lmks, z_dim_lmks))
    lmks_final = np.matmul(lmks_final, cv2_trans.transpose())
    
    
    if fix_kernel==1:
        map_kernel = map_kernel_ratio

    else:
        min_x, min_y = lmks.min(axis=0)
        max_x, max_y = lmks.max(axis=0)
        len_x = max_x - min_x
        len_y = max_y - min_y
        map_kernel = int(0.5 * (len_x + len_y) * map_kernel_ratio)
        map_kernel = map_kernel if map_kernel%2==1 else map_kernel + 1
        print(("map_kernel", map_kernel))
    shrink_map_kernel = int(0.5*map_kernel) #jpmanga
    if app_id==0:
        shrink_map_kernel_2 = int(0.2*map_kernel) #jianying
    else:
        shrink_map_kernel_2 = int(map_kernel) #test, xingtu, tiekuang
    
    shrink_map_kernel = shrink_map_kernel if shrink_map_kernel%2==1 else shrink_map_kernel + 1
    if out_face==1:
        z_dim_fo_lmks = np.ones(fo_lmks.shape[0])
        fo_lmks_final = np.column_stack((fo_lmks, z_dim_fo_lmks))
        fo_lmks_final = np.matmul(fo_lmks_final, cv2_trans.transpose())
       
    warp_img = cv2.warpAffine(cv_img, cv2_trans, (target_size, target_size))
    
    #计算mask
  
    if out_face == 0:
        face_mask = get_face_mask(cv_img, lmks,  expand_eyebrows=True)
    else:
        face_mask = get_face_mask(cv_img, fo_lmks)
    
    
    face_mask = (face_mask*255).astype(np.uint8)
   
    border_map = np.zeros((height, width, 1), np.uint8)
    if mask_type==1:
        blend_map = border_map
        warp_map_full = np.concatenate((face_mask, blend_map), axis=2)
        warp_map_full = cv2.warpAffine(warp_map_full, cv2_trans, (target_size, target_size), borderValue=(0,255))
        warp_map_back = cv2.warpAffine(warp_map_full[:,:,1:], cv2_trans_inv, (int(width), int(height)),borderValue=(255))
#        cv2.imwrite("warp_map_back_ori.jpg", warp_map_back)
        map_kernel = int(map_kernel/(height_ori/height))
        map_kernel = map_kernel if map_kernel%2==1 else map_kernel + 1
        if map_kernel<1:
            map_kernel = 1
        
        #test
        cv_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (shrink_map_kernel, shrink_map_kernel))
        warp_map_back = cv2.dilate(warp_map_back, cv_kernel)
#        cv2.imwrite("warp_map_back.jpg", warp_map_back)
        
        
    elif mask_type==2:
      
        blend_map = face_mask.copy()
        blend_map = 255-blend_map
        
        
        warp_map_full = np.concatenate((face_mask, border_map, blend_map), axis=2)
        warp_map_full = cv2.warpAffine(warp_map_full, cv2_trans, (target_size, target_size), borderValue=(0,255, 255))
        warp_map_back_full = cv2.warpAffine(warp_map_full[:,:,1:], cv2_trans_inv, (int(width), int(height)),borderValue=(255,255))

         #图片边框mask缩小 chunji v
        cv_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (shrink_map_kernel, shrink_map_kernel))
     
        #jpmanga
#        border_map_back = cv2.dilate(warp_map_back_full[:,:,:1], cv_kernel)
#        warp_map_back = cv2.erode(warp_map_back_full[:,:,1:2], cv_kernel)
        
        #test
        border_map_back = cv2.dilate(warp_map_back_full[:,:,:1], cv_kernel)
        cv_kernel2 = cv2.getStructuringElement(cv2.MORPH_RECT, (shrink_map_kernel_2, shrink_map_kernel_2))
        warp_map_back = cv2.erode(warp_map_back_full[:,:,1:2], cv_kernel2)
#        cv2.imwrite("warp_map_back.jpg", warp_map_back)
      
        
#        map_kernel = int(map_kernel/resize_factor)
#        map_kernel = map_kernel+1 if map_kernel%2==0 else map_kernel
        
        #图片边框mask缩小 xinghong v 
#        border_map_back = cv2.GaussianBlur(warp_map_back_full[:,:,:1], (map_kernel,map_kernel),0)
#        border_map_back[border_map_back>0] = 255
#        
#        warp_map_back = cv2.GaussianBlur(warp_map_back_full[:,:,1:2], (map_kernel,map_kernel),0)
#        warp_map_back = warp_map_back.astype(np.float32)
#        warp_map_back = warp_map_back/np.max(warp_map_back)*255
##    
#        #扩大脸部范围，防止将背景图的下巴包含进结果图
        warp_map_back[warp_map_back<255] = 0
        #最终mask是图片边框和脸部mask的并集
        warp_map_back[border_map_back>0] = 255
        warp_map_back = warp_map_back.astype(np.uint8)
        map_kernel = shrink_map_kernel
        
    warp_map_back = np.tile(warp_map_back[:,:,None],(1,1,3))
  
   

    if gamma_cor == 1:
        gray_img = cv2.cvtColor(warp_img, cv2.COLOR_BGR2GRAY)

        # detect face region
        gaussian_kernel = int(15 * change_ratio)
        gaussian_kernel_eye = int(11 * change_ratio)
        if gaussian_kernel % 2 == 0:
            gaussian_kernel = gaussian_kernel + 1
        if gaussian_kernel_eye % 2 == 0:
            gaussian_kernel_eye = gaussian_kernel_eye + 1
          
        face_mask = warp_map_full[:,:,0]

        face_mask_smooth = cv2.GaussianBlur(face_mask, (gaussian_kernel, gaussian_kernel), 0)
        face_mask_smooth = face_mask_smooth.astype(np.float32) / 255

        # exclude mouth and eye
        mouth_mask = get_mouth_mask(warp_img, lmks_final)
        mouth_mask = (mouth_mask * 255).astype(np.uint8)
        mouth_mask_smooth = cv2.GaussianBlur(mouth_mask, (gaussian_kernel, gaussian_kernel), 0)
        mouth_mask_smooth = mouth_mask_smooth.astype(np.float32) / 255
        eye_mask = get_eye_mask(warp_img, lmks_final)

        eye_mask_smooth = cv2.GaussianBlur(eye_mask * 255, (gaussian_kernel_eye, gaussian_kernel_eye),
                                           0)  # v4_eye15
        eye_mask_smooth[eye_mask_smooth > 0] = 255
        eye_mask_smooth = cv2.GaussianBlur(eye_mask_smooth, (gaussian_kernel, gaussian_kernel), 0)
        eye_mask_smooth = eye_mask_smooth.astype(np.float32) / 255

        face_mask_smooth = face_mask_smooth - mouth_mask_smooth - eye_mask_smooth

        # exclude hair region
        eye_y = lmks_final[52, 1]
        for i in range(52, 64):
            eye_y = np.min((eye_y, int(lmks_final[i, 1])))
        eye_y = int(eye_y)
        gray_img[eye_y:, :] = 255
        
        if hair_thresh_strategy==2:
            eyebrow_mask_l, eyebrow_mask_r = get_eyebrow_mask(warp_img, lmks_final)
            hair_thresh_l = np.mean(gray_img[eyebrow_mask_l>0].astype(np.float32))
            hair_thresh_r = np.mean(gray_img[eyebrow_mask_r>0].astype(np.float32))
            hair_thresh = np.max((hair_thresh_l, hair_thresh_r, hair_thresh_hard))
        else:
            hair_thresh = hair_thresh_hard
       

        face_mask_smooth[gray_img < hair_thresh] = 0
        face_mask_smooth = cv2.GaussianBlur(face_mask_smooth, (gaussian_kernel, gaussian_kernel), 0)
        
        # gamma correction for the whole image
        lighten_image = adjust_gamma(warp_img, gamma=1.5)
        out_img = warp_img.copy()
        # weighted sum
        for i in range(3):
            out_img[:, :, i] = lighten_image[:, :, i] * face_mask_smooth + out_img[:, :, i] * (1 - face_mask_smooth)
        out_img[out_img > 255] = 255
        out_img[out_img < 0] = 0
        warp_img = out_img.astype(np.uint8)
  
#    print(warp_map_back.shape)
 
    cv2_trans_inv = cv2_trans_inv/resize_factor
    return warp_img, cv2_trans_inv, warp_map_back, map_kernel


def get_nose_mask(cv_img, lmks):
    mask = np.zeros(cv_img.shape[:-1],np.uint8) 
    parts = []
    parts.append([int(lmks[55,0]), int(lmks[55,1])]) 
    parts.append([int(lmks[82,0]), int(lmks[82,1])]) 
    parts.append([int(lmks[49,0]), int(lmks[49,1])]) 
    parts.append([int(lmks[83,0]), int(lmks[83,1])]) 
    parts.append([int(lmks[58,0]), int(lmks[58,1])]) 
    parts.append([int(lmks[43,0]), int(lmks[43,1])]) 
           
    parts = np.array(parts)
    filler = cv2.convexHull(parts)
    cv2.fillConvexPoly(mask, filler, 1)
    return mask
 
def get_average_color(cv_img, mask):
    ave_color = np.zeros(3)
#    print(cv_img.shape[-1])
    for i in range(cv_img.shape[-1]):
        channel = cv_img[:,:,i]
        ave_color[i] = np.mean(channel[mask>0.5])
    return ave_color

def get_average_lumi(cv_img, mask):
    lumi_img = cv2.cvtColor(cv_img, cv2.COLOR_BGR2GRAY)
    if len(mask>0)==0:
        return 255
    return np.mean(lumi_img[mask>0])


def get_black_class(mean_lumi, spec_class=[255,110,0]):
#    print(mean_lumi)
    for i in range(len(spec_class)-1):
        if mean_lumi<=spec_class[i] and mean_lumi>spec_class[i+1]:
            return 3+i
    return 2+len(spec_class)


def apply_lut(image, lut):
    lut_image  = Image.fromarray(cv2.cvtColor(image.astype(np.uint8), cv2.COLOR_BGR2RGB))
    lut_image = lut_image.filter(lut)
    lut_image = cv2.cvtColor(np.array(lut_image), cv2.COLOR_RGB2BGR)  
    
    return lut_image
    
        
def read_lut_file(path):
    lut = cv2.imread(path)
    lut = cv2.cvtColor(lut, cv2.COLOR_BGR2RGB).astype(np.float32)
 
    lut_table = np.zeros((64*64*64,3), np.uint8)
    for j in range(8):
        for i in range(8):
            cur_block = lut[64*j:64*(j+1), 64*i:64*(i+1),:]
            cur_block = cur_block.reshape(64*64,3)
            lut_table[(j*8+i)*64*64:(j*8+i+1)*64*64,:] = cur_block

    lut_table = lut_table / 255.0
    return lut_table


def get_new_texture(texture, out_width, out_height):
    out_texture = np.zeros((out_height, out_width))
    dup_height = math.ceil(out_height/texture.shape[0])
    dup_width = math.ceil(out_width/texture.shape[1])
    big_texture = np.zeros((dup_height*texture.shape[0],
                            dup_width*texture.shape[1]))
    for i in range(dup_height):
        for j in range(dup_width):
            big_texture[i*texture.shape[0]:(i+1)*texture.shape[0], j*texture.shape[1]:(j+1)*texture.shape[1]] = texture[:,:,0]
    
#    print(big_texture.shape)
    out_texture = big_texture[:out_height, :out_width]
   
#    print(out_texture.shape)
    return out_texture
    
    

def particle_qy(image_ori, texture, ratio=0.4):
    image = image_ori.copy()
    height, width, _ = image.shape
    
  
#    image_normalize = 
#    texture = texture.astype(np.float32)/255.0
    ori_gray = cv2.cvtColor(image.astype(np.uint8), cv2.COLOR_BGR2GRAY).astype(np.float32)/255.0*2-1
    image = image.astype(np.float32)/255.0
    abs_ori_gray = np.abs(ori_gray)
    abs_ori_gray2 = abs_ori_gray * abs_ori_gray
    abs_ori_gray3 = abs_ori_gray2 * abs_ori_gray
    grain_gray = get_new_texture(texture, width, height)
    
    grain_gray = grain_gray * 2.0 - 1.0
    strength = 0.49019608
    mask = np.zeros(ori_gray.shape)
    
    mask[ori_gray>0] = (abs_ori_gray3[ori_gray>0] * 0.5 
                        + abs_ori_gray2[ori_gray>0] * 0.5) * (strength - 0.03921569)
    
    mask[ori_gray<=0] = (abs_ori_gray2[ori_gray<=0] * 0.4 + abs_ori_gray[ori_gray<=0] * 0.6) * strength
    
    
   
    new_color = image + np.tile(grain_gray[:,:,None], (1,1,3)) * (strength - np.tile(mask[:,:,None], (1,1,3)))
    new_color[new_color>=1] = 1
    new_color[new_color<0] = 0
    new_color = image*(1-ratio)+new_color*(ratio)
    new_color[new_color>=1] = 1
    new_color[new_color<0] = 0
    new_color = (new_color*255).astype(np.uint8)
    
    return new_color



def post_proc(image, lut, texture, app_id=0):
#    apply lut
#    time_st_post = time.time()
#    time_st = time.time()
    lut_image  = Image.fromarray(cv2.cvtColor(image.astype(np.uint8), cv2.COLOR_BGR2RGB))
    lut_image = lut_image.filter(lut)
#    times["lut time"] = time.time()-time_st
    #锐化
#    time_st = time.time()
#    lut_image = lut_image.filter(ImageFilter.SHARPEN)
   

#    apply texture  
    if app_id==0:
        lut_image = lut_image.filter(ImageFilter.SHARPEN)
        lut_image = cv2.cvtColor(np.array(lut_image), cv2.COLOR_RGB2BGR).astype(np.float32)  
        lut_image = particle_qy(image,texture)
        print("in new particle_qy")
    else:
        lut_image = cv2.cvtColor(np.array(lut_image), cv2.COLOR_RGB2BGR).astype(np.float32)  
        lut_image = lut_image/255.0
       
    #    time_st = time.time()
        if texture.shape[0]<image.shape[0] or texture.shape[1]<image.shape[1]:   
            texture = cv2.resize(texture, (image.shape[1], image.shape[0]))
        else:
            texture = texture[:image.shape[0], :image.shape[1], :]
    
        lut_image_ori = lut_image.copy()
    
        lut_image[lut_image_ori<0.5] = lut_image[lut_image_ori<0.5]*2.0*texture[:,:,:3][lut_image_ori<0.5]
        lut_image[lut_image_ori>=0.5] = 1.0-(1.0-lut_image[lut_image_ori>=0.5])*2.0*(1.0-texture[:,:,:3][lut_image_ori>=0.5])
        
        lut_image[lut_image>1.0] = 1.0
        lut_image[lut_image<0] = 0.0
    
        lut_image = lut_image*255

    return lut_image
    


out_dir = './'
gamma_cor = 1 #脸部gamma矫正
reflect_pad = 0 #对黑边的处理

align_method = 1 #脸部align方法

resize=1 #裁剪前是否降采样
resize_bound = 384 #降采样最短边
target_size = 384 #输出分辨率
out_face = 1 #使用外轮廓点进行gamma
check_black = 0 #是否做黑边检测
fix_kernel = 2 #是否fix kernel size of map

hair_thresh_hard = 70
hair_thresh_strategy = 2 #1:固定threshold  2:根据眉毛颜色确定threshold
nose_tgt_y = 0.51*target_size
nose_tgt_x = 0.5*target_size
change_ratio = target_size/256
border_width = 3

mask_type = 2 #1:拼回整个脸部裁剪图 2:拼回脸部

class LandDetector():
    def __init__(self, hair_thresh=70, hair_thresh_strategy=2, pm_small_model=0, bg_color=0):
        self.hair_thresh = hair_thresh
        self.hair_thresh_strategy = hair_thresh_strategy
        self.bg_color =  bg_color
        
        model_path = './model/tt_face_v7.0.model'.encode()
        extra_path = './model/tt_face_extra_v10.0.model'.encode()
        land_lib_path = './align_v4_linux/lib/land.so'
        fo_model_path = './model/tt_face_new_landmark_v1.0.model'.encode()
        fo_lib_path = './align_v4_linux/lib/facenewlandmark.so'
        attr_model_path = './model/tt_face_attribute_v5.0.model'.encode()
        attr_extra_path = './model/tt_face_attribute_extra_v2.0.model'.encode()
        attr_lib_path = './align_v4_linux/lib/faceattribute.so'
        pm_model_path = './model/tt_matting_v12.0.model'.encode()
        pm_model_large_path = './model/tt_matting_large_v2.0.model'.encode()
        pm_lib_path = './align_v4_linux/lib/portraitmatting.so'
        lut_jianying_path = './luts/lut.png'
        lut_xingtu_paths = ['./luts/xingtu_lut1.png','./luts/xingtu_lut2.png','./luts/xingtu_lut3.png']
        
        jianying_bg_lut_path = './luts/jianying_bg_lut.png'
        jianying_texture_path = './luts/texture.png'
        xingtu_texture_paths = ['./luts/xingtu_texture1.png','./luts/xingtu_texture2.png','./luts/xingtu_texture3.png']
        
        if out_face == 1:
            self.face_alignment_outline = fao.FaceAlignment(platform='Linux', model_path=model_path, extra_path=extra_path,
                                                            land_lib_path=land_lib_path, pad=[0.4, 0.4, 0.8, 1.2], use_detection=False)
            self.face_outlines = fao.FaceOutlines(platform='Linux', model_path=fo_model_path, faceoutlines_lib_path=fo_lib_path,smooth_mode=0)

        self.face_detection_fast = fao.FaceDetection(platform='Linux', model_path=model_path, extra_path=extra_path, land_lib_path=land_lib_path,
                                        slow_mode=False, full_mode=True, video_mode=False, detect_interval=1)
        self.face_attr = faceAttr.FaceAttribute(attr_model_path,  attr_extra_path, attr_lib_path, slow_mode=True)   
        
        if pm_small_model==0:
            self.portrait_matt = pm.PortraitMatting(pm_model_path, pm_lib_path, pm_small_model)
        else:
            self.portrait_matt = pm.PortraitMatting(pm_model_large_path, pm_lib_path, pm_small_model)
            
        self.jianying_lut = ImageFilter.Color3DLUT(64, read_lut_file(lut_jianying_path), target_mode=None, _copy_table=False)
        self.jianying_bg_lut = ImageFilter.Color3DLUT(64, read_lut_file(jianying_bg_lut_path), target_mode=None, _copy_table=False)
        self.jianying_texture = cv2.imread(jianying_texture_path, -1).astype(np.float32)/255.0
        self.xingtu_luts = []
        self.xingtu_textures = []
        for i in range(len(lut_xingtu_paths)):
            self.xingtu_luts.append(ImageFilter.Color3DLUT(64, read_lut_file(lut_xingtu_paths[i]), target_mode=None, _copy_table=False))
            self.xingtu_textures.append(cv2.imread(xingtu_texture_paths[i], -1).astype(np.float32)/255.0)
        

    def detect(self, cv_img):
        count, face_info = self.face_detection_fast.detect106_full(cv_img, 'BGR')
        return count
    
    def clip(self, cv_img, crop_bg=0, max_face=5, app_id=0, bg_mask = None):
    
        height, width, channels = cv_img.shape
        width_ori = width
        height_ori = height
        bg_img = cv_img.copy()
       
        if crop_bg==1:
            try:
                if bg_mask is not None:
                    res_img = np.tile(bg_mask[:,:,None], (1,1,3))
                    res_img = res_img.astype(np.float32)/255.0
                    cv_img = cv_img*res_img+self.bg_color*(1-res_img)
                    cv_img[cv_img<0] = 0
                    cv_img[cv_img>255] = 255
                    cv_img = cv_img.astype(np.uint8)
                    
                else:
                    ret, res_img = self.portrait_matt.detect(cv_img)
                    cv_img[res_img==0] = self.bg_color
                    bg_mask = res_img
            except:
                bg_mask = np.zeros(cv_img.shape[:2])
                print("no bg detected")
        else:
            bg_mask = None
#        bg_img = cv2.resize(cv_img, (target_size, target_size));
        
        resize_factor = 1
        # 降采样
        if resize == 1:
#            pil_img = cv2_to_pil(cv_img)
            if height > resize_bound and height <= width:
                resize_factor = resize_bound / height
                width = int(resize_bound / height * width)
                height = resize_bound
                cv_img = cv2.resize(cv_img, (width, height))
#                pil_img = pil_img.resize((width, height), Image.ANTIALIAS)
#                cv_img = pil_to_cv2(pil_img)
            elif width > resize_bound and width < height:
                resize_factor = resize_bound / width
                height = int(resize_bound / width * height)
                width = resize_bound
                cv_img = cv2.resize(cv_img, (width, height))
#                pil_img = pil_img.resize((width, height), Image.ANTIALIAS)
#                cv_img = pil_to_cv2(pil_img)

        # 轮廓点检测
        count, face_info = self.face_detection_fast.detect106_full(cv_img, 'BGR')
        if count <= 0:
#            print("No face detected!")
            return 0, None, bg_img, None, None, None, bg_mask, None 
        if out_face == 1:
            fo_lms = self.face_outlines.detect_full(cv_img, face_info[0], count, face_info[7], False)
            fo_lmks = fo_lms.reshape((-1, 2))
        
        ret, gender = self.face_attr.detect_gender_full(cv_img, self.face_detection_fast.point106, count)
       
       
        lms = face_info[0]
        lmks = lms.reshape((-1, 2))
        count = np.min((count,max_face))

        if fix_kernel==1:
#            map_kernel_fix = int(75/resize_factor)
#            map_kernel_fix = map_kernel_fix if map_kernel_fix%2==1 else map_kernel_fix + 1
            map_kernel_ratio = 5
        else:
            if app_id==0:
                map_kernel_ratio = 1 #jianying original
            else:
                map_kernel_ratio = 0.6 #test, xingtu, tiekuang
#            map_kernel_ratio = 0.75 #jpmanga origin
            print(("map_kernel_ratio", map_kernel_ratio))
            
            
        # alignment
        map_kernel = []
        for i in range(count):
           
            cur_lmks = lmks[106*i:106*(i+1)].copy()
            if out_face == 0:
                cur_warp_img, cur_cv2_trans_inv, cur_warp_map_back, cur_map_kernel = align_face(cv_img, cur_lmks, resize_factor, (height_ori, width_ori), map_kernel_ratio, app_id=app_id)
            else:
               cur_fo_lmks = fo_lmks[44*i:44*(i+1)].copy()
               cur_warp_img, cur_cv2_trans_inv, cur_warp_map_back, cur_map_kernel = align_face(cv_img, cur_lmks, resize_factor, (height_ori, width_ori), map_kernel_ratio, app_id=app_id, fo_lmks=cur_fo_lmks) 
            if i==0:
                warp_img = cur_warp_img
                cv2_trans_inv = cur_cv2_trans_inv
                warp_map_back = cur_warp_map_back
                map_kernel = np.append(map_kernel,int(cur_map_kernel))
#                print(cur_map_kernel)
             
            else:
                warp_img = np.concatenate((warp_img, cur_warp_img), axis = 2)
                cv2_trans_inv = np.concatenate((cv2_trans_inv, cur_cv2_trans_inv), axis = 1)
                warp_map_back = np.concatenate((warp_map_back, cur_warp_map_back), axis = 2)
                map_kernel = np.append(map_kernel, int(cur_map_kernel))   
        return count, warp_img, bg_img, cv2_trans_inv, warp_map_back, map_kernel, bg_mask, gender




if __name__ == "__main__":
#    if len(sys.argv)>2:
#        print("No input image path")
#        #sys.exit(0)
    if len(sys.argv)>1:
        land = LandDetector(hair_thresh=hair_thresh_hard, hair_thresh_strategy=hair_thresh_strategy)
        cv_img = cv2.imread(sys.argv[1])
        start = time.time()
        _, warp_img, ori_face, border_map = land.clip(cv_img)
        print(time.time()-start)
#        for x,y in times.items():
#            print(x,str(y))
        cv2.imwrite(os.path.join(out_dir,'test_out.jpg'), warp_img)
        cv2.imwrite(os.path.join(out_dir,'test_out_large.jpg'), ori_face)
        cv2.imwrite(os.path.join(out_dir,'map.jpg'), border_map)
    else:
        try:
            
            cv_img = cv2.imread("test3.jpg")#cv2.imread(sys.argv[1])
            #pdb.set_trace()
            land = LandDetector(hair_thresh=hair_thresh_hard, hair_thresh_strategy=hair_thresh_strategy)
            start = time.time()
            _, warp_img, ori_face, border_map = land.clip(cv_img)
            print(time.time()-start)
            #写入图片
            print("saving output image to "+os.path.join(out_dir,'test_out.jpg'))
            cv2.imwrite(os.path.join(out_dir,'test_out.jpg'), warp_img)
            cv2.imwrite(os.path.join(out_dir,'test_out_large.jpg'), ori_face)
            cv2.imwrite(os.path.join(out_dir,'map.jpg'), border_map)
        except Exception as e:
            print(e)
           

