#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 25 14:56:46 2019

align method v4 using smash

detailed explanation can be found in https://bytedance.feishu.cn/docs/doccnCvmiIEPVcQHvqymVal7tCb#HyAFdY

@author: huxinghong
"""



import sys
import numpy as np
import cv2
from .utils.matlab_cp2tform import get_similarity_transform, cvt_tform_mat_for_cv2
import os
import math
from .face_alignment import face_alignment_outline as fao
from .face_alignment.face_alignment_outline import get_mask
from .face_alignment import get_face_attribute as faceAttr
from .portrait_matting import portrait_matting as pm
from .face_alignment import face_alignment_pet as fap
from PIL import Image, ImageFilter
import time
import random


IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]


lms_id = np.array(
            [33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,
            56,57,58,59,60,61,62,63,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,
            100,101,102,103], dtype=np.int32)
std_lms_global = np.array([
            [ 0.000213256,  0.106454  ], #17 33
            [ 0.0752622,    0.038915  ], #18 34
            [ 0.18113,      0.0187482 ], #19 35
            [ 0.29077,      0.0344891 ], #20 36
            [ 0.393397,     0.0773906 ], #21 37
            [ 0.586856,     0.0773906 ], #22 38
            [ 0.689483,     0.0344891 ], #23 39
            [ 0.799124,     0.0187482 ], #24 40
            [ 0.904991,     0.038915  ], #25 41
            [ 0.98004,      0.106454  ], #26 42
            [ 0.490127,     0.203352  ], #27 43
            [ 0.490127,     0.307009  ], #28 44
            [ 0.490127,     0.409805  ], #29 45
            [ 0.490127,     0.515625  ], #30 46
            [ 0.36688,      0.587326  ], #31 47
            [ 0.426036,     0.609345  ], #32 48
            [ 0.490127,     0.628106  ], #33 49
            [ 0.554217,     0.609345  ], #34 50
            [ 0.613373,     0.587326  ], #35 51
            [ 0.121737,     0.216423  ], #36 52
            [ 0.187122,     0.178758  ], #37 53
            [ 0.265825,     0.179852  ], #38 54
            [ 0.334606,     0.231733  ], #39 55
            [ 0.260918,     0.245099  ], #40 56
            [ 0.182743,     0.244077  ], #41 57
            [ 0.645647,     0.231733  ], #42 58
            [ 0.714428,     0.179852  ], #43 59
            [ 0.793132,     0.178758  ], #44 60
            [ 0.858516,     0.216423  ], #45 61
            [ 0.79751,      0.244077  ], #46 62
            [ 0.719335,     0.245099  ], #47 63
            [ 0.254149,     0.780233  ], #48 84
            [ 0.340985,     0.745405  ], #49 85
            [ 0.428858,     0.727388  ], #50 86
            [ 0.490127,     0.742578  ], #51 87
            [ 0.551395,     0.727388  ], #52 88
            [ 0.639268,     0.745405  ], #53 89
            [ 0.726104,     0.780233  ], #54 90
            [ 0.642159,     0.864805  ], #55 91
            [ 0.556721,     0.902192  ], #56 92
            [ 0.490127,     0.909281  ], #57 93
            [ 0.423532,     0.902192  ], #58 94
            [ 0.338094,     0.864805  ], #59 95
            [ 0.290379,     0.784792  ], #60 96
            [ 0.428096,     0.778746  ], #61 97
            [ 0.490127,     0.785343  ], #62 98
            [ 0.552157,     0.778746  ], #63 99
            [ 0.689874,     0.784792  ], #64 100
            [ 0.553364,     0.824182  ], #65 101
            [ 0.490127,     0.831803  ], #66 102
            [ 0.42689 ,     0.824182  ]  #67 103
            ], dtype=np.float32)
       
data_cat = (np.array([
    [136, 316, 0],
    [218, 551, 4],
    [413, 636, 8],
    [596, 525, 12],
    [650, 273, 16],
    #[645, 278, 21],
    [395, 371, 42],
    #[657, 738, 44],
    [354, 494, 45],
    [449, 494, 46],
    #[657, 818, 47],
    [409, 552, 48],
    [279, 374, 80],
    [506, 374, 81]], dtype=np.float))
std_points_cat = data_cat[:, :2]/np.array([[790., 702]])
subset_cat = data_cat[:, 2].astype(np.int)

data_dog = (np.array([
    [136, 316, 0],
    [218, 551, 4],
    [413, 636, 8],
    [596, 525, 12],
    [650, 273, 16],
    #[645, 278, 21],
    [395, 371, 28],
    #[657, 738, 44],
    [354, 494, 33],
    [449, 494, 37],
    #[657, 818, 47],
    [409, 552, 50],
    [279, 374, 74],
    [506, 374, 75]], dtype=np.float))
std_points_dog = data_dog[:, :2]/np.array([[790., 702]])
subset_dog = data_dog[:, 2].astype(np.int)   


 
def pad_std_lms(std_lms, pad):
    new_std_lms = std_lms.copy()
    p = pad
    if p[0] > 0 or p[1] > 0 or p[2] > 0 or p[3] > 0:
        new_std_lms[:, 0] = (new_std_lms[:, 0] / (1 + p[0] + p[1])) + p[0] / (1 + p[0] + p[1])
        new_std_lms[:, 1] = (new_std_lms[:, 1] / (1 + p[2] + p[3])) + p[2] / (1 + p[2] + p[3])

    return new_std_lms



def get_eye_mask(cv_img, lmks, rec=0):
    mask = np.zeros(cv_img.shape[:-1],np.uint8)

    parts1 = []
    parts2 = []
    for i in range(52,58):
        outline1 = [int(lmks[i,0]), int(lmks[i,1])]
        outline2 = [int(lmks[i+6,0]), int(lmks[i+6,1])]
        parts1.append(outline1)
        parts2.append(outline2)

    parts1 = np.array(parts1)
    parts2 = np.array(parts2)
    filler1 = cv2.convexHull(parts1)
    filler2 = cv2.convexHull(parts2)
    cv2.fillConvexPoly(mask, filler1, 1)
    cv2.fillConvexPoly(mask, filler2, 1)
    return mask

def adjust_gamma(image, gamma=1.0):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
    return cv2.LUT(image,table)

def get_face_mask(cv_img, lmks, expand_eyebrows=False):
    mask = get_mask(cv_img.shape[:2], lmks, expand_eyebrows=expand_eyebrows)
    return mask

def get_halfbottom_mask(cv_img, lmks):
    mask = np.zeros(cv_img.shape[:-1],np.uint8)
    parts = []
    for i in range(7,26):
        outline = [int(lmks[i,0]), int(lmks[i,1])]
        parts.append(outline)
    parts.append([int(lmks[45,0]), int(lmks[45,1])])

    parts = np.array(parts)
    filler = cv2.convexHull(parts)
    cv2.fillConvexPoly(mask, filler, 1)
   
    return mask

def get_eyebrow_mask(cv_img, lmks, rec=0, inpaint_by_skimage=False):
   mask_right = np.zeros(cv_img.shape[:-1],np.uint8) 
   mask_left = np.zeros(cv_img.shape[:-1],np.uint8) 
            
   parts1 = []
   parts2 = []
   for i in range(33,38):
      outline1 = [int(lmks[i,0]), int(lmks[i,1])]
      outline2 = [int(lmks[i+5,0]), int(lmks[i+5,1])]
      parts1.append(outline1)    
      parts2.append(outline2)
   for i in range(67,63,-1):
      outline1 = [int(lmks[i,0]), int(lmks[i,1])]
      outline2 = [int(lmks[i+4,0]), int(lmks[i+4,1])]
      parts1.append(outline1)    
      parts2.append(outline2)
      
   parts1 = np.array(parts1)
   parts2 = np.array(parts2)
   filler1 = cv2.convexHull(parts1)
   filler2 = cv2.convexHull(parts2)
   cv2.fillConvexPoly(mask_left, filler1, 1)
   cv2.fillConvexPoly(mask_right, filler2, 1)

   return mask_left, mask_right

def get_mouth_mask(cv_img, lmks):
    mask = np.zeros(cv_img.shape[:-1],np.uint8)
    parts = []
    for i in range(84,96):
        outline = [int(lmks[i,0]), int(lmks[i,1])]
        parts.append(outline)

    parts = np.array(parts)
    filler = cv2.convexHull(parts)
    cv2.fillConvexPoly(mask, filler, 1)
    return mask

def pil_to_cv2(pil_img):
    numpy_image=np.array(pil_img)
    cv2_image=cv2.cvtColor(numpy_image, cv2.COLOR_RGB2BGR)
    return cv2_image

def cv2_to_pil(cv_img):
    image = Image.fromarray(cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB))
    return image

def exist_black(cv_img, check_thresh = 0.005):
    h,w,c = cv_img.shape
    lighten_image = adjust_gamma(cv_img, gamma=3)
    black_count = lighten_image[:,:,0]+lighten_image[:,:,1]+lighten_image[:,:,2]
    
    if np.sum(black_count == 0)>check_thresh*h*w:
        return 1
    else:
        return 0
    

    
def draw_by_map(count, face_img, bg_img, warp_map_back, cv2_trans_inv, map_kernel, size_ori):

   for i in range(count):
       
       cur_face_img = face_img[:,:,3*i:3*(i+1)]
       cur_warp_map_back = warp_map_back[:,:,3*i:3*(i+1)]
       cur_cv2_trans_inv = cv2_trans_inv[:,3*i:3*(i+1)]
       

    #    warp_img_back = cv2.warpAffine(cur_face_img, cur_cv2_trans_inv, size_ori)
       
       warp_img_back = cv2.warpAffine(cur_face_img, cur_cv2_trans_inv, size_ori, flags=cv2.WARP_INVERSE_MAP | cv2.INTER_LINEAR)
    #    cv2.imwrite("warp_img_back.jpg", warp_img_back)

       if mask_type==1:
            # cur_warp_map_back = cv2.GaussianBlur(cur_warp_map_back, (map_kernel[i]+4,map_kernel[i]+4),0)
           
            # cur_warp_map_back[cur_warp_map_back>0] = 255
            # cur_warp_map_back = cv2.GaussianBlur(cur_warp_map_back, (map_kernel[i],map_kernel[i]),0)
            cur_warp_map_back = cv2.resize(cur_warp_map_back, size_ori)
            cv2.imwrite("cur_warp_map_back.jpg", cur_warp_map_back)
            
            cur_warp_map_back = cur_warp_map_back.astype(np.float32)/255
            print((cur_warp_map_back.shape, bg_img.shape, warp_img_back.shape))
     
            blend_back_img = (1-cur_warp_map_back)*bg_img+cur_warp_map_back*warp_img_back
          
  
       elif mask_type==2:
            #mask边缘模糊
            
            cur_warp_map_back = cv2.GaussianBlur(cur_warp_map_back, (map_kernel[i],map_kernel[i]),0)
            cv2.normalize(cur_warp_map_back,  cur_warp_map_back, 0, 255, cv2.NORM_MINMAX)
#            cv2.imwrite("cur_warp_map_back.jpg", cur_warp_map_back)

            cur_warp_map_back = cv2.resize(cur_warp_map_back, size_ori)
            
          
            cur_warp_map_back = cur_warp_map_back.astype(np.float32)/255
            
            blend_back_img = cur_warp_map_back*bg_img+(1-cur_warp_map_back)*warp_img_back
           
  
 
   return blend_back_img

def get_align_M_customized(landmarks, std_landmarks):
    lms = landmarks
    trans, trans_inv = get_similarity_transform(lms, std_landmarks, reflective=True)
    cv2_trans = cvt_tform_mat_for_cv2(trans)
    return cv2_trans


def align_face2(cv_img, lmks, resize_factor, ori_shape, 
               map_kernel_ratio, std_landmarks, std_lms_pos=(50, 50, 50, 50), 
               flags=cv2.INTER_CUBIC, is_pet=False, fo_lmks=None,
               target_size=(384,384), eye_ratio=6, pad=[0,0,0,0], out_size=(384,512), face_map=None, crop_bg=0, ori_img = None, bg_mask = None):
    
    aligned_face = None
    h, w = cv_img.shape[:2]
    img1 = np.full((h, w, 4), 255, np.uint8)
    img1[:,:,:3] = cv_img
    if is_pet:
        landmarks = lmks
    else:
        landmarks = lmks[lms_id]
 
    std_lms = std_landmarks.reshape((-1, 2))
    
    witdh = out_size[0] - std_lms_pos[0] - std_lms_pos[1]
    height = out_size[1] - std_lms_pos[2] - std_lms_pos[3]
    std_lms[:, 0] = std_lms[:, 0] * witdh + std_lms_pos[0]
    std_lms[:, 1] = std_lms[:, 1] * height + std_lms_pos[2]
    
    cv_trans = get_align_M_customized(landmarks, std_landmarks)
  
    aligned_face = cv2.warpAffine(img1, cv_trans, out_size, flags=flags, borderValue=(0, 0, 0))
    warp_map_back = cv2.warpAffine(face_map, cv_trans, ori_shape,  borderValue=(0, 0, 0), flags=cv2.WARP_INVERSE_MAP | cv2.INTER_CUBIC)
    
    if crop_bg>=1:
        warp_bg_mask = cv2.warpAffine(bg_mask, cv_trans, target_size)
        warp_ori_img = cv2.warpAffine(ori_img, cv_trans, target_size)
    else:
        warp_bg_mask = None
        warp_ori_img = None
   
    
    return aligned_face[:,:,:3].copy(), cv_trans, warp_map_back, 1,  warp_bg_mask, warp_ori_img

    

def align_face(cv_img, lmks, resize_factor, ori_shape, map_kernel_ratio, app_id=0, fo_lmks=None,
               target_size=384, eye_ratio=6, face_map=None, crop_bg=0, ori_img = None, bg_mask = None):

    height_ori, width_ori = ori_shape
    
    height, width, channels = cv_img.shape
    
        
   
    left_eye_x, left_eye_y = lmks[74]
    right_eye_x, right_eye_y = lmks[77]
    eye_middle_x = (left_eye_x+right_eye_x)/2
    eye_middle_y = (left_eye_y+right_eye_y)/2
    nose = np.copy(lmks[45])

    eye_to_eye = np.sqrt((left_eye_x -right_eye_x)**2+(left_eye_y -right_eye_y)**2)
    crop_size = eye_to_eye*eye_ratio
    eye_to_nose = np.linalg.norm(np.array([eye_middle_x, eye_middle_y]) - nose)


    tar_eye_y = 0.51 - eye_to_nose/(eye_to_eye*eye_ratio)
    tar_left_eye_x = (1-1/eye_ratio)/2
    tar_right_eye_x = 1-(1-1/eye_ratio)/2

    fixed_points = np.array([[tar_left_eye_x, tar_right_eye_x,  0.5], [tar_eye_y, tar_eye_y, 0.51]]).transpose() * target_size
    lmks_cur = np.array([[left_eye_x, right_eye_x,  nose[0]], [left_eye_y, right_eye_y, nose[1]]]).transpose()
    

    trans, trans_inv = get_similarity_transform(lmks_cur.copy(), fixed_points, reflective=True)
 
    cv2_trans = cvt_tform_mat_for_cv2(trans)
    cv2_trans_inv = cvt_tform_mat_for_cv2(trans_inv)
    
    z_dim_lmks = np.ones(lmks.shape[0])
    lmks_final = np.column_stack((lmks, z_dim_lmks))
    lmks_final = np.matmul(lmks_final, cv2_trans.transpose())
    
    
    if fix_kernel==1:
        map_kernel = map_kernel_ratio

    else:
        min_x, min_y = lmks.min(axis=0)
        max_x, max_y = lmks.max(axis=0)
        len_x = max_x - min_x
        len_y = max_y - min_y
        map_kernel = int(0.5 * (len_x + len_y) * map_kernel_ratio)
        map_kernel = map_kernel if map_kernel%2==1 else map_kernel + 1
        #print(("map_kernel", map_kernel))
#     shrink_map_kernel = int(0.5*map_kernel) #jpmanga
# #    if app_id==0:
# #        shrink_map_kernel_2 = int(0.2*map_kernel) #jianying
# #    else:
#     shrink_map_kernel_2 = int(map_kernel) #test, xingtu, tiekuang
    
#     shrink_map_kernel = shrink_map_kernel if shrink_map_kernel%2==1 else shrink_map_kernel + 1


    warp_map_back = cv2.warpAffine(face_map, cv2_trans, ori_shape,  borderValue=(0, 0, 0), flags=cv2.WARP_INVERSE_MAP | cv2.INTER_CUBIC)
    # cv2.imwrite("face_map.jpg", face_map)
    if gamma_cor==1:
        z_dim_fo_lmks = np.ones(fo_lmks.shape[0])
        fo_lmks_final = np.column_stack((fo_lmks, z_dim_fo_lmks))
        fo_lmks_final = np.matmul(fo_lmks_final, cv2_trans.transpose())
       
    warp_img = cv2.warpAffine(cv_img, cv2_trans, (target_size, target_size))
    if crop_bg>=1:
        warp_bg_mask = cv2.warpAffine(bg_mask, cv2_trans, (target_size, target_size))
        warp_ori_img = cv2.warpAffine(ori_img, cv2_trans, (target_size, target_size))
    else:
        warp_bg_mask = None
        warp_ori_img = None
    #计算mask
  
    if gamma_cor == 0:
        face_mask = get_face_mask(cv_img, lmks,  expand_eyebrows=True)
    else:
        face_mask = get_face_mask(cv_img, fo_lmks)
    
    
    face_mask = (face_mask*255).astype(np.uint8)
   
    border_map = np.zeros((height, width, 1), np.uint8)
    if mask_type==1:
        blend_map = border_map
        warp_map_full = np.concatenate((face_mask, blend_map), axis=2)
        warp_map_full = cv2.warpAffine(warp_map_full, cv2_trans, (target_size, target_size), borderValue=(0,255))
#         warp_map_back = cv2.warpAffine(warp_map_full[:,:,1:], cv2_trans_inv, (int(width), int(height)),borderValue=(255))
# #        cv2.imwrite("warp_map_back_ori.jpg", warp_map_back)
#         map_kernel = int(map_kernel/(height_ori/height))
#         map_kernel = map_kernel if map_kernel%2==1 else map_kernel + 1
#         if map_kernel<1:
#             map_kernel = 1
        
#         #test
#         cv_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (shrink_map_kernel, shrink_map_kernel))
#         warp_map_back = cv2.dilate(warp_map_back, cv_kernel)
#        cv2.imwrite("warp_map_back.jpg", warp_map_back)
        
        
    elif mask_type==2:
      
        blend_map = face_mask.copy()
        blend_map = 255-blend_map
        
        
        warp_map_full = np.concatenate((face_mask, border_map, blend_map), axis=2)
        warp_map_full = cv2.warpAffine(warp_map_full, cv2_trans, (target_size, target_size), borderValue=(0,255, 255))
#         warp_map_back_full = cv2.warpAffine(warp_map_full[:,:,1:], cv2_trans_inv, (int(width), int(height)),borderValue=(255,255))

#          #图片边框mask缩小 chunji v
#         cv_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (shrink_map_kernel, shrink_map_kernel))
     
#         #jpmanga
# #        border_map_back = cv2.dilate(warp_map_back_full[:,:,:1], cv_kernel)
# #        warp_map_back = cv2.erode(warp_map_back_full[:,:,1:2], cv_kernel)
        
#         #test
#         border_map_back = cv2.dilate(warp_map_back_full[:,:,:1], cv_kernel)
#         cv_kernel2 = cv2.getStructuringElement(cv2.MORPH_RECT, (shrink_map_kernel_2, shrink_map_kernel_2))
#         warp_map_back = cv2.erode(warp_map_back_full[:,:,1:2], cv_kernel2)
#        cv2.imwrite("warp_map_back.jpg", warp_map_back)
      
        
#        map_kernel = int(map_kernel/resize_factor)
#        map_kernel = map_kernel+1 if map_kernel%2==0 else map_kernel
        
        #图片边框mask缩小 xinghong v 
#        border_map_back = cv2.GaussianBlur(warp_map_back_full[:,:,:1], (map_kernel,map_kernel),0)
#        border_map_back[border_map_back>0] = 255
#        
#        warp_map_back = cv2.GaussianBlur(warp_map_back_full[:,:,1:2], (map_kernel,map_kernel),0)
#        warp_map_back = warp_map_back.astype(np.float32)
#        warp_map_back = warp_map_back/np.max(warp_map_back)*255
##    
#        #扩大脸部范围，防止将背景图的下巴包含进结果图
    #     warp_map_back[warp_map_back<255] = 0
    #     #最终mask是图片边框和脸部mask的并集
    #     warp_map_back[border_map_back>0] = 255
    #     warp_map_back = warp_map_back.astype(np.uint8)
    #     map_kernel = shrink_map_kernel
        
    # warp_map_back = np.tile(warp_map_back[:,:,None],(1,1,3))
  
   

    if gamma_cor == 1:
        gray_img = cv2.cvtColor(warp_img, cv2.COLOR_BGR2GRAY)

        # detect face region
        gaussian_kernel = int(15 * change_ratio)
        gaussian_kernel_eye = int(11 * change_ratio)
        if gaussian_kernel % 2 == 0:
            gaussian_kernel = gaussian_kernel + 1
        if gaussian_kernel_eye % 2 == 0:
            gaussian_kernel_eye = gaussian_kernel_eye + 1
          
        face_mask = warp_map_full[:,:,0]

        face_mask_smooth = cv2.GaussianBlur(face_mask, (gaussian_kernel, gaussian_kernel), 0)
        face_mask_smooth = face_mask_smooth.astype(np.float32) / 255

        # exclude mouth and eye
        mouth_mask = get_mouth_mask(warp_img, lmks_final)
        mouth_mask = (mouth_mask * 255).astype(np.uint8)
        mouth_mask_smooth = cv2.GaussianBlur(mouth_mask, (gaussian_kernel, gaussian_kernel), 0)
        mouth_mask_smooth = mouth_mask_smooth.astype(np.float32) / 255
        eye_mask = get_eye_mask(warp_img, lmks_final)

        eye_mask_smooth = cv2.GaussianBlur(eye_mask * 255, (gaussian_kernel_eye, gaussian_kernel_eye),
                                           0)  # v4_eye15
        eye_mask_smooth[eye_mask_smooth > 0] = 255
        eye_mask_smooth = cv2.GaussianBlur(eye_mask_smooth, (gaussian_kernel, gaussian_kernel), 0)
        eye_mask_smooth = eye_mask_smooth.astype(np.float32) / 255

        face_mask_smooth = face_mask_smooth - mouth_mask_smooth - eye_mask_smooth

        # exclude hair region
        eye_y = lmks_final[52, 1]
        for i in range(52, 64):
            eye_y = np.min((eye_y, int(lmks_final[i, 1])))
        eye_y = int(eye_y)
        gray_img[eye_y:, :] = 255
        
        if hair_thresh_strategy==2:
            eyebrow_mask_l, eyebrow_mask_r = get_eyebrow_mask(warp_img, lmks_final)
            hair_thresh_l = np.mean(gray_img[eyebrow_mask_l>0].astype(np.float32))
            hair_thresh_r = np.mean(gray_img[eyebrow_mask_r>0].astype(np.float32))
            hair_thresh = np.max((hair_thresh_l, hair_thresh_r, hair_thresh_hard))
        else:
            hair_thresh = hair_thresh_hard
       

        face_mask_smooth[gray_img < hair_thresh] = 0
        face_mask_smooth = cv2.GaussianBlur(face_mask_smooth, (gaussian_kernel, gaussian_kernel), 0)
        
        # gamma correction for the whole image
        lighten_image = adjust_gamma(warp_img, gamma=1.5)
        out_img = warp_img.copy()
        # weighted sum
        for i in range(3):
            out_img[:, :, i] = lighten_image[:, :, i] * face_mask_smooth + out_img[:, :, i] * (1 - face_mask_smooth)
        out_img[out_img > 255] = 255
        out_img[out_img < 0] = 0
        warp_img = out_img.astype(np.uint8)
  
#    print(warp_map_back.shape)
 
    cv2_trans_inv = cv2_trans_inv/resize_factor
    return warp_img, cv2_trans, warp_map_back, map_kernel, warp_bg_mask, warp_ori_img


def get_nose_mask(cv_img, lmks):
    mask = np.zeros(cv_img.shape[:-1],np.uint8) 
    parts = []
    parts.append([int(lmks[55,0]), int(lmks[55,1])]) 
    parts.append([int(lmks[82,0]), int(lmks[82,1])]) 
    parts.append([int(lmks[49,0]), int(lmks[49,1])]) 
    parts.append([int(lmks[83,0]), int(lmks[83,1])]) 
    parts.append([int(lmks[58,0]), int(lmks[58,1])]) 
    parts.append([int(lmks[43,0]), int(lmks[43,1])]) 
           
    parts = np.array(parts)
    filler = cv2.convexHull(parts)
    cv2.fillConvexPoly(mask, filler, 1)
    return mask
 
def get_average_color(cv_img, mask):
    ave_color = np.zeros(3)
#    print(cv_img.shape[-1])
    for i in range(cv_img.shape[-1]):
        channel = cv_img[:,:,i]
        ave_color[i] = np.mean(channel[mask>0.5])
    return ave_color

def get_average_lumi(cv_img, mask):
    lumi_img = cv2.cvtColor(cv_img, cv2.COLOR_BGR2GRAY)
    if len(mask>0)==0:
        return 255
    return np.mean(lumi_img[mask>0])


def get_black_class(mean_lumi, spec_class=[255,110,0]):
#    print(mean_lumi)
    for i in range(len(spec_class)-1):
        if mean_lumi<=spec_class[i] and mean_lumi>spec_class[i+1]:
            return 3+i
    return 2+len(spec_class)


def apply_lut(image, lut):
    lut_image  = Image.fromarray(cv2.cvtColor(image.astype(np.uint8), cv2.COLOR_BGR2RGB))
    lut_image = lut_image.filter(lut)
    lut_image = cv2.cvtColor(np.array(lut_image), cv2.COLOR_RGB2BGR)  
    
    return lut_image
    
        
def read_lut_file(path):
    lut = cv2.imread(path)
    lut = cv2.cvtColor(lut, cv2.COLOR_BGR2RGB).astype(np.float32)
 
    lut_table = np.zeros((64*64*64,3), np.uint8)
    for j in range(8):
        for i in range(8):
            cur_block = lut[64*j:64*(j+1), 64*i:64*(i+1),:]
            cur_block = cur_block.reshape(64*64,3)
            lut_table[(j*8+i)*64*64:(j*8+i+1)*64*64,:] = cur_block

    lut_table = lut_table / 255.0
    return lut_table


def get_new_texture(texture, out_width, out_height):
    out_texture = np.zeros((out_height, out_width))
    dup_height = math.ceil(out_height/texture.shape[0])
    dup_width = math.ceil(out_width/texture.shape[1])
    big_texture = np.zeros((dup_height*texture.shape[0],
                            dup_width*texture.shape[1]))
    for i in range(dup_height):
        for j in range(dup_width):
            big_texture[i*texture.shape[0]:(i+1)*texture.shape[0], j*texture.shape[1]:(j+1)*texture.shape[1]] = texture[:,:,0]
    
#    print(big_texture.shape)
    out_texture = big_texture[:out_height, :out_width]
   
#    print(out_texture.shape)
    return out_texture
    
def soft_light(img_1, img_2):
    mask = img_1 < 0.5
    T1 = (2 * img_1 -1)*(img_2 - img_2 * img_2) + img_2
    T2 = (2 * img_1 -1)*(np.sqrt(img_2) - img_2) + img_2
    img = T1 * mask + T2 * (1-mask)
    return img
  

def particle_qy(image_ori, texture, ratio=0.35):
    image = image_ori.copy()
    height, width, _ = image.shape
    
  
#    image_normalize = 
#    texture = texture.astype(np.float32)/255.0
    ori_gray = cv2.cvtColor(image.astype(np.uint8), cv2.COLOR_BGR2GRAY).astype(np.float32)/255.0*2-1
    image = image.astype(np.float32)/255.0
    abs_ori_gray = np.abs(ori_gray)
    abs_ori_gray2 = abs_ori_gray * abs_ori_gray
    abs_ori_gray3 = abs_ori_gray2 * abs_ori_gray
    grain_gray = get_new_texture(texture, width, height)
    
    grain_gray = grain_gray * 2.0 - 1.0
    strength = 0.49019608
    mask = np.zeros(ori_gray.shape)
    
    mask[ori_gray>0] = (abs_ori_gray3[ori_gray>0] * 0.5 
                        + abs_ori_gray2[ori_gray>0] * 0.5) * (strength - 0.03921569)
    
    mask[ori_gray<=0] = (abs_ori_gray2[ori_gray<=0] * 0.4 + abs_ori_gray[ori_gray<=0] * 0.6) * strength
    
    
   
    new_color = image + np.tile(grain_gray[:,:,None], (1,1,3)) * (strength - np.tile(mask[:,:,None], (1,1,3)))
    new_color[new_color>=1] = 1
    new_color[new_color<0] = 0
    new_color = image*(1-ratio)+new_color*(ratio)
    new_color[new_color>=1] = 1
    new_color[new_color<0] = 0
    new_color = (new_color*255).astype(np.uint8)
    
    return new_color



def post_proc(image, lut, texture, app_id=0):

#    apply texture  
    if app_id==0:
        lut_image  = Image.fromarray(cv2.cvtColor(image.astype(np.uint8), cv2.COLOR_BGR2RGB))
        lut_image = lut_image.filter(ImageFilter.SHARPEN)
        lut_image = cv2.cvtColor(np.array(lut_image), cv2.COLOR_RGB2BGR).astype(np.float32)  
        lut_image = particle_qy(lut_image,texture)
        lut_image_pil = Image.fromarray(cv2.cvtColor(lut_image.astype(np.uint8), cv2.COLOR_BGR2RGB))
        lut_image = lut_image_pil.filter(lut)
        lut_image = cv2.cvtColor(np.array(lut_image), cv2.COLOR_RGB2BGR)
        print("in new particle_qy")
    elif app_id==1:
       lut_image  = Image.fromarray(cv2.cvtColor(image.astype(np.uint8), cv2.COLOR_BGR2RGB))
       lut_image = lut_image.filter(lut)
       lut_image = cv2.cvtColor(np.array(lut_image), cv2.COLOR_RGB2BGR).astype(np.float32)/255.0
       texture_cur = cv2.resize(texture[:,:,:3], (lut_image.shape[1], lut_image.shape[0]))
       lut_image = soft_light(texture_cur, lut_image)
       lut_image[lut_image>1.0] = 1.0
       lut_image[lut_image<0] = 0.0
       lut_image = lut_image*255
    else:
        lut_image  = Image.fromarray(cv2.cvtColor(image.astype(np.uint8), cv2.COLOR_BGR2RGB))
        lut_image = lut_image.filter(lut)
        lut_image = cv2.cvtColor(np.array(lut_image), cv2.COLOR_RGB2BGR).astype(np.float32)  
        lut_image = lut_image/255.0
       
    #    time_st = time.time()
        if texture.shape[0]<image.shape[0] or texture.shape[1]<image.shape[1]:   
            texture = cv2.resize(texture, (image.shape[1], image.shape[0]))
        else:
            texture = texture[:image.shape[0], :image.shape[1], :]
    
        lut_image_ori = lut_image.copy()
    
        lut_image[lut_image_ori<0.5] = lut_image[lut_image_ori<0.5]*2.0*texture[:,:,:3][lut_image_ori<0.5]
        lut_image[lut_image_ori>=0.5] = 1.0-(1.0-lut_image[lut_image_ori>=0.5])*2.0*(1.0-texture[:,:,:3][lut_image_ori>=0.5])
        
        lut_image[lut_image>1.0] = 1.0
        lut_image[lut_image<0] = 0.0
    
        lut_image = lut_image*255

    return lut_image
    


out_dir = './'
gamma_cor = 1 #脸部gamma矫正
reflect_pad = 0 #对黑边的处理


resize=1 #裁剪前是否降采样
resize_bound = 384 #降采样最短边
target_size = 384 #输出分辨率
# out_face = 1 #使用外轮廓点进行gamma
check_black = 0 #是否做黑边检测
fix_kernel = 2 #是否fix kernel size of map

hair_thresh_hard = 70
hair_thresh_strategy = 2 #1:固定threshold  2:根据眉毛颜色确定threshold
change_ratio = target_size/256
border_width = 3

mask_type = 1 #1:拼回整个脸部裁剪图 2:拼回脸部

align_methods = [0, 1,2] #align_method， 0:original, 1: chunji align, 2:stylegan align
align_paras = [[0.5,0.51,4.5,384,384], [0.5,0.51,6,384,384], [91,91,138,108,320,384], [123,123,138,108,384,384],[90,90,60,120,384,384], [90,90,80,100,384,384]]


class LandDetector():
    def __init__(self, hair_thresh=70, hair_thresh_strategy=2, pm_model_type=2, bg_color=0):
        self.hair_thresh = hair_thresh
        self.hair_thresh_strategy = hair_thresh_strategy
        self.bg_color =  bg_color
        
        model_path = './model/tt_face_v7.0.model'.encode()
        extra_path = './model/tt_face_extra_v10.0.model'.encode()
        land_lib_path = './align_v4_linux/lib/land.so'
        fo_model_path = './model/tt_face_new_landmark_v1.0.model'.encode()
        fo_lib_path = './align_v4_linux/lib/facenewlandmark.so'
        attr_model_path = './model/tt_face_attribute_v5.0.model'.encode()
        attr_extra_path = './model/tt_face_attribute_extra_v2.0.model'.encode()
        attr_lib_path = './align_v4_linux/lib/faceattribute.so'
        pm_model_path = './model/tt_matting_v12.0.model'.encode()
        pm_model_large_path = './model/tt_matting_large_v2.0.model'.encode()
        pm_model_jianying_path = './model/tt_matting_video_v1.0.model'.encode()
        pm_lib_path = './align_v4_linux/lib/portraitmatting.so'

        pet_lib_path = './align_v4_linux/lib/libFaceKeyPointDetect.so'
        pet_model_path = './model/tt_petface_v3.0.model'.encode()



        lut_jianying_path = './luts/lut.png'
        lut_xingtu_paths = ['./luts/xingtu_lut1.png','./luts/xingtu_lut2.png','./luts/xingtu_lut3.png']
        lut_pet_path = './luts/lut_pet.png'
        
        jianying_bg_lut_path = './luts/jianying_bg_lut.png'
        jianying_texture_path = './luts/texture.png'
        xingtu_texture_paths = ['./luts/xingtu_texture1.png','./luts/xingtu_texture2.png','./luts/xingtu_texture3.png']
        map_paths = ['./luts/日漫fusion_mask.png', './luts/map2_jp_large.png','./luts/map2_jp_small.png','./luts/map_pet.png']
        


        if gamma_cor == 1:
            self.face_alignment_outline = fao.FaceAlignment(platform='Linux', model_path=model_path, extra_path=extra_path,
                                                            land_lib_path=land_lib_path, pad=[0.4, 0.4, 0.8, 1.2], use_detection=False)
            self.face_outlines = fao.FaceOutlines(platform='Linux', model_path=fo_model_path, faceoutlines_lib_path=fo_lib_path,smooth_mode=0)

        self.face_detection_fast = fao.FaceDetection(platform='Linux', model_path=model_path, extra_path=extra_path, land_lib_path=land_lib_path,
                                        slow_mode=False, full_mode=True, video_mode=False, detect_interval=1)
        self.face_attr = faceAttr.FaceAttribute(attr_model_path,  attr_extra_path, attr_lib_path, slow_mode=True)   
        
        min_len = 128
        if pm_model_type==1:
            pm_model_path = pm_model_large_path
            min_len = 256
        elif pm_model_type==2:
            pm_model_path = pm_model_jianying_path
            min_len = 288
        print((pm_model_path, pm_lib_path, pm_model_type, min_len))
        self.portrait_matt = pm.PortraitMatting(pm_model_path, pm_lib_path, pm_model_type, min_len=min_len)
        # ret, res_img = self.portrait_matt.detect(cv2.imread("./images/inp.jpg"))
        # cv2.imwrite("bg_mask_init.jpg", res_img)

        self.pet_detector = fap.PetDetector(pet_lib_path, pet_model_path, 5)

        self.jianying_lut = ImageFilter.Color3DLUT(64, read_lut_file(lut_jianying_path), target_mode=None, _copy_table=False)
        self.jianying_bg_lut = ImageFilter.Color3DLUT(64, read_lut_file(jianying_bg_lut_path), target_mode=None, _copy_table=False)
        self.jianying_pet_lut = ImageFilter.Color3DLUT(64, read_lut_file(lut_pet_path), target_mode=None, _copy_table=False)
        self.jianying_texture = cv2.imread(jianying_texture_path, -1).astype(np.float32)/255.0
        self.xingtu_luts = []
        self.xingtu_textures = []
        self.maps = []
        for i in range(len(lut_xingtu_paths)):
            self.xingtu_luts.append(ImageFilter.Color3DLUT(64, read_lut_file(lut_xingtu_paths[i]), target_mode=None, _copy_table=False))
            self.xingtu_textures.append(cv2.imread(xingtu_texture_paths[i], -1).astype(np.float32)/255.0)
        for i in range(len(map_paths)):
            self.maps.append(cv2.imread(map_paths[i]))
            print(self.maps[i].shape)

    def detect(self, cv_img, effect_id):
        count, face_info = self.face_detection_fast.detect106_full(cv_img, 'BGR')
        if effect_id!=3: #not tc
            return  count, face_info, 0, None
        self.pet_detector.detect(cv_img)
        det_res = self.pet_detector.get_result()
        return count, face_info, len(det_res), det_res

    def img_preprocess(self, cv_img, img_fmt = 'BGR', type = 1):
        if type == 1: 
            # 从原图中裁剪出人脸方形区域, 作为新原始图像, 主要是用于2b的全图处理
            src_h, src_w, src_c = cv_img.shape
            count, face_info = self.face_detection_fast.detect106_full(cv_img, img_fmt)
            if count <= 0:
                return cv_img

            rect = face_info[1]
            x_center = (rect[0] + rect[2]) * 0.5
            y_center = (rect[1] + rect[3]) * 0.5
            width = rect[2] - rect[0]
            height = rect[3] - rect[1]

            half_edge = max(height, width)
            edge = half_edge * 2

            x0 = int(x_center - half_edge)
            x1 = x0 + edge
            y0 = int(y_center - half_edge)
            y1 = y0 + edge

            if x0 < 0:
                x0 = 0
                x1 = min(x0 + edge, src_w)
            elif x1 > src_w:
                x1 = src_w
                x0 = max(x1 - edge, 0)

            if y0 < 0:
                y0 = 0
                y1 = min(y0 + edge, src_h)
            elif y1 > src_h:
                y1 = src_h
                y0 = max(y1 - edge, 0)
            
            dst_img = cv_img[y0:y1, x0:x1]
            return dst_img
        else:
            return cv_img   
    
    def clip(self, cv_img, face_info_full, method_para, effect_id, \
                    max_face=5, app_id=0, big_model=1, bg_mask = None):
        #clip_key的两个数分别对应align_methods， align_para, whether to crop bg
        human_count, face_info, pet_count, pet_face_info = face_info_full
        clip_key = method_para[1-big_model]
        align_method = int(clip_key[0])
        align_para = align_paras[int(clip_key[1])]
        crop_bg = int(clip_key[2])
        map_index = int(clip_key[3])
        fill_bg_after = int(clip_key[4])
        print((clip_key, align_method, align_para, crop_bg, map_index))

        height, width, channels = cv_img.shape
        width_ori = width
        height_ori = height
        bg_img = cv_img.copy()
        # cv2.imwrite("bg_img.jpg", bg_img)
        if crop_bg==1:
            try:
                if bg_mask is not None:
                    res_img = np.tile(bg_mask[:,:,None], (1,1,3))
                    res_img = res_img.astype(np.float32)/255.0
                    cv_img = cv_img*res_img+self.bg_color*(1-res_img)
                    cv_img[cv_img<0] = 0
                    cv_img[cv_img>255] = 255
                    cv_img = cv_img.astype(np.uint8)
                else:
                    ret, res_img = self.portrait_matt.detect(cv_img)
                    cv_img[res_img==0] = self.bg_color
                    bg_mask = res_img
                    bg_mask = np.tile(bg_mask[:,:,None], (1,1,3))
                    # print(bg_mask.shape)
                    # cv2.imwrite("bg_mask.jpg", bg_mask)
            except:
                bg_mask = np.zeros(cv_img.shape[:2])
                print("no bg detected")
        elif fill_bg_after==1:
            if bg_mask is None:
                ret, bg_mask = self.portrait_matt.detect(cv_img)
                bg_mask = np.tile(bg_mask[:,:,None], (1,1,3))
        else:
            bg_mask = None
#        bg_img = cv2.resize(cv_img, (target_size, target_size));
        


        resize_factor = 1
        # 降采样
#         if resize == 1:
# #            pil_img = cv2_to_pil(cv_img)
#             if height > resize_bound and height <= width:
#                 resize_factor = resize_bound / height
#                 width = int(resize_bound / height * width)
#                 height = resize_bound
#                 cv_img = cv2.resize(cv_img, (width, height))
# #                pil_img = pil_img.resize((width, height), Image.ANTIALIAS)
# #                cv_img = pil_to_cv2(pil_img)
#             elif width > resize_bound and width < height:
#                 resize_factor = resize_bound / width
#                 height = int(resize_bound / width * height)
#                 width = resize_bound
#                 cv_img = cv2.resize(cv_img, (width, height))
# #                pil_img = pil_img.resize((width, height), Image.ANTIALIAS)
# #                cv_img = pil_to_cv2(pil_img)
            
        # 轮廓点检测
        # count, face_info = self.face_detection_fast.detect106_full(cv_img, 'BGR')
        # cv2.imwrite("cv_img.jpg", cv_img)
        if human_count <= 0:
            if effect_id<3:
                print("No face detected!")
                return 0, None, bg_img, None, None, None, bg_mask, None, None, None, 0
            elif effect_id==3:
                if pet_count<=0:
                    print("No face detected!")
                    return 0, None, bg_img, None, None, None, bg_mask, None, None, None, 0

        if human_count!=0:
            ret, gender = self.face_attr.detect_gender_full(cv_img, self.face_detection_fast.point106, human_count)
            lms = face_info[0]
            lmks = lms.reshape((-1, 2))
            if gamma_cor == 1 :
                fo_lms = self.face_outlines.detect_full(cv_img, face_info[0], human_count, face_info[7], False)
                fo_lmks = fo_lms.reshape((-1, 2))

        count = np.min((human_count+pet_count,max_face))
        if effect_id==3:
            type1_count = np.min((human_count, max_face))
        else:
            type1_count = count
        print(("count inner", count))

        if fix_kernel==1:
#            map_kernel_fix = int(75/resize_factor)
#            map_kernel_fix = map_kernel_fix if map_kernel_fix%2==1 else map_kernel_fix + 1
            map_kernel_ratio = 5
        else:
#            if app_id==0:
#                map_kernel_ratio = 1 #jianying original
#            else:
            map_kernel_ratio = 0.6 #test, xingtu, tiekuang
#            map_kernel_ratio = 0.75 #jpmanga origin
            print(("map_kernel_ratio", map_kernel_ratio))
            
            
        # alignment
        map_kernel = []
        warp_img  = []
        cv2_trans_inv = []
        warp_map_back = []
        face_type = []
        face_mask = []
        ori_face = []

        if align_method==0:
            out_size = (int(align_para[3]), int(align_para[4]))
            face_map = cv2.resize(self.maps[map_index], out_size)
        else:
            out_size = (int(align_para[4]), int(align_para[5]))
            face_map = cv2.resize(self.maps[map_index], out_size)

        std_lms_pos = (int(align_para[0]), int(align_para[1]), int(align_para[2]), int(align_para[3]))
        std_landmarks = std_lms_global.copy()
        std_landmarks = pad_std_lms(std_landmarks, [0,0,0,0])

        for i in range(min(human_count, max_face)):
            cur_lmks = lmks[106*i:106*(i+1)].copy()
            if effect_id == 3:
                face_type.append(0)
            if align_method==0:
                if gamma_cor==0:
                    eye_ratio = float(align_para[2])
                    cur_warp_img, cur_cv2_trans_inv, cur_warp_map_back, cur_map_kernel, cur_face_mask, cur_ori_face = align_face(cv_img, cur_lmks, resize_factor, (width_ori, height_ori), \
                            map_kernel_ratio, app_id=app_id, eye_ratio=eye_ratio, crop_bg=crop_bg+fill_bg_after, bg_mask=bg_mask, ori_img=bg_img, face_map=face_map)
                else: 
                    eye_ratio = align_para[2]
                    cur_fo_lmks = fo_lmks[44*i:44*(i+1)].copy()
                    cur_warp_img, cur_cv2_trans_inv, cur_warp_map_back, cur_map_kernel, cur_face_mask, cur_ori_face = align_face(cv_img, cur_lmks, resize_factor,\
                                (width_ori, height_ori), map_kernel_ratio, app_id=app_id, fo_lmks=cur_fo_lmks, eye_ratio=eye_ratio, crop_bg=crop_bg+fill_bg_after, bg_mask=bg_mask, ori_img=bg_img, face_map=face_map) 
            
            else:
                cur_warp_img, cur_cv2_trans_inv, cur_warp_map_back, cur_map_kernel, cur_face_mask, cur_ori_face = align_face2(cv_img, cur_lmks, 
                        resize_factor, (width_ori, height_ori), map_kernel_ratio, std_landmarks.copy(), 
                        out_size =out_size, target_size=out_size, std_lms_pos=std_lms_pos,
                        flags=cv2.INTER_CUBIC, is_pet=False, fo_lmks=None,  pad=[0,0,0,0], face_map=face_map,crop_bg=crop_bg+fill_bg_after, bg_mask=bg_mask, ori_img=bg_img)
            # cv2.imwrite("face_mask_"+str(i)+'.jpg', cur_face_mask)
            warp_img.append(cur_warp_img)
            cv2_trans_inv.append(cur_cv2_trans_inv)
            warp_map_back.append(cur_warp_map_back)
            map_kernel.append(int(cur_map_kernel)) 
            face_mask.append(cur_face_mask)
            ori_face.append(cur_ori_face)
            
            # if i==0:
            #     warp_img = cur_warp_img
            #     cv2_trans_inv = cur_cv2_trans_inv
            #     warp_map_back = cur_warp_map_back
            #     face_mask = cur_face_mask
            #     # print(("face_mask shape", face_mask.shape))
            #     ori_face = cur_ori_face
            #     map_kernel = np.append(map_kernel,int(cur_map_kernel))
            # else:
           

                # warp_img = np.concatenate((warp_img, cur_warp_img), axis = 2)
                # cv2_trans_inv = np.concatenate((cv2_trans_inv, cur_cv2_trans_inv), axis = 1)
                # warp_map_back = np.concatenate((warp_map_back, cur_warp_map_back), axis = 2)
                # face_mask = np.concatenate((face_mask, cur_face_mask), axis = 2)
                # ori_face = np.concatenate((ori_face, cur_ori_face), axis = 2)
                # map_kernel = np.append(map_kernel, int(cur_map_kernel))   

        if effect_id==3:
            clip_key = method_para[3-big_model]
            align_method = int(clip_key[0])
            align_para = align_paras[int(clip_key[1])]
            crop_bg = int(clip_key[2])
            map_index = int(clip_key[3])

            out_size = (int(align_para[3]), int(align_para[4]))
            tar_size = (384, 384)
            out_size = (384, 384)
            pad_cat=[90, 90, 60, 120]
            pad_dog=[90, 90, 80, 100]
            face_map = cv2.resize(self.maps[3], tar_size)
            for i in range(human_count, min(pet_count+human_count, max_face)):
                face_type.append(1)
                if pet_face_info[i-human_count]['type'] == 'cat':
                    subset = subset_cat
                    std_points = std_points_cat
                    pad_cur = pad_cat.copy()
                elif pet_face_info[i-human_count]['type'] == 'dog':
                    subset = subset_dog
                    std_points = std_points_dog
                    pad_cur = pad_dog.copy()
                res = pet_face_info[i-human_count]['points']
                res = np.array(res).reshape((-1, 2))

                cur_lmks = res[subset]
                
                cur_warp_img, cur_cv2_trans_inv, cur_warp_map_back, cur_map_kernel, cur_face_mask, cur_ori_face = align_face2(cv_img, cur_lmks, 
                resize_factor, (width_ori, height_ori), map_kernel_ratio, std_points.copy(),
                out_size = out_size, target_size=tar_size, std_lms_pos=pad_cur,
                flags=cv2.INTER_CUBIC, is_pet=True, fo_lmks=None,  pad=[0,0,0,0], face_map=face_map,crop_bg=crop_bg, bg_mask=bg_mask, ori_img=bg_img)
                
                warp_img.append(cur_warp_img)
                cv2_trans_inv.append(cur_cv2_trans_inv)
                warp_map_back.append(cur_warp_map_back)
                map_kernel.append(int(cur_map_kernel)) 
                face_mask.append(cur_face_mask)
                ori_face.append(cur_ori_face)
        else:
            face_type = gender

        return count, warp_img, bg_img, cv2_trans_inv, warp_map_back, map_kernel, bg_mask, face_type, face_mask, ori_face, type1_count




if __name__ == "__main__":
#    if len(sys.argv)>2:
#        print("No input image path")
#        #sys.exit(0)
    if len(sys.argv)>1:
        land = LandDetector(hair_thresh=hair_thresh_hard, hair_thresh_strategy=hair_thresh_strategy)
        cv_img = cv2.imread(sys.argv[1])
        start = time.time()
        _, warp_img, ori_face, border_map = land.clip(cv_img)
        print(time.time()-start)
#        for x,y in times.items():
#            print(x,str(y))
        cv2.imwrite(os.path.join(out_dir,'test_out.jpg'), warp_img)
        cv2.imwrite(os.path.join(out_dir,'test_out_large.jpg'), ori_face)
        cv2.imwrite(os.path.join(out_dir,'map.jpg'), border_map)
    else:
        try:
            
            cv_img = cv2.imread("test3.jpg")#cv2.imread(sys.argv[1])
            #pdb.set_trace()
            land = LandDetector(hair_thresh=hair_thresh_hard, hair_thresh_strategy=hair_thresh_strategy)
            start = time.time()
            _, warp_img, ori_face, border_map = land.clip(cv_img)
            print(time.time()-start)
            #写入图片
            print("saving output image to "+os.path.join(out_dir,'test_out.jpg'))
            cv2.imwrite(os.path.join(out_dir,'test_out.jpg'), warp_img)
            cv2.imwrite(os.path.join(out_dir,'test_out_large.jpg'), ori_face)
            cv2.imwrite(os.path.join(out_dir,'map.jpg'), border_map)
        except Exception as e:
            print(e)
           

