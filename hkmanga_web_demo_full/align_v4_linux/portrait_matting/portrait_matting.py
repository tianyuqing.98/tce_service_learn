#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  6 15:57:44 2020

@author: huxinghong
"""

import ctypes
import numpy as np
import cv2
import os
import sys
import time

IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]


def is_image_file(filename):
    return any(filename.endswith(extension) for extension in IMG_EXTENSIONS)




class PortraitMatting:
    def __init__(self, model_path, lib_path, model_type=0, contour_type=1, min_len=128):
        self.min_len  = min_len
        if model_type==1:
           model_type=2
        elif model_type==2:
           model_type=4
        else:
           model_type=0
        self.pm = ctypes.cdll.LoadLibrary(lib_path)
        print(("contour_type", contour_type))
        ret = self.pm.init_PM(model_path, model_type, contour_type, self.min_len)
        if ret==0:
            print("MP_InitModel success!")
            


    def detect(self, image, use_rgba=False):
        
        h, w, c = image.shape
        if use_rgba:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGBA)

        if w>=h:
            mat_height = self.min_len;
            mat_width = 16*(int(w*float(self.min_len)/h/16.0+0.5));
        else:
            mat_width = self.min_len;
            mat_height = 16*(int(h*float(self.min_len)/w/16.0+0.5));
    
        self.alpha_mat_c = (ctypes.c_ubyte * w * h)()
        ret = self.pm.do_PM(image.tostring(), h, w, w*c , use_rgba, self.alpha_mat_c)
        
        
        if ret==-1:
            return 0, None
        
        alpha_mat = np.ctypeslib.as_array(self.alpha_mat_c)
        #alpha_mat = cv2.resize(alpha_mat, (w,h))
        

        return 1, alpha_mat

  

