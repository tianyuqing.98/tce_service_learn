import ctypes
import numpy as np
import cv2

   
lms_id = np.array(
        [33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,
        56,57,58,59,60,61,62,63,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,
        100,101,102,103], dtype=np.int32)

std_lms = np.array([
            [ 0.000213256,  0.106454  ], #17 33
            [ 0.0752622,    0.038915  ], #18 34
            [ 0.18113,      0.0187482 ], #19 35
            [ 0.29077,      0.0344891 ], #20 36
            [ 0.393397,     0.0773906 ], #21 37
            [ 0.586856,     0.0773906 ], #22 38
            [ 0.689483,     0.0344891 ], #23 39
            [ 0.799124,     0.0187482 ], #24 40
            [ 0.904991,     0.038915  ], #25 41
            [ 0.98004,      0.106454  ], #26 42
            [ 0.490127,     0.203352  ], #27 43
            [ 0.490127,     0.307009  ], #28 44
            [ 0.490127,     0.409805  ], #29 45
            [ 0.490127,     0.515625  ], #30 46
            [ 0.36688,      0.587326  ], #31 47
            [ 0.426036,     0.609345  ], #32 48
            [ 0.490127,     0.628106  ], #33 49
            [ 0.554217,     0.609345  ], #34 50
            [ 0.613373,     0.587326  ], #35 51
            [ 0.121737,     0.216423  ], #36 52
            [ 0.187122,     0.178758  ], #37 53
            [ 0.265825,     0.179852  ], #38 54
            [ 0.334606,     0.231733  ], #39 55
            [ 0.260918,     0.245099  ], #40 56
            [ 0.182743,     0.244077  ], #41 57
            [ 0.645647,     0.231733  ], #42 58
            [ 0.714428,     0.179852  ], #43 59
            [ 0.793132,     0.178758  ], #44 60
            [ 0.858516,     0.216423  ], #45 61
            [ 0.79751,      0.244077  ], #46 62
            [ 0.719335,     0.245099  ], #47 63
            [ 0.254149,     0.780233  ], #48 84
            [ 0.340985,     0.745405  ], #49 85
            [ 0.428858,     0.727388  ], #50 86
            [ 0.490127,     0.742578  ], #51 87
            [ 0.551395,     0.727388  ], #52 88
            [ 0.639268,     0.745405  ], #53 89
            [ 0.726104,     0.780233  ], #54 90
            [ 0.642159,     0.864805  ], #55 91
            [ 0.556721,     0.902192  ], #56 92
            [ 0.490127,     0.909281  ], #57 93
            [ 0.423532,     0.902192  ], #58 94
            [ 0.338094,     0.864805  ], #59 95
            [ 0.290379,     0.784792  ], #60 96
            [ 0.428096,     0.778746  ], #61 97
            [ 0.490127,     0.785343  ], #62 98
            [ 0.552157,     0.778746  ], #63 99
            [ 0.689874,     0.784792  ], #64 100
            [ 0.553364,     0.824182  ], #65 101
            [ 0.490127,     0.831803  ], #66 102
            [ 0.42689 ,     0.824182  ]  #67 103
            ], dtype=np.float32)



class FaceOutlines:
    # no smooth: smooth_mode=0
    def __init__(self, platform, model_path, faceoutlines_lib_path, smooth_mode=0):
        self.platform = platform
        # if platform == 'Mac':
        #     self.detector = ctypes.cdll.LoadLibrary(faceoutlines_lib_path)
        #     self.detector.init_FNL(model_path, smooth_mode)
        # elif platform == 'Linux':
        #     self.detector = ctypes.cdll.LoadLibrary(faceoutlines_lib_path)
        #     self.detector.init_FNL(model_path, smooth_mode)
        self.detector = ctypes.cdll.LoadLibrary(faceoutlines_lib_path)
        self.detector.init_FNL(model_path, smooth_mode)
        self.points = (ctypes.c_float * 440)()
        self.landmarks106 = (ctypes.c_float * 1060)()
        self.ids = (ctypes.c_int32 * 5)()


    def detect(self, image, landmarks, use_rgba):
        for i in range(106 * 2) : self.landmarks106[i] = landmarks[i]
        image_detection = cv2.cvtColor(image, cv2.COLOR_BGR2BGRA)
        h, w, c = image_detection.shape
        # if self.platform == 'Mac':
        #     self.detector.do_predict_FNL(image_detection.tostring(), h, w, w * c, self.landmarks106, use_rgba, self.points)
        # if self.platform == 'Linux':
        #     self.detector.do_predict_FNL(image_detection.tostring(), h, w, w * c, self.landmarks106, use_rgba, self.points)
        self.detector.do_predict_FNL(image_detection.tostring(), h, w, w * c, self.landmarks106, use_rgba, self.points)

        p = np.zeros(44 * 2, dtype=float)
        for i in range(44 * 2) : p[i] = self.points[i]
        return p

    def detect_full(self, image, landmarks, num_face, ids, use_rgba):
        tmp_landmarks = landmarks.reshape((-1,))
        for i in range(212 * num_face):
            self.landmarks106[i] = tmp_landmarks[i]
        for i in range(num_face):
            self.ids[i] = ids[i]
        image_detection = cv2.cvtColor(image, cv2.COLOR_BGR2BGRA)
        h, w, c = image_detection.shape
        self.detector.do_predict_FNL_FULL(image_detection.tostring(), h, w, w * c, num_face, self.landmarks106, self.ids, use_rgba, self.points)

        p = np.zeros(44 * 2 * num_face, dtype=float)
        for i in range(44 * 2 * num_face): p[i] = self.points[i]
        return p
        

class FaceDetection:
    # Image format for face detection: 
    # - Linux: RGB
    # - Mac  : BGR
    def __init__(self, platform, model_path, extra_path, land_lib_path, slow_mode=False, 
                 full_mode=False, video_mode=False, detect_interval=2):
        self.platform = platform
        self.full_mode = full_mode
        # if platform=='Linux':
        #     self.detector = ctypes.cdll.LoadLibrary(land_lib_path)
        # elif platform == 'Mac':
        self.detector = ctypes.cdll.LoadLibrary(land_lib_path)
        if full_mode:
            self.detector.init_FS_FULL(model_path, extra_path, video_mode, slow_mode, detect_interval)
            self.rects = (ctypes.c_int * 20)()
            self.scores = (ctypes.c_float * 5)()
            self.point106 = (ctypes.c_float * 1060)()
            self.yaws = (ctypes.c_float * 5)()
            self.pitches = (ctypes.c_float * 5)()
            self.rolls = (ctypes.c_float * 5)()
            self.eye_dists = (ctypes.c_float * 5)()
            self.ids = (ctypes.c_int32 * 5)()
            self.actions = (ctypes.c_uint32 * 5)()
            self.tracking_cnts = (ctypes.c_uint32 * 5)()
        else:
            self.detector.init_FS(model_path, extra_path, slow_mode)
            self.point5 = (ctypes.c_float * 10)()
            self.point106 = (ctypes.c_float * 212)()

    def detect106(self, image, image_fmt):
        try:
            shape = image.shape
            if self.platform == 'Linux':
                if image_fmt == 'BGR':
                    image_detection = image[:,:,::-1]
                else:
                    image_detection = image.copy()
                count = self.detector.do_predict106(image_detection.tostring(), shape[0], shape[1], shape[1] * shape[2], self.point106)
            elif self.platform == 'Mac':
                if image_fmt == 'RGB':
                    image_detection = image[:,:,::-1]
                else:
                    image_detection = image.copy()
                count = self.detector.do_predict_FS(image_detection.tostring(), shape[0], shape[1], shape[1] * shape[2], self.point106)

            if count == 0:
                return count, None
            
            p = np.zeros(106 * 2, dtype=float)
            for i in range(106 * 2) : p[i] = self.point106[i]
            return count, p
        except:
            print("FaceDetection error")
            return 0, None
            

    def detect106_full(self, image, image_fmt):
        shape = image.shape
        if image_fmt == 'RGB':
            image_detection = image[:,:,::-1]
        else:
            image_detection = image.copy()
       
        count = self.detector.do_predict_FS_FULL(image_detection.tostring(), shape[0], shape[1], shape[1] * shape[2],
                                                    self.rects, self.scores, self.point106, self.yaws, self.pitches, self.rolls,
                                                    self.eye_dists, self.ids, self.actions, self.tracking_cnts)
       
        if count > 0:
            p = np.zeros(106 * 2 * count, dtype=float)
            for i in range(106 * 2 * count): p[i] = self.point106[i]
            rects = np.zeros(4 * count, dtype=int)
            for i in range(4 * count): rects[i] = self.rects[i]
            scores = np.zeros(count, dtype=float)
            for i in range(count): scores[i] = self.scores[i]
            yaws = np.zeros(count, dtype=float)
            for i in range(count): yaws[i] = self.yaws[i]
            pitches = np.zeros(count, dtype=float)
            for i in range(count): pitches[i] = self.pitches[i]
            rolls = np.zeros(count, dtype=float)
            for i in range(count): rolls[i] = self.rolls[i]
            eye_dists = np.zeros(count, dtype=float)
            for i in range(count): eye_dists[i] = self.eye_dists[i]
            ids = np.zeros(count, dtype=int)
            for i in range(count): ids[i] = self.ids[i]
            actions = np.zeros(count, dtype=np.uint32)
            for i in range(count): actions[i] = self.actions[i]
            tracking_cnts = np.zeros(count, dtype=np.uint32)
            for i in range(count): tracking_cnts[i] = self.tracking_cnts[i]
            return count, (p, rects, scores, yaws, pitches, rolls, eye_dists, ids, actions, tracking_cnts)
        else:
            return 0, ()

    def detect5(self, image):
        try:
            shape = image.shape
            if self.platform == 'Linux':
                if image_fmt == 'BGR':
                    image_detection = image[:,:,::-1]
                else:
                    image_detection = image.copy()
                count = self.detector.do_predict(image_detection.tostring(), shape[0], shape[1], shape[1] * shape[2], self.point5)
            elif self.platform == 'Mac':
                if image_fmt == 'RGB':
                    image_detection = image[:,:,::-1]
                else:
                    image_detection = image.copy()
                count = self.detector.do_predict_FS(image_detection.tostring(), shape[0], shape[1], shape[1] * shape[2], self.point106)
                self.point5[0] = self.point106[2 * 74]
                self.point5[1] = self.point106[2 * 74 + 1]
                self.point5[2] = self.point106[2 * 77]
                self.point5[3] = self.point106[2 * 77 + 1]
                self.point5[4] = self.point106[2 * 46]
                self.point5[5] = self.point106[2 * 46 + 1]
                self.point5[6] = self.point106[2 * 84]
                self.point5[7] = self.point106[2 * 84 + 1]
                self.point5[8] = self.point106[2 * 90]
                self.point5[9] = self.point106[2 * 90 + 1]
            if count == 0:
                return count, None

            p = np.zeros(10, dtype=float)
            for i in range(10) : p[i] = self.point5[i]
            return count, p
        except:
            print("FaceDetection error")
            return 0, None

class FaceAlignment():
    # model_path & extra_path must be '***'.encode()
    # pad: left, right, up, bottom padding for the std_lms
    def __init__(self, platform, model_path, extra_path, land_lib_path, pad=[0, 0, 0, 0], slow_mode=False, use_detection=True):
        self.use_detection = use_detection
        if self.use_detection:
            self.face_detection = FaceDetection(platform=platform, model_path=model_path, extra_path=extra_path, land_lib_path=land_lib_path, slow_mode=slow_mode)
        else:
            self.face_detection = None

        self.pad = pad
        self.lms_id = np.array(
            [33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,
            56,57,58,59,60,61,62,63,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,
            100,101,102,103], dtype=np.int32)
        self.std_lms = np.array([
            [ 0.000213256,  0.106454  ], #17 33
            [ 0.0752622,    0.038915  ], #18 34
            [ 0.18113,      0.0187482 ], #19 35
            [ 0.29077,      0.0344891 ], #20 36
            [ 0.393397,     0.0773906 ], #21 37
            [ 0.586856,     0.0773906 ], #22 38
            [ 0.689483,     0.0344891 ], #23 39
            [ 0.799124,     0.0187482 ], #24 40
            [ 0.904991,     0.038915  ], #25 41
            [ 0.98004,      0.106454  ], #26 42
            [ 0.490127,     0.203352  ], #27 43
            [ 0.490127,     0.307009  ], #28 44
            [ 0.490127,     0.409805  ], #29 45
            [ 0.490127,     0.515625  ], #30 46
            [ 0.36688,      0.587326  ], #31 47
            [ 0.426036,     0.609345  ], #32 48
            [ 0.490127,     0.628106  ], #33 49
            [ 0.554217,     0.609345  ], #34 50
            [ 0.613373,     0.587326  ], #35 51
            [ 0.121737,     0.216423  ], #36 52
            [ 0.187122,     0.178758  ], #37 53
            [ 0.265825,     0.179852  ], #38 54
            [ 0.334606,     0.231733  ], #39 55
            [ 0.260918,     0.245099  ], #40 56
            [ 0.182743,     0.244077  ], #41 57
            [ 0.645647,     0.231733  ], #42 58
            [ 0.714428,     0.179852  ], #43 59
            [ 0.793132,     0.178758  ], #44 60
            [ 0.858516,     0.216423  ], #45 61
            [ 0.79751,      0.244077  ], #46 62
            [ 0.719335,     0.245099  ], #47 63
            [ 0.254149,     0.780233  ], #48 84
            [ 0.340985,     0.745405  ], #49 85
            [ 0.428858,     0.727388  ], #50 86
            [ 0.490127,     0.742578  ], #51 87
            [ 0.551395,     0.727388  ], #52 88
            [ 0.639268,     0.745405  ], #53 89
            [ 0.726104,     0.780233  ], #54 90
            [ 0.642159,     0.864805  ], #55 91
            [ 0.556721,     0.902192  ], #56 92
            [ 0.490127,     0.909281  ], #57 93
            [ 0.423532,     0.902192  ], #58 94
            [ 0.338094,     0.864805  ], #59 95
            [ 0.290379,     0.784792  ], #60 96
            [ 0.428096,     0.778746  ], #61 97
            [ 0.490127,     0.785343  ], #62 98
            [ 0.552157,     0.778746  ], #63 99
            [ 0.689874,     0.784792  ], #64 100
            [ 0.553364,     0.824182  ], #65 101
            [ 0.490127,     0.831803  ], #66 102
            [ 0.42689 ,     0.824182  ]  #67 103
            ], dtype=np.float32)
        self.pad_std_lms()
        
    def pad_std_lms(self):
        p = self.pad
        if p[0] > 0 or p[1] > 0 or p[2] > 0 or p[3] > 0:
            self.std_lms[:, 0] = (self.std_lms[:, 0] / (1 + p[0] + p[1])) + p[0] / (1 + p[0] + p[1])
            self.std_lms[:, 1] = (self.std_lms[:, 1] / (1 + p[2] + p[3])) + p[2] / (1 + p[2] + p[3])

    def get_align_M(self, landmarks106, size, method, param):
        if method == 'umeyama':
            lms = landmarks106[self.lms_id]
            trans, trans_inv = get_similarity_transform(lms, self.std_lms * size, reflective=True)
            cv2_trans = cvt_tform_mat_for_cv2(trans)
        elif method == 'xinghong_v4':
            left_eye_x, left_eye_y = landmarks106[74]
            right_eye_x, right_eye_y = landmarks106[77]
            eye_middle_x = (left_eye_x+right_eye_x)/2
            eye_middle_y = (left_eye_y+right_eye_y)/2
            nose = np.copy(landmarks106[46])

            eye_to_eye = np.sqrt((left_eye_x -right_eye_x)**2+(left_eye_y -right_eye_y)**2)

            crop_size = eye_to_eye*param[0]
            target_size = size

            eye_to_nose = np.linalg.norm(np.array([eye_middle_x, eye_middle_y]) - nose)

            cur_left_eye_x = left_eye_x
            cur_left_eye_y = left_eye_y
            cur_right_eye_x = right_eye_x
            cur_right_eye_y = right_eye_y

            tar_eye_y = param[1] - eye_to_nose / (eye_to_eye*param[0])
            tar_left_eye_x = param[2]
            tar_right_eye_x = 1 - param[2]

            fixed_points = np.array([[tar_left_eye_x, tar_right_eye_x, 0.5], [tar_eye_y, tar_eye_y, param[1]]]).transpose() * target_size
            lmks = np.array([[cur_left_eye_x, cur_right_eye_x, nose[0]], [cur_left_eye_y, cur_right_eye_y, nose[1]]]).transpose()

            trans, trans_inv = get_similarity_transform(lmks.copy(), fixed_points, reflective=True)
            cv2_trans = cvt_tform_mat_for_cv2(trans)
        else:
            print('undefined method')
            cv2_trans = None
        return cv2_trans
    
    def expand_eyebrows(self, landmarks, eyebrows_expand_mod=1.0):
        landmarks = np.array(landmarks.copy(), dtype=np.float32)

        #
        ml_pnt = (landmarks[52] + landmarks[0]) / 2
        mr_pnt = (landmarks[61] + landmarks[31]) / 2
        ql_pnt = (landmarks[52] + ml_pnt) / 2
        qr_pnt = (landmarks[61] + mr_pnt) / 2

        # Top of the eye arrays
        bot_l = np.array((ql_pnt, landmarks[52], landmarks[53], landmarks[54], landmarks[55]))
        bot_r = np.array((landmarks[58], landmarks[59], landmarks[60], landmarks[61], qr_pnt))
        top_l = landmarks[33:38]
        top_r = landmarks[38:43]

        landmarks[33:38] = top_l + eyebrows_expand_mod * 0.5 * (top_l - bot_l)
        landmarks[38:43] = top_r + eyebrows_expand_mod * 0.5 * (top_r - bot_r)

        return landmarks

    def get_mask(self, image_shape, landmarks106, color=(1,), expand_eyebrows=True):
        if(expand_eyebrows):
            new_landmarks = self.expand_eyebrows(landmarks106, 1.0)
        else:
            new_landmarks = np.array(landmarks106.copy(), dtype=np.float32)
        mask = np.zeros(image_shape[0:2] + (1,), dtype=np.float32)

        r_jaw = (new_landmarks[0:17], new_landmarks[33:34])
        l_jaw = (new_landmarks[16:33], new_landmarks[42:43])
        r_cheek = (new_landmarks[33:36], new_landmarks[16:17])
        l_cheek = (new_landmarks[40:43], new_landmarks[16:17])
        nose_ridge = (new_landmarks[35:41], new_landmarks[16:17])
        r_eye = (new_landmarks[33:38], new_landmarks[43:44], new_landmarks[47:52], new_landmarks[82:84], new_landmarks[16:17])
        l_eye = (new_landmarks[38:43], new_landmarks[43:44], new_landmarks[47:52], new_landmarks[82:84], new_landmarks[16:17])
        nose = (new_landmarks[43:52], new_landmarks[80:84])
        parts = [r_jaw, l_jaw, r_cheek, l_cheek, nose_ridge, r_eye, l_eye, nose]

        for item in parts:
            merged = np.concatenate(item).astype(np.int32)
            cv2.fillConvexPoly(mask, cv2.convexHull(merged), color)

        return mask

    def align(self, image, format, out_size=256, out_mask=False, out_M=False, method='umeyama', param=[4.5, 0.51, 7/18]):

        count, landmarks106 = self.face_detection.detect106(image, image_fmt=format)
        aligned_face = None
        aligned_lms = None
        aligned_mask = None
        cv_trans = None
        if count > 0:
            landmarks106 = landmarks106.reshape((106, 2))
            if method != 'xinghong_v4_ori':
                cv_trans = self.get_align_M(landmarks106, out_size, method, param)
                aligned_face = cv2.warpAffine(image, cv_trans, (out_size, out_size), flags=cv2.INTER_CUBIC)
                aligned_lms = np.dot(landmarks106, cv_trans[:2, :2].transpose()) + cv_trans[:, 2]
            else:
                h, w = image.shape[:2]
                left_eye_x, left_eye_y = landmarks106[74]
                right_eye_x, right_eye_y = landmarks106[77]
                eye_middle_x = (left_eye_x+right_eye_x)/2
                eye_middle_y = (left_eye_y+right_eye_y)/2
                nose = np.copy(landmarks106[46])
                eye_to_eye = np.sqrt((left_eye_x -right_eye_x)**2+(left_eye_y -right_eye_y)**2)

                crop_size = eye_to_eye*param[0]
                target_size = out_size
                factor = target_size/crop_size

                eye_to_nose = np.linalg.norm(np.array([eye_middle_x, eye_middle_y]) - nose)

                cur_left_eye_x = left_eye_x * factor
                cur_left_eye_y = left_eye_y * factor
                cur_right_eye_x = right_eye_x * factor
                cur_right_eye_y = right_eye_y * factor

                tar_eye_y = 0.51 - eye_to_nose / crop_size
                tar_left_eye_x = 7 / 18
                tar_right_eye_x = 1 - 7 / 18

                matRotate = cv2.getRotationMatrix2D((0, 0), 0, factor)
                warp_img = cv2.warpAffine(image, matRotate, (int(w * factor), int(h * factor)))

                fixed_points = np.array(
                    [[tar_left_eye_x, tar_right_eye_x, 0.5], [tar_eye_y, tar_eye_y, 0.51]]).transpose() * target_size
                target_lmks = np.array([[cur_left_eye_x, cur_right_eye_x, nose[0] * factor],
                                        [cur_left_eye_y, cur_right_eye_y, nose[1] * factor]]).transpose()
                trans, trans_inv = get_similarity_transform(target_lmks.copy(), fixed_points, reflective=True)

                cv2_trans = cvt_tform_mat_for_cv2(trans)

                aligned_face = cv2.warpAffine(warp_img, cv2_trans, (target_size, target_size))

                aligned_lms = np.matmul(landmarks106, matRotate[:2, :2].transpose())
                z_dim_lmks = np.ones(aligned_lms.shape[0])
                aligned_lms = np.column_stack((aligned_lms, z_dim_lmks))
                aligned_lms = np.matmul(aligned_lms, cv2_trans.transpose())[:,:2]

            if out_mask:
                aligned_mask = self.get_mask((out_size, out_size), aligned_lms, expand_eyebrows=True)

        if out_M:
            return aligned_face, aligned_lms, aligned_mask, cv_trans
        else:
            return aligned_face, aligned_lms, aligned_mask

    def align_with_lms(self, image, format, landmarks106, out_size=256, out_mask=False, out_M=False, method='umeyama', param=[4.5, 0.51, 7/18]):

        aligned_mask = None
        landmarks106 = landmarks106.reshape((106, 2)) 
        cv_trans = self.get_align_M(landmarks106, out_size, method, param)
        aligned_face = cv2.warpAffine(image, cv_trans, (out_size, out_size), flags=cv2.INTER_CUBIC)
        aligned_lms = np.dot(landmarks106, cv_trans[:2, :2].transpose()) + cv_trans[:, 2]

        if out_mask:
            aligned_mask = self.get_mask((out_size, out_size), aligned_lms, expand_eyebrows=True)

        if out_M:
            return aligned_face, aligned_lms, aligned_mask, cv_trans
        else:
            return aligned_face, aligned_lms, aligned_mask

    def align_list(self, images, format, out_size=256, out_mask=False, affine_transform=True):

        count, landmarks106 = self.face_detection.detect106(images[0], image_fmt=format)
        warp_images = []
        mask_images = []
        if count > 0:
            landmarks106 = landmarks106.reshape((106, 2))
            if (affine_transform):
                landmarks5 = landmarks106[[74, 77, 46, 84, 90]]
                fixed_points5 = self.fixed_points5 * out_size
                trans, _ = get_similarity_transform(landmarks5, fixed_points5, reflective=True)
                cv2_trans = cvt_tform_mat_for_cv2(trans)
                landmarks106 = np.dot(landmarks106, cv2_trans[:2, :2].transpose()) + cv2_trans[:, 2]
            for image in images:
                if (affine_transform):
                    warp_image = cv2.warpAffine(image, cv2_trans, (out_size, out_size))
                    warp_images.append(warp_image)
                else:
                    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                    warp_images.append(image)
                if out_mask:
                    mask_image = (self.get_mask((out_size, out_size), landmarks106) * 255).astype(np.uint8)
                    mask_images.append(mask_image)

        return warp_images, mask_images, landmarks106

def expand_eyebrows_func(landmarks, eyebrows_expand_mod=1.0):
    landmarks = np.array(landmarks.copy(), dtype=np.float32)

    #
    ml_pnt = (landmarks[52] + landmarks[0]) / 2
    mr_pnt = (landmarks[61] + landmarks[31]) / 2
    ql_pnt = (landmarks[52] + ml_pnt) / 2
    qr_pnt = (landmarks[61] + mr_pnt) / 2

    # Top of the eye arrays
    bot_l = np.array((ql_pnt, landmarks[52], landmarks[53], landmarks[54], landmarks[55]))
    bot_r = np.array((landmarks[58], landmarks[59], landmarks[60], landmarks[61], qr_pnt))
    top_l = landmarks[33:38]
    top_r = landmarks[38:43]

    landmarks[33:38] = top_l + eyebrows_expand_mod * 0.5 * (top_l - bot_l)
    landmarks[38:43] = top_r + eyebrows_expand_mod * 0.5 * (top_r - bot_r)

    return landmarks

def get_mask(image_shape, landmarks, color=(1,), expand_eyebrows=False):
    mask = np.zeros(image_shape[0:2] + (1,), dtype=np.float32)
    parts = []
    if(expand_eyebrows):
        new_landmarks = expand_eyebrows_func(landmarks, 1.0)
        mask = np.zeros(image_shape[0:2] + (1,), dtype=np.float32)

        r_jaw = (new_landmarks[0:17], new_landmarks[33:34])
        l_jaw = (new_landmarks[16:33], new_landmarks[42:43])
        r_cheek = (new_landmarks[33:36], new_landmarks[16:17])
        l_cheek = (new_landmarks[40:43], new_landmarks[16:17])
        nose_ridge = (new_landmarks[35:41], new_landmarks[16:17])
        r_eye = (new_landmarks[33:38], new_landmarks[43:44], new_landmarks[47:52], new_landmarks[82:84], new_landmarks[16:17])
        l_eye = (new_landmarks[38:43], new_landmarks[43:44], new_landmarks[47:52], new_landmarks[82:84], new_landmarks[16:17])
        nose = (new_landmarks[43:52], new_landmarks[80:84])
        parts = [r_jaw, l_jaw, r_cheek, l_cheek, nose_ridge, r_eye, l_eye, nose]

        for item in parts:
            merged = np.concatenate(item).astype(np.int32)
            cv2.fillConvexPoly(mask, cv2.convexHull(merged), color)
    else:
        new_landmarks = np.array(landmarks.copy(), dtype=np.float32)
        for i in range(0,44):
            outline = [int(landmarks[i,0]), int(landmarks[i,1])]
            parts.append(outline)    
            
        parts = np.array(parts)
        filler = cv2.convexHull(parts)
        cv2.fillConvexPoly(mask, filler, 1)

    return mask

  
   
