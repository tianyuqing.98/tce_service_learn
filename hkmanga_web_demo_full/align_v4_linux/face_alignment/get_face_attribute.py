import ctypes
import numpy as np
import cv2
import os
import sys
import time


IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]


def is_image_file(filename):
    return any(filename.endswith(extension) for extension in IMG_EXTENSIONS)


   
class FaceAttribute:
    def __init__(self, model_path, attr_extra_path, lib_path, slow_mode=False):
        
        self.fa = ctypes.cdll.LoadLibrary(lib_path)
        ret = self.fa.init_FS_ATTR(model_path, attr_extra_path, slow_mode)
        if ret==0:
            print("init_FA success!")
            
       
    def detect(self, image, landmarks, count, use_rgba=False):
        
        h, w, c = image.shape
        if use_rgba:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGBA)
   
        type_c = (ctypes.c_ubyte * 1 )()
        landmarks_c = (ctypes.c_float * 212)()
        for i in range(106 * 2) : landmarks_c[i] = landmarks[i]
        skin_type = -1
        ret = self.fa.do_predict_FS_ATTR(image.tostring(), h, w, w*c ,use_rgba, landmarks_c, count, type_c)
       
        if ret==-1:
            return 0, None
        skin_type = np.ctypeslib.as_array(type_c)[0]
       

        return 1, skin_type
    
    
    def detect_gender_full(self, image, landmarks, count, use_rgba=False):
        
        h, w, c = image.shape
        if use_rgba:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGBA)
   
        type_c = (ctypes.c_float * 5 )()
        landmarks_c = (ctypes.c_float * 1060)()
        for i in range(count*212) : landmarks_c[i] = landmarks[i]
        gender_type = np.zeros(count)*(-1)
        ret = self.fa.do_predict_ALL_GENDER(image.tostring(), h, w, w*c , landmarks_c, count,  type_c)
       
        if ret==-1:
            return 0, None
        
#        print(type_c)
        for i in range(count):
            if np.ctypeslib.as_array(type_c)[i]>0.5:
                gender_type[i] = 1
            else:
                gender_type[i] = 0
       

        return count, gender_type

  
