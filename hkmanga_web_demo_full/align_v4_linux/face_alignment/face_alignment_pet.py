#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 23 14:04:37 2020

@author: huxinghong
"""
import ctypes
import os
import numpy as np
import cv2



            
class PetDetector:
    def __init__(self, lib_path, model_path, max_face_num):
        self.lib = ctypes.cdll.LoadLibrary(lib_path)

        self.lib.create_pet_detector.restype = ctypes.c_void_p
        self.lib.delete_pet_detector.argtypes = [ctypes.c_void_p]
        self.lib.detector_init.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_uint]
        self.lib.detector_init.restype = ctypes.c_int
        self.lib.pf_predict.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_int, ctypes.c_int, ctypes.c_int]
        self.lib.pf_predict.restype = ctypes.c_int
        self.lib.get_pet_num.argtypes = [ctypes.c_void_p]
        self.lib.get_pet_num.restype = ctypes.c_int
        self.lib.packup.argtypes = [ctypes.c_void_p, ctypes.c_void_p]

        self.detector_ptr = self.lib.create_pet_detector()
        self.model_path = model_path
        self.max_face_num = max_face_num

        self.init()

    def init(self):
        status = self.lib.detector_init(self.detector_ptr, self.model_path, self.max_face_num)
        if status != 0:
            print('Init err')

    def detect(self, img):
        h, w, c = img.shape
        status = self.lib.pf_predict(self.detector_ptr, img.tostring(), h, w, w*c)
        if status < 0:
            print('Detection err:', status)

    @staticmethod
    def parse(raw):
        N = int(len(raw) / 191)
        ret = []
        idx = 0
        for i in range(N):
            tmp = dict()
            type = int(raw[idx])
            idx += 1
            if type == 1:
                tmp['type'] = 'cat'
                tmp['n_point'] = 82
            elif type == 2:
                tmp['type'] = 'dog'
                tmp['n_point'] = 76
            elif type == 3:
                tmp['type'] = 'human'
                tmp['n_point'] = 4
            else:
                tmp['type'] = 'other'
                tmp['n_point'] = 4

            tmp['rect'] = np.array(raw[idx: idx+4]).astype(np.int)
            idx += 4

            tmp['score'] = raw[idx]
            tmp['yaw'] = raw[idx+1]
            tmp['pitch'] = raw[idx+2]
            tmp['roll'] = raw[idx+3]
            tmp['id'] = int(raw[idx+4])
            tmp['action'] = int(raw[idx+5])
            idx += 6
            tmp['points'] = np.array(raw[idx:idx+tmp['n_point'] * 2]).reshape((-1, 2))
            idx += 2*90
            ret.append(tmp)
        return ret

    def get_result(self):
        num = self.lib.get_pet_num(self.detector_ptr)
        size = num * 191
        buffer = (ctypes.c_float * size)()
        self.lib.packup(self.detector_ptr, buffer)
        return self.parse(buffer)


   

