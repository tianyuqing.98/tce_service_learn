import euler
from cartoonfull_thrift import CartoonService
from cartoonfull_thrift import ProcessCartoonfullRequest, ProcessCartoonfullResponse
import time, argparse


def process_response(image_data):
    print('image recv')
    f = open('images/out_3.jpg', 'wb')
    f.write(image_data.image)
    f.close()

    '''o = open('images/back.png', 'wb')
    o.write(image_data.)
    o.close()
    print(image_data.width)
    '''





if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, default=1234, help='listen port')
    args = parser.parse_args()

    client = euler.Client(CartoonService, 'tcp://localhost:%d' % (args.port))
    # read one image and send to server
    fd = ProcessCartoonfullRequest()
    #fd.image = open('images/four.jpg', 'rb').read()
    fd.image = open('images/many.jpg', 'rb').read()
    #fd.image = open('trans.png', 'rb').read()
    #fd.image = open('test.png', 'rb').read()
    fd.attribute = '{"app_id":0, "effect_id":0, "clip_type":0, "face_type":1,"face_count":5}'
    #fd.image = open('t.sh', 'rb').read()
    time_st = time.time()
    resp = client.Process(fd)
    time_ed = time.time()
    print('thread: %f' % (time_ed - time_st))
    process_response(resp)
    print('client done')
