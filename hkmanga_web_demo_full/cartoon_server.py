import euler
from cartoonfull_thrift import CartoonService, ProcessCartoonfullRequest, ProcessCartoonfullResponse
from base_thrift import BaseResp
import cv2, os, sys
import numpy as np
import multiprocessing as mp
import threading
from align_v4_linux.align_single_v4_smash import LandDetector, draw_by_map, post_proc, apply_lut
sys.path.append('pix2pix')
from jpcartoon import Cartooner, getOpt
import pdb, time, argparse, json
#
PROCESS_NUM = 6
BATCH_SIZE = 1
land_detector = None
tasks_queue = None
signal_queue = None
result_queue = [mp.Queue() for _ in range(PROCESS_NUM)]
cv = [mp.Condition() for _ in range(PROCESS_NUM)]
cv_index = None
cv_try = mp.Condition()
bg_size = 960



#clip_key的两个数分别对应align_methods， align_para, whether to crop bg before processing, fusion mask index, whether to fill white caused by model
clip_keys = [
            ["00000", "00000"],#jp大小模型
            ["01101","01000"], #hk大小模型
            ["01001","01001"],#jz大小模型
            ["12010","13020",#tc人脸大小模型
              "14030","15030"],#tc宠物大小模型
            ]
big_model_threshs = [1,1,1,1] #使用大模型的最大人脸数
model_paths = [
    ['jp_female_small,384,1.1g,0','jp_male_small,384,1.1g,0','jp_female_big,384,100G,1','jp_male_big,384,100G,1', 'jp_bg,960,1.1g,0'], 
    ['hk_female_small,384,970M,0','hk_male_small,384,970M,0','hk_female_big,384,100G,1','hk_male_big,384,100G,1', 'jp_bg,960,1.1g,0'], 
    ['jz_female_small,384,450M,0','jz_male_small,384,450M,0','jz_female_big,384,20g,0','jz_male_big,384,20g,0', 'jz_bg,960,350M,0'], 
    ['tc_human_small,384,1.1g,0','tc_pet_small,384,808M,0','tc_human_big,384,100G,1','tc_pet_big,384,100G,1', 'tc_bg,960,1.1g,0'], 
]

class SingleImage():
    def __init__(self, effect_id, use_big_model, bg_image, face_type1=[], face_type2=[],  app_id=0):
        self.bg_image = bg_image
        self.face_type1 = face_type1
        self.face_type2 = face_type2
        self.app_id = app_id
        self.effect_id = effect_id
        self.use_big_model = use_big_model
        self.cv_index = None

class CudaServer(threading.Thread):
    def __init__(self, linger, batch_size, face_net=2, bg_net=5, image_size = 960):
        threading.Thread.__init__(self)
        self.busy = False
        self.cartoon = []
        
        for i in range(len(model_paths)):
            cartoon_cur = []
            for j in range(len(model_paths[i])):
                epoch = model_paths[i][j].split(',')[0]
                image_size = int(model_paths[i][j].split(',')[1])
                flops = model_paths[i][j].split(',')[2]
                no_dropout = True if model_paths[i][j].split(',')[3]=="1" else False
                cartoon_cur.append(Cartooner(getOpt(net_flops=flops,  image_size=image_size, epoch=epoch, no_dropout=no_dropout)))
            self.cartoon.append(cartoon_cur)

        '''original implementation
        #net_type: 2 for cyclegan model, 5 for pix2pix model 1.7G, 4 for pix2pix model 400M
        #1 and 2 instance for jianying female and male
        self.cartoon.append(Cartooner(getOpt(net_type=5, image_size=384, epoch='female')))
        self.cartoon.append(Cartooner(getOpt(net_type=5, image_size=384, epoch='male')))

        #3 and 4 instance for xingtu female and male
        self.cartoon.append(Cartooner(getOpt(net_type=2, image_size=384, epoch='female')))
        self.cartoon.append(Cartooner(getOpt(net_type=2, image_size=384, epoch='male')))

        self.cartoon_bg = Cartooner(getOpt(net_type=bg_net, image_size=image_size))
        '''
        self.run_first()
        self.con = threading.Condition()
        self.linger = linger
        self.batch_size = batch_size
        self.timer = threading.Timer(self.linger, self.onTimer)
        self.timer.start()

    def run_first(self):
        '''import torch
        temp = torch.randn(720, 1280, 3)
        out = self.cartoon_bg.transform_batch([temp])'''

        img = cv2.imread('./images/clip.png').astype(np.float32)
        imgs = [img]
        self.cartoon[0][0].transform_batch(imgs)
        self.cartoon[0][1].transform_batch(imgs)
        self.cartoon[0][2].transform_batch(imgs)
        self.cartoon[0][3].transform_batch(imgs)
        img2 = cv2.resize(img, (960, 960))
        self.cartoon[0][4].transform_batch([img2])

        '''
        time_st = time.time()
        imgs = [img]
        self.cartoon[0].transform_batch(imgs)
        time_ed = time.time()
        print('384 time: %f' % (time_ed - time_st))
        for i in range(10):
            self.test_time(imgs)

        img = cv2.imread('./test/3.png').astype(np.float32)
        img = cv2.resize(img, (960, 960))
        imgs = [img]
        self.cartoon_bg.transform_batch(imgs)
        t1 = time.time()
        self.cartoon_bg.transform_batch(imgs)
        t2 = time.time()
        print('960 time:%f' % (t2 - t1))
        '''
        print('first run done')
    
    ''' deprecate functions
    def test_time(self, imgs):
        time_st = time.time()
        self.cartoon.transform_batch(imgs)
        time_ed = time.time()
        print('time: %f' % (time_ed - time_st))

    def process_one(self, clipped_image):
        output = self.cartoon.transform(clipped_image)
        return output

    def run1(self):
        while True:
            simage = tasks_queue.get(True)
            output = self.process_one(simage.image)
            result_queue[simage.cv_index].put(output)
            
            cv_index.put(simage.cv_index)
    '''
    def run_batch(self):
        if self.busy: return
        self.busy = True

        images = []
        index = []
        n = 0
        
        while not tasks_queue.empty():
            simage = tasks_queue.get(True)
            effect_id = simage.effect_id
            #t1 = time.time()
            if True:
            # try:
                face_type1 = []
                face_type2 = []
                if simage.use_big_model == 0: 
                    print("use small model")
                    if len(simage.face_type1) > 0:
                        print("use female model")
                        face_type1 = self.cartoon[effect_id][0].transform_batch(simage.face_type1)
                    if len(simage.face_type2) > 0:
                        print("use male model")
                        face_type2 = self.cartoon[effect_id][1].transform_batch(simage.face_type2)
                else: 
                    print("use big model")
                    if len(simage.face_type1) > 0:
                        face_type1 = self.cartoon[effect_id][2].transform_batch(simage.face_type1)
                    if len(simage.face_type2) > 0:
                        face_type2 = self.cartoon[effect_id][3].transform_batch(simage.face_type2)

                back = self.cartoon[effect_id][4].transform_batch([simage.bg_image])[0]
                result_queue[simage.cv_index].put([back, face_type1, face_type2])
                cv_index.put(simage.cv_index)
            # except BaseException as e:
            #     print('cuda server error:' + str(e))
            #t2 = time.time()
            #print('GPU time:{}'.format(t2-t1))

        self.busy = False

    def run(self):
        while True:
            signal_queue.get(True)
            #print('full run')
            self.tryRun()

    def onTimer(self):
        #print('on timer run')
        self.tryRun()

        self.timer = threading.Timer(self.linger, self.onTimer)
        self.timer.start()

    def tryRun(self):
        if not self.busy:
            while tasks_queue.qsize() > 0:
                self.run_batch()

def process_init():
    global land_detector
    land_detector = LandDetector()
    print('smash detector created by process {}'.format(os.getpid()))

server = euler.Server(CartoonService, post_fork_callback=process_init)

def getResp(code, status):
    resp = ProcessCartoonfullResponse()
    resp.BaseResp = BaseResp()
    resp.BaseResp.StatusMessage = status
    resp.BaseResp.StatusCode = code
    return resp


@server.register('Process')
def Process(ctx, req):
    #print('------------------')
    face_count = 1 
    size_thresh = 1920
    app_id = 0 
    effect_id = 0 #the type of comic
    try:
        if req.attribute is not None and req.attribute != '':
            attrs = json.loads(req.attribute)
            app_id = int(attrs['app_id'])
            effect_id = int(attrs['effect_id']) 
            clip_type = int(attrs['clip_type'])
            face_type = int(attrs['face_type'])
            face_count = int(attrs['face_count'])
    except BaseException as e:
        print('attribute parse error')
        resp = getResp(5, str(e))
        return resp
    
  
    clip_key = clip_keys[effect_id]
    big_model_thresh = big_model_threshs[effect_id]
    

    # if app_id== 0:
    #     net_id = 0
    # else:
    #     net_id = 1
    max_face = face_count

    try:
        image = cv2.imdecode(np.asarray(bytearray(req.image), dtype=np.uint8), cv2.IMREAD_COLOR) 
        height, width, _ = image.shape
        if height > size_thresh and height <= width:
            width = int(size_thresh / height * width)
            height = size_thresh
            image = cv2.resize(image, (width, height))
        elif width > size_thresh and width < height:
            height = int(size_thresh / width * height)
            width = size_thresh
            image = cv2.resize(image, (width, height))
    except BaseException as e:
        print('imdecode failed:' + str(e))
        resp = getResp(1, str(e))
        return resp

    if clip_type == 2:
        image = land_detector.img_preprocess(image, 1)

    human_count, human_face_info, pet_count, pet_face_info = land_detector.detect(image, effect_id)
    face_info_full = (human_count, human_face_info, pet_count, pet_face_info)
    count = human_count+pet_count
    count = min((count, max_face))
    bg_mask = None

    print(("count ", count))

    # if True:
    try:
        if count<=big_model_thresh or app_id == 1 or clip_type==0:
            net_id = 1
            count, face_img, bg_img, cv2_trans_inv,warp_map_back, map_kernel, bg_mask, face_type, face_mask, ori_face, type1_count \
                    = land_detector.clip(image, face_info_full, method_para=clip_key, effect_id=effect_id, max_face=max_face, app_id=app_id, \
                                            bg_mask=bg_mask, big_model=1)
        else:
            net_id = 0
            count, face_img, bg_img, cv2_trans_inv,warp_map_back, map_kernel, bg_mask, face_type, face_mask, ori_face, type1_count \
                    = land_detector.clip(image, face_info_full, method_para=clip_key, effect_id=effect_id, max_face=max_face, app_id=app_id, \
                                            bg_mask=bg_mask,  big_model=0)

    except BaseException as e:
        print('Land detector failed:' + str(e))
        resp = getResp(2, 'Land detector failed')
        return resp

    if clip_type==0:
        print(("face_count ", count))
        count = np.min((face_count, count))
    if count<1: 
        if clip_type==0 or clip_type == 2:
            print('No faces detected')
            resp = getResp(3, 'No faces detected')
            return resp
    #####################
    # try:
    if True:
        # origin: BGR
        bg_img = cv2.resize(bg_img, (bg_size, bg_size))
        bg_img = cv2.cvtColor(bg_img, cv2.COLOR_BGR2RGB) # to rgb
        bg_img = bg_img.astype(np.float32)# / 127.5 - 1.0
        

        face_type1 = []
        face_type2 = []
        trans_inv1 = []
        trans_inv2 = []
        face_mask1 = []
        face_mask2 = []
        type_index = []
        for i in range(count):
            cur_clipped = cv2.cvtColor(face_img[i], cv2.COLOR_BGR2RGB).astype(np.float32)
            # cv2.imwrite("cur_clipped"+str(i)+'.jpg', cur_clipped)
            # cv2.imwrite("cur_map_"+str(i)+'.jpg', face_mask[i] )
            if face_type[i] == 0:
                type_index.append(len(face_type1))
                face_type1.append(cur_clipped)
                trans_inv1.append(cv2_trans_inv[i])
                face_mask1.append(face_mask[i])
            else:
                type_index.append(len(face_type2))
                face_type2.append(cur_clipped)
                trans_inv2.append(cv2_trans_inv[i])
                face_mask2.append(face_mask[i])
        
        simage = SingleImage(effect_id, net_id, bg_img, face_type1, face_type2, app_id)
        simage.cv_index = cv_index.get(True)
        tasks_queue.put(simage)
        if tasks_queue.qsize() >= BATCH_SIZE:
            signal_queue.put(1)
        
        result = result_queue[simage.cv_index].get(True) # np, float, (-1,1), CHW
        result_bg = result[0]
        face_type1 = result[1]
        face_type2 = result[2]
        # cv2.imwrite("result_bg.jpg", result_bg)
        if clip_type==0:
            
            result = face_type1[0] if len(face_type1)>0 else face_type2[0]
            cv2_trans = trans_inv1[0] if len(face_type1)>0 else trans_inv2[0]
            bg_mask = face_mask1[0] if len(face_type1)>0 else face_mask2[0]
            result_bg = cv2.resize(result_bg, (image.shape[1], image.shape[0]))
            # cv2.imwrite("result.jpg", result)
            width = result.shape[1]
            height = result.shape[0]
        elif effect_id!=3:
            result = cv2.resize(result_bg, (image.shape[1], image.shape[0]))
            if effect_id<=1:
                result  = apply_lut(result, land_detector.jianying_bg_lut)
            for i in range(count):
                if face_type[i] == 1: # boy
                    result_face = face_type2[type_index[i]]
                else:
                    result_face = face_type1[type_index[i]]
                    #cv2.imwrite('face.png', result_faces[i])
                    #cv2.imwrite('back.png', result)
                # cv2.imwrite('result'+str(i)+'.png', result)
                # cv2.imwrite('warp_map_back'+str(i)+'.png', warp_map_back[i])
                result = draw_by_map(1, result_face, result, warp_map_back[i], cv2_trans_inv[i], [int(map_kernel[i])], (image.shape[1], image.shape[0]))
        else:
            result = cv2.resize(result_bg, (image.shape[1], image.shape[0]))
            for i in range(len(face_type2)):
                result_face = face_type2[i]
                if i==0:
                    result_face  = apply_lut(result_face, land_detector.jianying_pet_lut)
                result = draw_by_map(1, result_face, result, warp_map_back[i+type1_count], cv2_trans_inv[i+type1_count], [int(map_kernel[i+human_count])], (image.shape[1], image.shape[0]))
   
            for i in range(len(face_type1)):
                result_face = face_type1[i]
                result = draw_by_map(1, result_face, result, warp_map_back[i], cv2_trans_inv[i], [int(map_kernel[i])], (image.shape[1], image.shape[0]))
   

    
        if bg_mask is not None:
            if clip_type==1 or clip_type==2: #whole image
                ori_image = cv2.resize(result_bg, (result.shape[1],result.shape[0]))
            else:
                print((result_bg.shape, cv2_trans.shape, (width, height)))
                ori_image = cv2.warpAffine(result_bg, cv2_trans, (width, height))
                # ori_image = ori_face[0]
                # cv2.imwrite("bg_mask.jpg", bg_mask)
            # bg_mask = cv2.GaussianBlur(bg_mask, (11,11), 0)
            # bg_mask[bg_mask<np.max(bg_mask)] = 0
            # bg_mask = cv2.GaussianBlur(bg_mask, (7,7), 0)
           
           
            bg_mask = bg_mask.astype(np.float32)/255.0
            result = result*(bg_mask)+ori_image*(1-bg_mask)
            result[result<0] = 0
            result[result>255] = 255

        if effect_id<=1:
            result  = post_proc(result, land_detector.jianying_lut, land_detector.jianying_texture, 0)
        # else:
            # if effect_id>0:
            #     result  = post_proc(result, land_detector.xingtu_luts[effect_id-1], land_detector.xingtu_textures[effect_id-1], 1)
        
        cv_output = cv2.imencode('.jpg', result)[1].tostring()
        #cv2.imwrite('out.png', cv_output)

        # response
        resp = getResp(0, 'Succeed')
        resp.image = cv_output
        #resp.clip = origin
        resp.width = width
        resp.height = height
        #time_ed = time.time()
        #print('total time: %f' % (time_ed - time_st))

        return resp
    # except BaseException as err:
    #     print('process error:' + str(err))
    #     resp = getResp(4, str(err))
    #     return resp


if __name__ == '__main__':
    print('server started')
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, default=1234, help='listen port')
    args = parser.parse_args()
    print('server port:{}'.format(args.port))
    os.system('kill -9 $(lsof -i :%d -t)' % (args.port))

    tasks_queue = mp.Queue()
    signal_queue = mp.Queue()
    cv_index = mp.Queue()
    for i in range(PROCESS_NUM): cv_index.put(i)

    cuda_server = CudaServer(.3, BATCH_SIZE)
    cuda_server.start()
    server.run('tcp://0.0.0.0:%d' % (args.port), workers_count=PROCESS_NUM)
    #server.run('tcp://0.0.0.0:%d' % (1234), workers_count=PROCESS_NUM)
    #server.run('sd://toutiao.effect.japcartoon')
