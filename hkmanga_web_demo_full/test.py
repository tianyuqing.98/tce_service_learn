import sys
sys.path.append('./superres')
from superres.upsample import Upsampler
import cv2, os
import numpy as np
####
def upone(image_upsampler, in_path, out_path):
    #image_upsampler = Upsampler()
    image = cv2.imread(in_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = np.transpose(image, (2, 0, 1)).copy()
    image = image.astype(np.float32) / 255.0 * 2.0 - 1.0

    output = image_upsampler.filter(image)
    cv2.imwrite(out_path, output)

def upSome(image_upsampler, in_dir, out_dir):
    for root, dirs, files in os.walk(in_dir):
        for f in files:
            src_path = os.path.join(root, f)
            out_path = os.path.join(out_dir, f)
            upone(image_upsampler, src_path, out_path)

if __name__ == '__main__':
    image_upsampler = Upsampler()
    src_dir = 'test_in2'
    dst_dir = 'test_out2'
    upSome(image_upsampler, src_dir, dst_dir)
