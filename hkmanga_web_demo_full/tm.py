from __future__ import print_function
import os, sys, time
import threading, argparse
import euler
from jpcartooncut_thrift import CartoonService
from jpcartooncut_thrift import ProcessJpcartoonRequest, ProcessJpcartoonResponse

class MThread(threading.Thread):
    def __init__(self, dir_name, id):
        threading.Thread.__init__(self)
        self.id = id
        self.path = dir_name
        self.fd = ProcessJpcartoonRequest()
        self.client = euler.Client(CartoonService, 'tcp://localhost:1234')

    def run(self):
        for i in range(100):
            self.run_once()

    def run_once(self):
        for _, _, files in os.walk(self.path):
            for fname in files:
                full_path = os.path.join(self.path, fname)
                #time_st = time.time()
                process(self.client, full_path, self.id)
                #time_ed = time.time()
                #print('thread:%d, %f' % (self.id, time_ed - time_st))

def process(client, file_path, id):
    fd = ProcessJpcartoonRequest()
    fd.image = open(file_path, 'rb').read()
    time_st = time.time()
    resp = client.Process(fd)
    time_ed = time.time()
    print('thread:%d, %f' % (id, time_ed - time_st))
    print('client done')


if __name__ == '__main__':
    threadList = []
    num = 8
    for i in range(num):
        threadList.append(MThread('./test', i))

    for i in range(num):
        threadList[i].start()

