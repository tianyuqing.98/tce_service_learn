import os, cv2
from options.test_options import TestOptions
from models import create_model
import torch
from util import util
from PIL import Image
from data.base_dataset import get_params, get_transform
import numpy as np
import pdb


class Cartooner():
    def __init__(self, opt):
        self.model = create_model(opt)
        self.model.setup(opt)
        self.model.eval()

    def transform(self, clipped_image):
        input = torch.from_numpy(clipped_image).permute(2, 0, 1).unsqueeze(0)
        with torch.no_grad():
            self.model.forward1(input)
  
        output = util.tensor2im(self.model.fake_B)
        return output

    def transform_batch(self, clipped_images):
        batch_size = len(clipped_images)
        #print(clipped_images[0].shape)
        '''
        inputs = torch.zeros(batch_size, 3, 384, 384)
        for i, img in enumerate(clipped_images):
            input = torch.from_numpy(img).permute(2, 0, 1)
            inputs[i] = input
        '''
        inputs = torch.from_numpy(np.stack(clipped_images))
        #print(inputs.shape)
        with torch.no_grad():
            self.model.forward1(inputs)

        #print(self.model.fake_B.shape)
        output = []
        for i in range(batch_size):
            fake_b = (self.model.fake_B[i].permute(1, 2, 0) + 1.0) * 127.5
            fake_b = torch.flip(fake_b, (2,))
            image_numpy = fake_b.cpu().float().numpy()
            #image_numpy = self.model.fake_B[i].cpu().float().numpy()
            #image_numpy = (np.transpose(image_numpy, (1, 2, 0)) + 1.0) * 127.5
            #output.append(image_numpy.astype(np.uint8))
            #print(image_numpy.shape)
            output.append(image_numpy) # CHW, (-1, 1)
        #output = util.tensor2im(self.model.fake_B)
        return output


def getOpt(net_flops, image_size = 720, epoch = None, no_dropout=True):
    
#    print("bg net is "+str(bg_net))
    print(("no_dropout", no_dropout))
    opt = TestOptions().parse()
    #opt.gpu_ids = '0'
#    opt.num_threads = 0
    opt.batch_size = 1
    opt.serial_batches = True
    opt.no_flip = True
    opt.display_id = -1
    opt.load_size = image_size
    opt.crop_size = image_size
    opt.epoch = epoch
    opt.name = 'checkpoints'
    opt.no_dropout = no_dropout
    if net_flops=='100G':
        opt.netG = 'resnet_9blocks'
        opt.direction = 'AtoB'
        opt.model = 'cycle_gan'
        # opt.flops = net_flops
        print("init 100G model")
    else:
        opt.direction = 'BtoA'
        opt.model = 'pix2pix_small'
        opt.netG = 'UnetMobileWithConfigQuantNgfs_bias'
        opt.flops = net_flops
        print("init model "+net_flops)

    
    # if net_type==1: 
    #     opt.name = 'cycle384'
    #     opt.load_size = 384
    #     opt.crop_size = 384
    #     opt.model = 'cycle_gan'
    #     opt.direction = 'AtoB'
    #     opt.no_dropout = True
    #     opt.epoch = "1w6"
    # elif net_type==2: #stylegan
    #     opt.name = 'cycle384'
    #     opt.load_size = 384
    #     opt.crop_size = 384
    #     opt.model = 'cycle_gan'
    #     opt.direction = 'AtoB'
    #     opt.no_dropout = True
    # elif net_type==3:
    #     opt.name = 'pix2pix384'
    #     opt.load_size = 384
    #     opt.crop_size = 384
    #     opt.model = 'pix2pix'
    #     opt.direction = 'BtoA'
    #     opt.no_dropout = False
    # elif net_type==4: #400M
    #     opt.name = 'pix2pix_small'
    #     opt.load_size = image_size
    #     opt.crop_size = image_size
    #     opt.model = 'pix2pix_small'
    #     opt.direction = 'BtoA'
    #     opt.no_dropout = False
    #     opt.netG = 'UnetMobileWithConfigQuant'
    # elif net_type==5: #1.7G
    #     opt.name = 'pix2pix_small'
    #     opt.load_size = image_size
    #     opt.crop_size = image_size
    #     opt.model = 'pix2pix_small'
    #     opt.direction = 'BtoA'
    #     opt.no_dropout = False
    #     opt.netG = 'UnetMobileWithConfigQuantNgfs_bias'
    #     opt.epoch = "1700"
    # elif net_type==6: #1.35G, tmp
    #     opt.name = 'pix2pix_small'
    #     opt.load_size = image_size
    #     opt.crop_size = image_size
    #     opt.model = 'pix2pix_small'
    #     opt.direction = 'BtoA'
    #     opt.no_dropout = False
    #     opt.netG = 'UnetMobileWithConfigQuantNgfs_bias'
    #     opt.epoch = "female_1.35"
   
    # if epoch!=None:
    #     opt.epoch = epoch #note, another special treatment for 970M face model in pix2pix_small_model.py
    opt.num_threads = 1
    opt.norm = 'instance'
    opt.checkpoints_dir = './pix2pix/checkpoints'
    
    opt.gpu_ids = [0]
  
    return opt

if __name__ == '__main__':
    #pdb.set_trace()

    opt = getOpt('1.7g')
    #opt.print_options(opt)
    print(opt.direction)
    
    cartoon = Cartooner(opt)
    image1 = cv2.cvtColor(cv2.imread('clip.png'), cv2.COLOR_BGR2RGB)
    image1 = image1.astype(np.float32) / 127.5 - 1.0

    image2 = cv2.cvtColor(cv2.imread('clip.png'), cv2.COLOR_BGR2RGB)
    image2 = image2.astype(np.float32) / 127.5 - 1.0
    #image = Image.open('test.png').convert('RGB')
    #clip = self.detector.clip(image)
    #cv2.imwrite('clip.png', clip)
    images = [image1, image2]
    output = cartoon.transform_batch(images) 
#    print(output[0].shape)

    img_np = (np.transpose(output[0], (1, 2, 0)) + 1.0) * 127.5
    img_np = img_np.astype(np.uint8)


    cv2.imwrite('out.png', img_np)
    #util.save_image(output[0], 'out.png')

    #cv_img = cv2.imencode('.jpg', output)[1].tostring()
    #cv2.imwrite('cv.png', cv_img)
    #open('cv.png', 'wb').write(Image.fromarray(output))

