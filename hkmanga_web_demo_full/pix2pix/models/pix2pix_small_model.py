#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 26 19:48:07 2020

@author: huxinghong
"""

import torch
from .base_model import BaseModel
from . import networks
from . import ssim_loss
import pdb
import os
from collections import OrderedDict


def gen_ngfs(ngf, ngfs, input_nc, output_nc):
    if ngf > 0:
        return ngf
    else:
        res0 = list(map(int, ngfs.split(',')))
        for i in res0:
            if i % 4 != 0:
                print('ERROR! {:d}'.format(i))
                return -1
        res0.insert(0, input_nc)
        res0.insert(-1, output_nc)
        res = [[res0[3 * i], res0[3 * i + 1], res0[3 * i + 2]] for i in range(len(res0) // 3)]
        return res
    
def flops2params(flops):
    if flops == '1.1g':
        params = '24,12,24,48,48,48,96,96,96,192,192,192,192,192,192,192,384,192,192,384,192,192,384,192,96,384,96,' \
                 '48,192,48,24,96,24,48'
    elif flops == '808M':
        params = '24,8,24,48,32,48,96,72,96,192,224,192,192,296,192,192,360,192,192,360,192,192,296,192,96,256,96,48,' \
                 '160,48,24,40,24,32'
    elif flops == '612M':
        params = '24,8,24,36,24,36,80,44,80,156,96,156,156,116,156,156,204,156,156,188,156,156,224,156,80,252,80,36,' \
                 '120,36,24,60,24,28'
    elif flops == '450M':
        params = '16,8,16,32,24,32,64,48,64,128,100,128,128,128,128,128,240,128,128,248,128,128,256,128,64,160,64,32,' \
                 '80,32,16,32,16,32'
    elif flops == '350M':
        params = '16,8,16,32,16,32,64,32,64,128,64,128,128,128,128,128,128,128,128,192,128,128,192,128,64,128,64,32,' \
                 '56,32,16,24,16,24'
    elif flops == '189M':
        params = '12,8,12,24,16,24,48,24,48,128,48,128,128,128,128,128,128,128,128,160,128,128,160,128,48,48,48,24,' \
                 '24,24,12,12,12,12'
    elif flops == '100M':
        params = '12,8,12,16,12,16,28,16,28,104,28,104,104,100,104,104,100,104,104,128,104,104,128,104,28,28,28,16,' \
                 '16,16,12,12,12,8'
    elif flops == '80M':
        params = '12,8,12,16,12,16,28,16,28,104,28,104,104,100,104,104,100,104,104,128,104,104,128,104,28,28,28,16,' \
                 '16,16,12,12,12,8'
    elif flops == '6g':
        params = '64,44,64,124,204,124,252,400,252,504,832,504,504,952,504,504,940,504,504,868,504,504,864,504,252,' \
                 '852,252,124,388,124,64,176,64,64'
    elif flops == '8g':
        params = 52
    elif flops == '20g':
        params = 84
    elif flops == '1.1g':
        params = '24,12,24,48,48,48,96,96,96,192,192,192,192,192,192,192,384,192,192,384,192,192,384,192,96,384,96,' \
                 '48,192,48,24,96,24,48'
    elif flops == '970M':
        params = '24,8,24,48,32,48,96,72,96,192,224,192,192,296,192,192,360,192,192,360,192,192,296,192,96,256,96,48,'\
                 '160,48,24,40,24,32'
    else:
        params = '24,12,24,48,48,48,96,96,96,192,192,192,192,192,192,192,384,192,192,384,192,192,384,192,96,384,96,' \
                 '48,192,48,24,96,24,48' 
    return params

    
def load_model(net, model_path, device):
    state_dict = torch.load(model_path, map_location=str(torch.device('cpu')))
    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
        name = k.replace('module.', '')
        new_state_dict[name] = v
    # print(net)
    # print(state_dict.keys())
    net.load_state_dict(new_state_dict)
    
    #save torch 1.3 model
    #torch.save(net.state_dict(), model_path+'_1.3.pth',_use_new_zipfile_serialization=False)

    net.to(device)
    net.eval()
    
    
    
class Pix2PixSmallModel(BaseModel):
    """ This class implements the pix2pix model, for learning a mapping from input images to output images given paired data.

    The model training requires '--dataset_mode aligned' dataset.
    By default, it uses a '--netG unet256' U-Net generator,
    a '--netD basic' discriminator (PatchGAN),
    and a '--gan_mode' vanilla GAN loss (the cross-entropy objective used in the orignal GAN paper).

    pix2pix paper: https://arxiv.org/pdf/1611.07004.pdf
    """
    @staticmethod
    def modify_commandline_options(parser, is_train=True):
        """Add new dataset-specific options, and rewrite default values for existing options.

        Parameters:
            parser          -- original option parser
            is_train (bool) -- whether training phase or test phase. You can use this flag to add training-specific or test-specific options.

        Returns:
            the modified parser.

        For pix2pix, we do not use image buffer
        The training objective is: GAN Loss + lambda_L1 * ||G(A)-B||_1
        By default, we use vanilla GAN loss, UNet with batchnorm, and aligned datasets.
        """
        # changing the default values to match the pix2pix paper (https://phillipi.github.io/pix2pix/)
        parser.set_defaults(norm='batch', netG='unet_256', dataset_mode='aligned')
        if is_train:
            parser.set_defaults(pool_size=0, gan_mode='vanilla')
            parser.add_argument('--lambda_L1', type=float, default=100.0, help='weight for L1 loss')
            parser.add_argument('--use_SSIM', action='store_true',  help='use ssim loss')
            parser.add_argument('--lambda_SSIM',type=float, default=0, help='weight for ssim loss')
        return parser

    def __init__(self, opt):
        """Initialize the pix2pix class.

        Parameters:
            opt (Option class)-- stores all the experiment flags; needs to be a subclass of BaseOptions
        """
        BaseModel.__init__(self, opt)
        #pdb.set_trace()
        # specify the training losses you want to print out. The training/test scripts will call <BaseModel.get_current_losses>
        self.loss_names = ['G_GAN', 'G_L1', 'D_real', 'D_fake']
        # specify the images you want to save/display. The training/test scripts will call <BaseModel.get_current_visuals>
        self.visual_names = ['real_A', 'fake_B', 'real_B']
        # specify the models you want to save to the disk. The training/test scripts will call <BaseModel.save_networks> and <BaseModel.load_networks>
        if self.isTrain:
            self.model_names = ['G', 'D']
        else:  # during test time, only load G
            self.model_names = ['G']
        # define networks (both generator and discriminator)
       
        if opt.netG=="UnetMobileWithConfigQuantNgfs_bias":
            ngfs = flops2params(opt.flops)
            if isinstance(ngfs, int):
                ngfs = gen_ngfs(ngfs, ngfs, 3, 3)
            else:
                ngfs = gen_ngfs(-1, ngfs, 3, 3)
        elif opt.netG=="UnetMobileWithConfigQuant":
            ngfs = gen_ngfs(-1, '16,8,16,32,24,32,64,48,64,128,100,128,128,128,128,128,240,128,128,248,128,128,256,128,64,160,64,32,80,32,16,32,16,32', 3, 3)
#        print(len(ngfs))
        self.netG = networks.define_G(input_nc=3, output_nc=3, ngf=ngfs, netG=opt.netG, norm='instance', 
                                     use_dropout=False, dropout_p=0.5, init_type='normal', init_gain=0.02, image_size=(opt.crop_size, opt.crop_size))
        
        #print(self.netG)
        if self.isTrain:  # define a discriminator; conditional GANs need to take both input and output images; Therefore, #channels for D is input_nc + output_nc
            self.netD = networks.define_D(opt.input_nc + opt.output_nc, opt.ndf, opt.netD,
                                          opt.n_layers_D, opt.norm, opt.init_type, opt.init_gain, self.gpu_ids)

        if self.isTrain:
            # define loss functions
            self.criterionGAN = networks.GANLoss(opt.gan_mode).to(self.device)
            self.criterionL1 = torch.nn.L1Loss()
           
            if self.opt.use_SSIM:
                self.criterionSSIM = ssim_loss.SSIM()
                print("use ssim loss")
            # initialize optimizers; schedulers will be automatically created by function <BaseModel.setup>.
            self.optimizer_G = torch.optim.Adam(self.netG.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
            self.optimizer_D = torch.optim.Adam(self.netD.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
            self.optimizers.append(self.optimizer_G)
            self.optimizers.append(self.optimizer_D)

    def load_netG(self, path):
        net = self.netG
        if isinstance(net, torch.nn.DataParallel):
            net = net.module
        state_dict = torch.load(path, map_location=str(self.device))
        if hasattr(state_dict, '_metadata'):
            del state_dict._metadata
        for key in list(state_dict.keys()):  
            self.__patch_instance_norm_state_dict(state_dict, net, key.split('.'))

        net.load_state_dict(state_dict)   

             

    def set_input(self, input):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.

        Parameters:
            input (dict): include the data itself and its metadata information.

        The option 'direction' can be used to swap images in domain A and domain B.
        """
        AtoB = self.opt.direction == 'AtoB'
        self.real_A = input['A' if AtoB else 'B'].to(self.device)
        self.real_B = input['B' if AtoB else 'A'].to(self.device)
        self.image_paths = input['A_paths' if AtoB else 'B_paths']

    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""
        self.fake_B = self.netG(self.real_A)  # G(A)
        
    def load_networks(self, epoch):
        """Load all the networks from the disk.

        Parameters:
            epoch (int) -- current epoch; used in the file name '%s_net_%s.pth' % (epoch, name)
        """
        for name in self.model_names:
            print('******loading:' + name)
            if isinstance(name, str):
                
                load_filename = '%s_net_%s.pth' % (epoch, name)
                load_path = os.path.join(self.save_dir, load_filename)
                load_model(self.netG, load_path, self.device)

    def forward1(self, input):
#        pdb.set_trace()
#        print(input.shape)
        self.real_A = input.to(self.device).permute(0, 3, 1, 2) / 127.5 - 1.0
#        print("forwarding pix2pix_small")
#        print(self.real_A.shape)
        self.fake_B = self.netG(self.real_A)

    def backward_D(self):
        """Calculate GAN loss for the discriminator"""
        # Fake; stop backprop to the generator by detaching fake_B
        fake_AB = torch.cat((self.real_A, self.fake_B), 1)  # we use conditional GANs; we need to feed both input and output to the discriminator
        pred_fake = self.netD(fake_AB.detach())
        self.loss_D_fake = self.criterionGAN(pred_fake, False)
        # Real
        real_AB = torch.cat((self.real_A, self.real_B), 1)
        pred_real = self.netD(real_AB)
        self.loss_D_real = self.criterionGAN(pred_real, True)
        # combine loss and calculate gradients
        self.loss_D = (self.loss_D_fake + self.loss_D_real) * 0.5
        self.loss_D.backward()

    def backward_G(self):
        """Calculate GAN and L1 loss for the generator"""
        # First, G(A) should fake the discriminator
        fake_AB = torch.cat((self.real_A, self.fake_B), 1)
        pred_fake = self.netD(fake_AB)
        self.loss_G_GAN = self.criterionGAN(pred_fake, True)
        # Second, G(A) = B
        self.loss_G_L1 = self.criterionL1(self.fake_B, self.real_B) * self.opt.lambda_L1
        # combine loss and calculate gradients
        self.loss_G = self.loss_G_GAN + self.loss_G_L1
        if self.opt.use_SSIM:
            self.loss_G_SSIM = self.criterionSSIM(self.fake_B, self.real_B) * self.opt.lambda_SSIM
            self.loss_G = self.loss_G + self.loss_G_SSIM
        self.loss_G.backward()

    def optimize_parameters(self):
        self.forward()                   # compute fake images: G(A)
        # update D
        self.set_requires_grad(self.netD, True)  # enable backprop for D
        self.optimizer_D.zero_grad()     # set D's gradients to zero
        self.backward_D()                # calculate gradients for D
        self.optimizer_D.step()          # update D's weights
        # update G
        self.set_requires_grad(self.netD, False)  # D requires no gradients when optimizing G
        self.optimizer_G.zero_grad()        # set G's gradients to zero
        self.backward_G()                   # calculate graidents for G
        self.optimizer_G.step()             # udpate G's weights
